/*** settings **/

var DEBUG = true;






/*** functions **/

var rand = function(min, max){
	return Math.floor((Math.random()*max)+min);
};

function deg2rad(angle){
	return angle * .017453292519943295;
}

function inRange(x1, y1, x2, y2, _x1, _y1, _x2, _y2){
	if (parseInt(x1) >= parseInt(_x1) && parseInt(x2) <= parseInt(_x2) && parseInt(y1) >= parseInt(_y1) && parseInt(y2) <= parseInt(_y2))
		return true;
	return false;
}

