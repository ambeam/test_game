var gamejs = require('gamejs');


/********** MAP READER SCRIPT
 *
 */

var MapReader = function(){
    MapReader.superConstructor.apply(this, arguments);
    
    this.map_schema = null;
    this.map_tiles = [];
    this.respawn = [];
    this.settings = null;
    this.scenario = {};
    this.name = '';
    this.tile_images_list = [];
    var env;

    this.load = function(map){
    	if (!this.readSchema(map.schema)){
    		throw new Excpetion('Map schema is unreadable.');
    	}
    	if (!this.readElements(map.elements)){
    		throw new Excpetion('There are no elements over the map (ie. player respawn point).');
    	}
        this.settings = map.settings;
        this.name = map.settings.name;
        this.scenario = map.scenario;
        env = map.environment ? map.environment : [];
    };
    
    this.readSchema = function(schema){
    	if (schema.length){
    		this.map_schema = '';
            this.map_tiles = schema;
    		for(i in schema){
    			var current = schema[i];
    			this.map_schema += current.block.toString();
                if (typeof this.tile_images_list[current.tile] == "undefined")
                    this.tile_images_list[current.tile] = 1;
                else this.tile_images_list[current.tile]++;
    		}
    		return true;
    	}
    	else return false;
    };

    this.getSchemaTileAt = function(index){
        return this.map_tiles[index];
    };
    
    this.canShotThrough = function(index){
    	return this.map_schema.charAt(index) != 2;
    };

    this.getSchemaTileAtCoords = function(index){
        var size = this.getMapSize();
        return this.map_tiles[index[1]*size[0] + index[0]];
    };

    this.getTileFor = function(index){
        if (typeof index == "number")
            var tile = this.getSchemaTileAt(index);
        else var tile = this.getSchemaTileAtCoords(index);
        if (tile.tile == '' || tile.tile == '_null')
            return false;
        return map_tiles_folder+this.settings.tileset+'_'+tile.tile+'.png';
    };

    this.readElements = function(elements){
    	if (elements.length){
    		this.map_elements = [];
    		for(i in elements){
    			this.map_elements[elements[i].id] = elements[i];
    		}
    		return true;
    	}
    	else return false;
    };
    
    this.getCurrentMapSchema = function(){
    	return this.map_schema;
    };

    this.getScenario = function(){
        return this.scenario;
    };
    
    this.getEnvironment = function(index){
		var tmp = [];
		for(i in env){
			var ni = (typeof env[i].zindex != "undefined") ? env[i].zindex : 1;
			if (ni == index)
				tmp.push(env[i]);
		}
    	return tmp;
    };

    this.getElement = function(e){
    	var list = [];
    	for(i in this.map_elements){
    		if (this.map_elements[i].e_type == e)
    			list.push(this.map_elements[i]);
    	}
    	return (!list.length) ? false : list;
    };
    
    this.getRespawnPoint = function(all){
    	var e = this.getElement('player_respawn');
    	return all ? e[0] : e[0].coords;
    };
    
    this.getEnemyRespawnPoints = function(){
    	var e = this.getElement('enemy_respawn');
        var coords = [];
    	if (e !== false){
            for(i in e){
                coords.push(e[i].coords);
            }
    	}
    	return coords.length ? coords : [];
    };

    this.getMapSize = function(){
        return this.settings.size;
    };
    
    this.getImage = function(){
    	return typeof this.settings.map_image != "undefined" ? this.settings.map_image : false;
    };

    this.getPreloadTilesList = function(){
        var tiles = [];
        for(i in this.tile_images_list){
        	if (this.settings.tileset == '')
        		continue;
            tiles.push(map_tiles_folder+this.settings.tileset+'_'+i+'.png');
        }
        
        if (env.length){
        	for(i in env){
        		tiles.push(map_tree_folder+env[i].e_type+'.png');
        	}
        }
        
        return tiles;
    };
};

gamejs.utils.objects.extend(MapReader, gamejs.sprite.Sprite);
exports.MapReader = MapReader;