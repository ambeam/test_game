
var MapGeneratorV2 = {
    content: [],
    image: null,
    map_size: [],
    options: [{
        class: 'empty',
        data: 0,
        title: 'Empty'
    }, {
        class: 'wall',
        data: 1,
        title: 'Wall (blocks all)'
    }, {
        class: 'block',
        data: 2,
        title: 'Block (can shot through)'
    }, {
        class: 'player-respawn',
        data: 3,
        title: 'Player respawn point'
    }, {
        class: 'enemy',
        data: 4,
        title: 'Enemy'
    }
    ],
//        "- = _empty\n" +
//            "0 = ground\n" +
//            "1 = wall\n" +
//            "2 = block\n" +
//            "a = item:Armor\n" +
//            "b = item:Money\n" +
//            "e = enemy:0:Patrol:4:[[3, 3], [4, 8]]\n" +
//            "f = enemy:0:Patrol:3:[[6, 7], [9, 12]]\n" +
//            "r = player_respawn:\n",
    default_tileset: 'tm1',
    elements: [],
    schema: [],

    _findLargerThan: function(value){
        var n = 0;
        while(n < value){
            n += 32;
        }
        return n;
    },

    UpdateSize: function(){
        $('#mapSize').html(this.map_size[0]+'x'+this.map_size[1]);
    },

    RefreshImage: function(){
        $('#mapContent').find('img').each(function(){
            $(this).remove();
        });
        $('<img />').attr('src', $('#mapImage').val()).appendTo('#mapContent');
        $('#mapContent').find('img').on('load', function(){
            var width = $(this).width(),
                height = $(this).height();
            if (width % 32 || height % 32){
                width = MapGeneratorV2._findLargerThan(width);
                height = MapGeneratorV2._findLargerThan(height);
                $(this).width(width);
                $(this).height(height);
            }

            MapGeneratorV2.map_size = [width / 32, height / 32];
            MapGeneratorV2.UpdateSize();
            MapGeneratorV2.Fill();
        });
    },

    Fill: function(){
        for(var i = 0; i < this.map_size[0]; i++){
            for(var l = 0; l < this.map_size[1]; l++){
                $('<a/>').attr('href', 'javascript:;').addClass('frame empty').attr('data-vertical', i).attr('data-horizontal', l).css({left:i*32, top:l*32}).appendTo('#mapContent');
            }
        }
        $('.frame').click(function(){
            var sel = $('.select-frame.selected');
            if (sel.length){
                $(this).attr('class', 'frame '+sel.attr('data-type'));
            }
        });
    },

    Start: function(){
        var e = $('#mapOptions');
        for(i in this.options){
            $('<a/>').attr('href', 'javascript:;').addClass(this.options[i].class+' select-frame').attr('data-type', this.options[i].class).appendTo(e);
            $('<p/>').text(this.options[i].title).appendTo(e);
            $('<div/>').addClass('clear').appendTo(e);
        }

        $('.select-frame').on('click', function(){
            $('#mapOptions').children('a').removeClass('selected');
            $(this).addClass('selected');
        });
    },
    
    FindOpt: function(opt){
    	for(i in this.options){
    		if (this.options[i].class == opt)
    			return this.options[i].data;
    	}
    	return 1;
    },
    
    Generate: function(){
    	var cont = $('.gen-content'),
    		_elements = [],
    		element_id = 0;
     	this.content = [];
     	$('.gjs-settings-container').addClass('loader');
    	for(var l = 0; l < this.map_size[1]; l++){
    		
    		for(var i = 0; i < this.map_size[0]; i++){
    			var _cl = $('.frame[data-vertical='+i+'][data-horizontal='+l+']').attr('class'),
    				_c = _cl.split(/\s+/),
    				_status = 0;
    			
    			for(_cnt in _c){
    				if (_c[_cnt] == 'frame')
    					continue;
    				if (_c[_cnt] == 'player-respawn'){
    					_status = 0;
    					_elements.push({
    						e_type: 'player_respawn',
    						coords: [i, l],
    						id: element_id++,
    						angle: 0
    					});
    				}
    				else if (_c[_cnt] == 'enemy'){
    					_status = 0;
    					_elements.push({
    						e_type: 'enemy',
    						coords: [i, l],
                            type: 1,
                            mode: "Patrol",
                            shoot_type: 3,
                            patrol: [[3,3],[i,l]],
                            id: element_id++
    					});
    				}
    				else _status = this.FindOpt(_c[_cnt]);
    			}
    			
    			this.content.push({
                    tile: '',
                    block: _status,
                    special: ''
                });
    		}
    	}
    	
    	cont.text(this.content);
    	
    	var res = {};
        res.settings = {
            name: $('#mapName').val(),
            mode: 'single',
            tileset: '',
            map_image: $('#mapImage').val(),
            size: this.map_size,
            shopping: true,
            allow_save: true,
            finish_prize: 10000
        };
        res.scenario = {};
        res.schema = this.content;
        res.elements = _elements;
        cont.val(JSON.stringify(res));
        $('.gjs-settings-container').removeClass('loader');
    	return false;
    },

    Update: function(){
        this.Start();

        $('#mapImage').on('change blur', function(){
            MapGeneratorV2.RefreshImage();
        }).trigger('change');

        $('.gen-map').on('click', function(){
        	MapGeneratorV2.Generate();
        });
    }
};

$(document).ready(function(){
    MapGeneratorV2.Update();
});
