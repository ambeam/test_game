var gamejs = require('gamejs');

/********** PARTICLE CLASS
 * 
 */
var display = gamejs.display.getSurface();

var Particles = function(settings, coords){
	Particles.superConstructor.apply(this, arguments);
	
	var defaults = {
		maxParticles: 1200, // experiment! 20,000 provides a nice galaxy
		emissionRate: 200,
		particleSize: 1,
		lifeTime: 30,
		fadeTime: 25,
		image: '',
		velocity: 2,
		passing: false
	};
	
	var emitter = null;
	var img = null;
	var img_size = [];
	var particles_array = [];
	
	var options = $.extend(true, {}, defaults, settings || {});
	var emitter = new Emitter(new Vector(coords[0], coords[1]), Vector.fromAngle(0, options.velocity));
	for(var j = 0; j < options.emissionRate; j++){
		particles_array.push(emitter.emitParticle());
	}
	
	if (options.image != ''){
		img = gamejs.image.load(options.image),
		img_size = img.getSize();
	}
	
	this.update = function(msDuration){
		this.plotParticles();
	};
	
	this.draw = function(){
		// For each particle
		for(var i = 0; i < particles_array.length; i++){
			var particle = particles_array[i];
			var position = particle.position;
			var fade = particle.life > options.fadeTime ? (1 * options.lifeTime - particle.life) / (options.lifeTime-options.fadeTime) : 1;
			if (fade < 0) fade = 0;
		    // Draw a square at our position [particleSize] wide and tall
			if (options.image){
				img.setAlpha(1-fade);
				display.blit(img, [viewport_offset[0] + position.x - img_size[0]/2, viewport_offset[1] + position.y - img_size[1]/2]);
			}
			else gamejs.draw.rect(display, 'rgba(255, 255, 210, '+fade+')', new gamejs.Rect([viewport_offset[0] + position.x, viewport_offset[1] + position.y], [options.particleSize, options.particleSize]));
		}
	};
	
	this.plotParticles = function(boundsX, boundsY){
		// a new array to hold particles within our bounds
		var currentParticles = [];
		for(var i = 0; i < particles_array.length; i++){
		    var particle = particles_array[i];
		    if (particle.life > options.lifeTime)
		    	continue;
		    var pos = particle.position;
		    // If we're out of bounds, drop this particle and move on to the next
		    if (pos.x < viewport_offset[0] || pos.x > viewport_width - viewport_offset[0] || pos.y < viewport_offset[1] || pos.y > viewport_height - viewport_offset[1]) continue;
		    
		    if (!options.passing){
		    	if (1 == checkSheetAt(pixelsToCoords(pos.x, pos.y))){
		    		continue;
		    	}
		    }
		    
		    // Move our particles
		    particle.move();
		    // Add this particle to the list of current particles
		    currentParticles.push(particle);
		}
		// Update our global particles, clearing room for old particles to be collected
		particles_array = currentParticles;
	};
	
};


/**
 * Vector object
 */
function Vector(x, y){
	this.x = x || 0;
	this.y = y || 0;
}

//Add a vector to another
Vector.prototype.add = function(vector){
	this.x += vector.x;
	this.y += vector.y;
}

//Gets the length of the vector
Vector.prototype.getMagnitude = function(){
	return Math.sqrt(this.x * this.x + this.y * this.y);
};
 
// Gets the angle accounting for the quadrant we're in
Vector.prototype.getAngle = function(){
	return Math.atan2(this.y,this.x);
};

// Allows us to get a new vector from angle and magnitude
Vector.fromAngle = function(angle, magnitude){
	return new Vector(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
};


/**
 * Particle object
 */
function Particle(point, velocity, acceleration){
	this.position = point || new Vector(0, 0);
	this.velocity = velocity || new Vector(0, 0);
	this.acceleration = acceleration || new Vector(0, 0);
	this.life = 0;
}

Particle.prototype.move = function(){
	// Add our current acceleration to our current velocity
	this.velocity.add(this.acceleration);
	// Add our current velocity to our position
	this.position.add(this.velocity);
	this.life++;
};


/**
 * Emitter object
 */
function Emitter(point, velocity, spread){
	this.position = point; // Vector
  	this.velocity = velocity; // Vector
  	this.spread = spread || Math.PI; // possible angles = velocity +/- spread
  	this.drawColor = "#999"; // So we can tell them apart from Fields later
}

Emitter.prototype.emitParticle = function(){
	// Use an angle randomized over the spread so we have more of a "spray"
	var angle = this.velocity.getAngle() + this.spread - (Math.random() * this.spread * 2);
	// The magnitude of the emitter's velocity
	var magnitude = this.velocity.getMagnitude();
	// The emitter's position
	var position = new Vector(this.position.x, this.position.y);
	// New velocity based off of the calculated angle and magnitude
	var velocity = Vector.fromAngle(angle, magnitude);
	// return our new Particle!
	return new Particle(position,velocity);
};


gamejs.utils.objects.extend(Particles, gamejs.sprite.Sprite);
exports.Particles = Particles;