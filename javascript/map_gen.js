var MapGenerator = {
    content: [],
    map_size: [],
    defaults:
        "- = _empty\n" +
        "0 = ground\n" +
        "1 = wall\n" +
        "2 = block\n" +
        "a = item:Armor\n" +
        "b = item:Money\n" +
        "e = enemy:0:Patrol:4:[[3, 3], [4, 8]]\n" +
        "f = enemy:0:Patrol:3:[[6, 7], [9, 12]]\n" +
        "r = player_respawn:\n",
    default_tileset: 'tm1',
    elements: [],
    schema: [],

    Load: function(content){
        this.content = this.ParseContent(content);
    },

    ParseContent: function(content){
        var lines = content.split("\n"),
            max_count = 0;
        for(i in lines){
            if (max_count < lines[i].length)
                max_count = lines[i].length;
        }
        for(i in lines){
            if (lines[i].length < max_count){
                var add = '';
                for(l = 1; l <= max_count - lines[i].length; l++)
                    add += '-';
                lines[i] += add;
            }
        }
        this.map_size = [max_count, lines.length];
        return lines;
    },

    Build: function(){
        var res = {};
        res.settings = {
            name: $('#mapName').val(),
            mode: 'single',
            tileset: $('#mapTileset').val(),
            size: this.map_size,
            shopping: true,
            allow_save: true,
            finish_prize: true
        };
        res.scenario = {};
        this.ReadSchema();
        res.schema = this.schema;
        res.elements = this.elements;
        $('#mapContentResult').val(JSON.stringify(res));
    },

    GetSettings: function(){
        var df = $('#mapSettings').val().split("\n"),
            res = {};

        for(i in df){
            var _tmp = df[i].trim().split('=');
            if (_tmp.length == 2){
                var _t = _tmp[1].trim(),
                    parts = _t.split(':');
                if (parts.length > 1){
                    res[_tmp[0].trim()] = {e_type: parts[0], additional: parts[1]};
                    if (parts.length > 2){
                        res[_tmp[0].trim()].settings = parts;
                    }
                }
                else res[_tmp[0].trim()] = _t;
            }
        }
        return res;
    },

    ReadSchema: function(){
        var df = this.GetSettings(),
            schema = [],
            id = 1;
        for(i in this.content){
            for(l in this.content[i]){
                var c = this.content[i][l],
                    block = 1,
                    tile = '';
                if (typeof df[c] == 'object'){
                    if (df[c].e_type == 'item'){
                        this.elements.push({e_type: df[c].e_type, type: df[c].additional, coords: [parseInt(l), parseInt(i)], id: id++});
                        block = 0;
                        tile = 'ground';
                    }
                    else if (df[c].e_type == 'enemy'){
                        this.elements.push({
                            e_type: 'enemy',
                            coords: [parseInt(l), parseInt(i)],
                            type: df[c].settings[1],
                            mode: df[c].settings[2],
                            shoot_type: df[c].settings[3],
                            patrol: JSON.parse(df[c].settings[4]),
                            id: id++
                        });
                        block = 0;
                        tile = 'ground';
                    }
                    else if(df[c].e_type == 'tile'){
                        block = 0;
                        tile = df[c].additional;
                        if (typeof df[c].settings[2] != "undefined")
                            block = df[c].settings[2];
                    }
                    else this.elements.push({e_type: df[c].e_type, coords: [parseInt(l), parseInt(i)], id: id++});

                }
                else if (df[c] != 'wall'){
                    block = 0;
                    tile = df[c];
                }
                else tile = df[c];

                if (tile == "_empty"){
                    tile = "_null";
                    block = 1;
                }
                schema.push({
                    tile: tile,
                    block: block,
                    special: ''
                });
            }
        }
        this.schema = schema;
    },

    Defaults: function(){
        $('#mapSettings').val(this.defaults);
        $('#mapTileset').val(this.default_tileset);
    },

    Update: function(){
        this.Defaults();
        $('#genMap').on('click', function(){
            MapGenerator.Load($('#mapContent').val());
            MapGenerator.Build();
        });
    }
};

$(document).ready(function(){
    MapGenerator.Update();
});

/*

 11111111111111111111111111111111-1111111111111111111111111
 10000000000000000000000000000001-10000000000000000000000a1
 10000000000000000000000000000001-1000000000000000000000001
 1000000000000000000000000000000111000000000000000000000001
 1000000000000000000000000000000000000000000000000000000001
 1000000000000000000000000000000111000000000000000000000001
 10r00000000000000000000000000001-1000000000000000000000001
 11111111111111111111111111111111-1111111111111111111111111



1111111111111-111111111
1000000000001-100000a01
1000000000001-100000001
1000000000001-100000001
1000000000001-100000001
1000000000001-100000001
10000000000011100000001
10000000000000000000001
10000000000011100000001
1000000000001-100000r01
1111111111111-111111111
*/