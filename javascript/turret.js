var gamejs = require('gamejs');


/********** TURRET CLASS
 *
 */

var RAD_CONST = 0.017453292519943295;

var display = gamejs.display.getSurface();

var Turret = function(data, weapon, id){
    Turret.superConstructor.apply(this, arguments);

    this.weapon = Object.create(weapon);
    this.id = id;
    this.x = data.x * 32 + 4;
    this.y = data.y * 32;
    this.angle = 0;
    this.prev_angle = this.angle;

    this.type = data.type;
    this.settings = Object.create(turretTypes[data.type]);
    this.strength = this.settings.strength;

    this.originalImage = gamejs.image.load(this.settings.image);
    this.originalImage = gamejs.transform.scale(this.originalImage, this.settings.size);
    this.image = gamejs.transform.rotate(this.originalImage, this.angle);
    this.size = this.originalImage.getSize();

    this.x2 = this.x + this.size[0];
    this.y2 = this.y + this.size[1];
    this.rect = new gamejs.Rect([this.x, this.y], this.size);
    this.shooting = false;

    this.calibrate = [1, 1];

    this.getCoords = function(){
        return [Math.floor((this.x + this.size[0] / 2) / 32), Math.floor((this.y + this.size[1] / 2) / 32)];
    };

    /**
     * Zwraca polozenie w pikselach
     *
     * @param coords
     * @returns {Array}
     */
    this.returnExactTargetCoords = function(coords){
        var x = (32 - this.size[0]) / 2;
            y = (32 - this.size[1]) / 2;
        return [coords[0] + x + this.calibrate[0], coords[1] + y + this.calibrate[1]];
    };

    /**
     * Wyznacza kat obrotu wiezy
     */
    this.calculateAngle = function(target_cell, map){
        var _search_cell = map.numToCoords(target_cell);
        _search_cell = this.returnExactTargetCoords([_search_cell[0] * 32, _search_cell[1] * 32]);
        var angle = (Math.atan2(_search_cell[1] - parseInt(this.y), _search_cell[0] - parseInt(this.x)) * (180 / Math.PI)) / 10;
        return ((angle.toFixed(1) * 10) + 360) % 360 + 90;
    };

    /**
     * Glowna petla
     */
    this.update = function(msDuration, map, player){

        var dist = map.getDistance(player.getCoords(), this.getCoords());

        if (dist * 32 > this.settings.sight_distance){
            this.shooting = false;
        }
        else this.shooting = true;

        if (this.shooting){
            var ang = this.calculateAngle(map.coordsToNum(player.getCoords()), map);
            if (this.angle != ang){
                this.prev = this.angle;
                this.angle = ang;
                this.turn = true;
                if (this.angle - this.prev > 180){
                    this.angle -= 360;
                }
                else if (this.angle + this.prev > 360 && this.prev > 180 && this.angle < 180){
                    this.angle += 360;
                }
            }
        }

        if (this.turn){
            var inc = this.settings.turn_speed * msDuration;
            if (this.prev < this.angle){
                this.prev += inc;
                if (this.prev >= this.angle){
                    if (this.angle < 0){
                        this.angle += 360;
                    }
                    if (this.angle >= 360)
                        this.angle %= 360;
                    this.prev = this.angle;
                    this.turn = false;
                }
            }
            else{
                this.prev -= inc;
                if (this.prev <= this.angle){
                    if (this.angle < 0){
                        this.angle += 360;
                    }
                    if (this.angle >= 360)
                        this.angle %= 360;
                    this.prev = this.angle;
                    this.turn = false;
                }
            }
            this.image = gamejs.transform.rotate(this.originalImage, this.prev);
        }

        return this.shooting ? this.shot(msDuration) : null;
    };

    this.shot = function(msDuration){
        this.weapon.next_fire -= msDuration;
        if (this.weapon.next_fire <= 0){
            this.weapon.next_fire = this.weapon.delay;
            var origin = [1 * Math.sin(deg2rad(this.angle)), -1 * Math.cos(deg2rad(this.angle))];
            return {
                shot: this.weapon,
                angle: this.angle,
                x: (this.x + origin[0] * 12) + this.size[0] / 2 - 3,
                y: (this.y + origin[1] * 12) + this.size[1] / 2 - 5
            };
        }
        return null;
    };

    this.isHit = function(coords, size, vector){
        var x = coords[0] + (size[0] / 2) + vector[0],
            y = coords[1] + (size[1] / 2) + vector[1];
        if (x >= this.x && x <= this.x + this.size[0] && y >= this.y && y <= this.y + this.size[0]){
            return true;
        }
        return false;
    };

    this.hit = function(amount, vector, map){
        this.strength -= amount;
        if (this.strength <= 0){
            return false;
        };
        return true;
    };

    this.drawBody = function(){
        display.blit(this.image, [viewport_offset[0] + this.x, viewport_offset[1] + this.y]);
        if (display_enemy_health)
            this.displayHealthBar();
        if (display_foresight_circle)
            this.displayForesightCircle();
    };

    this.displayHealthBar = function(){
        var color = "#00ff00",
            factor = this.strength / this.settings.strength;
        var width = 28 * factor;
        if (Math.round(factor * 100) < 55) color = '#FFFF00';
        if (Math.round(factor * 100) < 40) color = '#FFBF00';
        if (Math.round(factor * 100) < 25) color = '#FF0000';

        gamejs.draw.rect(display, color, new gamejs.Rect([viewport_offset[0] + this.x - 2, viewport_offset[1] + this.y + this.size[1]], [width, 4]));
        gamejs.draw.rect(display, '#000000', new gamejs.Rect([viewport_offset[0] + this.x - 2, viewport_offset[1] + this.y + this.size[1]], [28, 4]), 1);
    };

    this.displayForesightCircle = function(){
        gamejs.draw.circle(display, 'rgba(255, 255, 255, 0.07)', [viewport_offset[0] + this.x + this.size[0]/2, viewport_offset[1] + this.y + this.size[1]/2], this.settings.sight_distance);
    };
};


gamejs.utils.objects.extend(Turret, gamejs.sprite.Sprite);
exports.Turret = Turret;