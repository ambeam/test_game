var ship_speed = 20;

var gamejs = require('gamejs');
var box2d = require('gamejs/Box2dWeb-21.a.3');
var vectors = require('gamejs/utils/vectors');
var math = require('gamejs/utils/math');


var STEER_NONE		= 0;
var STEER_RIGHT		= 1;
var STEER_LEFT		= 2;
var ACC_NONE		= 0;
var ACC_ACCELERATE	= 1;
var ACC_BRAKE		= 2;

var width_px		= 1024;
var height_px		= 600;
var scale			= 15;
var width_m			= width_px / scale;
var height_m		= height_px / scale;
var keys_down		= {};
var b2world;

var bindings =	{
	accelerate		: gamejs.event.K_UP,
	brake			: gamejs.event.K_DOWN,
	steer_left		: gamejs.event.K_LEFT,
    steer_right		: gamejs.event.K_RIGHT
};


gamejs.preload(["images/tank1.png"]);

var Tank = function(rect){
	Tank.superConstructor.apply(this, arguments);
	
	this.speed = ship_speed;
	this.originalImage = gamejs.image.load("images/tank1.png");
	
	var dims = this.originalImage.getSize();
	this.rotation = 180;
	this.direction = 2;
	this.rotate_speed = 5;
	this.rotation_ended = true;
	this.temp_rotation = 0;
	this.image = gamejs.transform.rotate(this.originalImage, this.rotation);
	this.rect = new gamejs.Rect(rect);
	this.max_steer_angle = 20;
	this.wheel_angle = 0;
	this.power = 60;
	this.max_speed = 60;
	
	this.previous_position = [0, 0];
	
//	
//	var def 			= new box2d.b2BodyDef();
//    def.type 			= box2d.b2Body.b2_dynamicBody;
//    def.position 		= new box2d.b2Vec2(rect[0], rect[1]);
//    def.angle 			= math.radians(this.rotation);
//    def.linearDamping	= 0.15;
//    def.bullet			= true;
//    def.angularDamping	= 0.3;
//    this.body			= b2world.CreateBody(def);
//    
//    var fixdef = new box2d.b2FixtureDef();
//    fixdef.density 		= 1;
//    fixdef.isSensor		= true;
//    fixdef.shape		= new box2d.b2PolygonShape();
//    fixdef.shape.SetAsBox(1, 2);
//    this.body.CreateFixture(fixdef);
	
	
	
	
	
	
	
	
	
	
	
	
//	this.ChangeDirection = function(dir){
//		if (this.rotation_ended){
//			this.direction = dir;
//			this.temp_rotation = 90 * dir;
//			this.rotation_ended = false;
//		}
//	};
//	
	return this;
};

gamejs.utils.objects.extend(Tank, gamejs.sprite.Sprite);




Tank.prototype.getSpeed = function(msDuration){
	var p = this.previous_position,
		n = [this.rect.left, this.rect.top];
	var sp = [(n[0]-p[0]), (n[1]-p[1])];
	
	var len = vectors.len(sp);
    return (len/1000)*3600;
};

Tank.prototype.update = function(msDuration){
	
	var incr = (this.max_steer_angle/200) * msDuration;
	
	if (this.steer == STEER_RIGHT){
        this.wheel_angle = Math.min(Math.max(this.wheel_angle, 0) + incr, this.max_steer_angle);
    }
	else if (this.steer == STEER_LEFT){
        this.wheel_angle = Math.max(Math.min(this.wheel_angle, 0) - incr, -this.max_steer_angle);
    }
	else{
        this.wheel_angle = 0;
    }
	
	var base_vect;
	
	if ((this.accelerate == ACC_ACCELERATE) && (this.getSpeed() < this.max_speed)){
        base_vect = [0, -1];
    }
    else if (this.accelerate == ACC_BRAKE){
        if (this.getLocalVelocity()[1] < 0)
        	base_vect = [0, 1.3];
        else
        	base_vect = [0, 0.7];
    }
    else
    	base_vect = [0, 0];
	
	var fvect = [this.power*base_vect[0], this.power*base_vect[1]];
	
	console.log(fvect);
	
	this.previous_position = [this.rect.left, this.rect.top];
	
	
	this.rect.left += fvect[0];
	this.rect.top += fvect[1];
	
	
	//this.image = gamejs.transform.rotate(this.originalImage, this.rotation);
	
};














gamejs.preload([]);
gamejs.ready(function(){
	
    var display = gamejs.display.setMode([800, 600]);
    var tank = new Tank([100, 100]);
    var mainSurface = gamejs.display.getSurface();
    
    gamejs.onEvent(function(event){
    	
    	if (event.type === gamejs.event.KEY_DOWN)
    		keys_down[event.key] = true;
        else if (event.type === gamejs.event.KEY_UP)
        	keys_down[event.key] = false;
    	
    });

    gamejs.onTick(function(msDuration){
    	
    	if (keys_down[bindings.accelerate]){
            tank.accelerate = ACC_ACCELERATE;
        }
        else if (keys_down[bindings.brake]){
            tank.accelerate = ACC_BRAKE;
        }
        else{
            tank.accelerate = ACC_NONE;
        }
        
        if (keys_down[bindings.steer_right]){
        	tank.steer = STEER_RIGHT;
        }
        else if (keys_down[bindings.steer_left]){
        	tank.steer = STEER_LEFT;
        }
        else{
        	tank.steer = STEER_NONE;
        }
    	
    	mainSurface.fill("#FFFFFF");
    	tank.update(msDuration);
    	tank.draw(mainSurface);
    });
});
