var gamejs = require('gamejs');

/********** FLYBY CLASS
 * 
 */
var display = gamejs.display.getSurface();

var Flyby = function(value, coords, color, settings){
	Flyby.superConstructor.apply(this, arguments);
	
	this.value = typeof(value) == "number" ? '+'+value : value;
	this.distance = 0;
	this.max_distance = settings ? settings.distance : 45;
	this.speed = settings ? settings.speed : 10;
	
	this.color = color;
	
	this.x = (coords[0] * 32);
	this.y = (coords[1] * 32) + 7;
	var vs = new String(value);
	if (vs.length == 2){
		this.x += 5;
	}
	else if (vs.length > 3){
		this.x -= (vs.length - 3) * 5;
	}
	
	this.font = new gamejs.font.Font('12px arial');
	
	this.update = function(msDuration){
		this.y -= this.speed * (msDuration/global_refresh_rate);
		this.distance++;
		if (this.distance > this.max_distance)
			this.kill();
	};
	
	this.draw = function(){
		display.blit(this.font.render(this.value, this.color), [viewport_offset[0] + this.x, viewport_offset[1] + this.y]);
	};
};



gamejs.utils.objects.extend(Flyby, gamejs.sprite.Sprite);
exports.Flyby = Flyby;