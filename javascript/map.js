var gamejs = require('gamejs');
var draw = require('gamejs/draw');
var srf = require('gamejs/surfacearray');

/********** MAP CLASS
 * 
 */

organizer.add([
               "maps/tiles/tm1_ground.png",
               "maps/tiles/tm1_wall.png",
               "maps/tiles/tm1_tele.png",
               "maps/globalMap/map2.jpg",
               "maps/globalMap/map4.jpg",
               "maps/globalMap/map5.jpg",
               "images/tree/tree01.png",
               "images/tree/tree02.png",
               "images/tree/tree03.png",
               "images/tree/tree04.png",
               "images/tree/tree05.png",
               "images/tree/tree06.png",
               "images/tree/tree07.png",
               "images/tree/tree08.png",
               "images/tree/tree09.png",
               "images/tree/tree10.png",
               "images/tree/tree11.png",
               "images/tree/tree12.png",
               "images/tree/grass01.png"
               ]);
//gamejs.preload();



/**
 *  -33 -32 -31
 *   -1  X   1
 *   31  32  33
 */
var cells_around_point = [];


var Map = function(display, items){
	Map.superConstructor.apply(this, arguments);
	this.sheet = [];
	this.items = items;
    this.size = [0, 0];
	this.image_array = [];
    this.pixel_map = null;
    this.minimap_scale_factor = 1;
	
	this.surface = null;
	
	var env_lev0 = [],
		env_lev1 = [],
		env_loaded0 = [],
		env_loaded1 = [];
	
//	var x1 = 40,
//		y1 = 10,
//		x2 = 50,
//		y2 = 40;
	
	
	this.load = function(obj, full){
        //gamejs.preload(obj.getPreloadTilesList());
		
        this.size = obj.getMapSize();
        env_lev1 = obj.getEnvironment(1);
		env_lev0 = obj.getEnvironment(0);
        _mpSize = this.size;
        
        this.sheet = {
    		data: obj.getCurrentMapSchema(),
    		elements: [],
    		charAt: function(attr){
    			var _info = this.data.charAt(attr);
    			if (this.elements[attr] == 0)
    				_info = 2;
    			return _info;
    		}
    	};
        
        _mpSheet = this.sheet;
        
        this.surface = new gamejs.Surface(this.size[0] * 32, this.size[1] * 32);

        cells_around_point = [-this.size[0], -(this.size[0]-1), 1, this.size[0]+1, this.size[0], this.size[0]-1, -1, -(this.size[0]+1)];

        if (this.size[0]*32 < viewport_width){
            viewport_offset[0] = (viewport_width / 2) - (this.size[0] * 32) / 2;
        }
        if (this.size[1]*32 < viewport_height){
            viewport_offset[1] = (viewport_height / 2) - (this.size[1] * 32) / 2;
        }

        if (full){
        	var _surface = gamejs.image.load(obj.getImage());
        	_surface = gamejs.transform.scale(_surface, [32 * this.size[0], 32 * this.size[1]]);
        	this.surface.blit(_surface);
        }
        else {
        	for(var l = 0; l <= this.size[1]-1; l++){
    			for(var i = 0; i <= this.size[0]-1; i++){
                    var t = obj.getTileFor([i, l]);
                    if (t === false)
                        continue;
                    var image = gamejs.image.load(t);
    				this.image_array[i, l] = image;
    				var offset = [32*i, 32*l];
    				if (!full)
    					this.surface.blit(image, offset);
    			}
    		}
        }
		
		this.createPixelMap();
		this.prepareEnvironment();
	};
	
	this.draw = function(){
		display.blit(this.surface, [viewport_offset[0], viewport_offset[1]]);
	};
	
	this.updateElements = function(structures, towers){
		var _el = [];
		
		for(var l = 0; l <= this.size[1]-1; l++){
			for(var i = 0; i <= this.size[0]-1; i++){
				_el[this.coordsToNum([i, l])] = 1;
			}
		}
		
		if (structures instanceof Array){
			if (structures.length){
		        for (i in structures){
		        	_el[this.coordsToNum(structures[i].coords)] = 0;
		        }
		    }
		}
		else if (typeof structures == 'object'){
			structures.forEach(function(t){
				_el[this.coordsToNum(t.getCoords())] = 0;
			});
		}
		
		if (towers instanceof Array){
			if (towers.length){
		        for (i in towers){
		        	_el[this.coordsToNum(towers[i].coords)] = 0;
		        }
		    }
		}
		else if (typeof towers == 'object'){
			towers.forEach(function(t){
				_el[this.coordsToNum(t.getCoords())] = 0;
			});
		}
		this.sheet.elements = _el;
	};
	
	this.prepareEnvironment = function(){
		if (!env_lev0.length && !env_lev1.length)
			return;
		
		if (env_lev0.length){
			for(i in env_lev0){
				
				var _img = gamejs.image.load(map_tree_folder+env_lev0[i].e_type+'.png');
				_img = gamejs.transform.rotate(_img, rand(0, 359));
				
				env_loaded0.push({
					image: _img,
					coords: env_lev0[i].coords,
					destroyable: env_lev0[i].destroyable,
					strength: env_lev0[i].strength,
					blocking: false
				});
			}
		}
		
		if (env_lev1.length){
			for(i in env_lev1){
				env_loaded1.push({
					image: gamejs.image.load(map_tree_folder+env_lev1[i].e_type+'.png'),
					coords: env_lev1[i].coords,
					destroyable: env_lev1[i].destroyable,
					strength: env_lev1[i].strength,
					blocking: false
				});
			}
		}
	};
	
	this.drawEnvironment = function(display, level){
		var _env = level == 1 ? env_loaded1 : env_loaded0;
		if (!_env.length)
			return;
		for(i in _env){
			var _size = _env[i].image.getSize();
			display.blit(_env[i].image, [viewport_offset[0] + _env[i].coords[0] * 32 + 16 - _size[0]/2, viewport_offset[1] + _env[i].coords[1] * 32 + 16 - _size[1]/2]);
		}
	};
	
	this.isUnderTree = function(pos){
		if (!env_loaded1.length)
			return false;
		for(i in env_loaded1){
			var _size = env_loaded1[i].image.getSize(),
				x = env_loaded1[i].coords[0] * 32 + 24 - _size[0]/2,
				y = env_loaded1[i].coords[1] * 32 + 24 - _size[1]/2,
				x1 = x + _size[0]/2,
				y1 = y + _size[1]/2;
			
			if (pos[0] > x && pos[0] < x1 && pos[1] > y && pos[1] < y1)
				return true;
		}
		return false;
	};

    /**
     * Generowanie minimapy
     */
	this.drawMiniMap = function(display, items, enemies, structures, turrets, player){
        if (!display_minimap)
            return;

        var mx = (-viewport_offset[0] / 32) * this.minimap_scale_factor,
            my = (-viewport_offset[1] / 32) * this.minimap_scale_factor,
            mx2 = (viewport_width / 32) * this.minimap_scale_factor,
            my2 = (viewport_height / 32) * this.minimap_scale_factor;
//        if (mx < 0) mx = 0;
//        if (my < 0) my = 0;
//        if (mx2 > minimap_size[0]) mx2 = minimap_size[0];
//        if (my2 > minimap_size[1]) my2 = minimap_size[1];

        draw.rect(display, 'rgba(32,32,32,1)', new gamejs.Rect([minimap_pos[0] - 2, minimap_pos[1] - 2], [minimap_size[0] + 4, minimap_size[1] + 4]));
        srf.blitArray(display, this.pixel_map, minimap_pos);
        draw.rect(display, 'rgba(255,255,255,0.8)', new gamejs.Rect([minimap_pos[0] + mx, minimap_pos[1] + my], [mx2, my2]), 1);
        draw.rect(display, 'rgba(255,255,255,0.5)', new gamejs.Rect([minimap_pos[0] + mx, minimap_pos[1] + my], [mx2, my2]));

        var pc = player.getCoords();
        draw.circle(display, 'rgba(0, 200, 100, 1)', [minimap_pos[0] + pc[0]*this.minimap_scale_factor, minimap_pos[1] + pc[1]*this.minimap_scale_factor], 1);
        for(i in items){
            var ix = (items[i].x / 32) * this.minimap_scale_factor,
                iy = (items[i].y / 32) * this.minimap_scale_factor;
            draw.circle(display, 'rgba(62, 157, 145, 1)', [minimap_pos[0] + ix, minimap_pos[1] + iy], 1);
        }
        var _msf = this.minimap_scale_factor;
        enemies.forEach(function(e){
            var pe = e.getCoords();
            draw.circle(display, 'rgba(255, 10, 10, 1)', [minimap_pos[0] + pe[0]*_msf, minimap_pos[1] + pe[1]*_msf], 1);
        });
        structures.forEach(function(e){
            var pe = e.getCoords();
            draw.circle(display, 'rgba(192, 128, 10, 1)', [minimap_pos[0] + pe[0]*_msf, minimap_pos[1] + pe[1]*_msf], 1);
        });
        turrets.forEach(function(e){
            var pe = e.getCoords();
            draw.circle(display, 'rgba(192, 128, 10, 1)', [minimap_pos[0] + pe[0]*_msf, minimap_pos[1] + pe[1]*_msf], 1);
        });
    };

    this.createPixelMap = function(){

        var scale_factor = 1;
        if (this.size[0] != minimap_size[0]){
            scale_factor = (minimap_size[0] / this.size[0]);
        }
        if (this.size[1] * scale_factor > minimap_size[1]){
            scale_factor = (minimap_size[1] / this.size[1]);
        }

        this.minimap_scale_factor = scale_factor;
        var srfArray = new srf.SurfaceArray(this.surface);
        this.pixel_map = new srf.SurfaceArray(minimap_size);

        for(var i = 0; i <= this.size[0] - 1; i++){
            this.pixel_map[i] = [];
            for(var l = 0; l <= this.size[1] - 1; l++){
                if (scale_factor <= 1)
                    this.pixel_map.set(Math.round(i * scale_factor), Math.round(l * scale_factor), srfArray.get(i * 32 + 16, l * 32 + 16));
                else {
                    for(var a = 0; a <= Math.ceil(scale_factor) - 1; a++)
                        for(var b = 0; b <= Math.ceil(scale_factor) - 1; b++){
                            this.pixel_map.set(Math.round(i * scale_factor) + a, Math.round(l * scale_factor) + b, srfArray.get(i * 32 + 16, l * 32 + 16));
                        }
                }
            }
        }

    };

	this.mapAdvancedCollider = function(coords, size, vector){
		var x = coords[0] + vector[0] - size[0],
			y = coords[1] + vector[1] - size[1],
			x1 = coords[0] + vector[0] + size[0],
			y1 = coords[1] + vector[1] + size[1];
		
		var cells = [(parseInt(x / 32) + parseInt(y / 32) * this.size[0]),
		             (parseInt(x1 / 32) + parseInt(y / 32) * this.size[0]),
		             (parseInt(x / 32) + parseInt(y1 / 32) * this.size[0]),
		             (parseInt(x1 / 32) + parseInt(y1 / 32) * this.size[0])];
		
		if (DEBUG){
			draw.rect(display, '#00fffa', new gamejs.Rect([viewport_offset[0] + x, viewport_offset[1] + y], size), 1);
			
			var cl = this.numToPos(cells[0]);
			draw.rect(display, this.sheet.charAt(cells[0])!=0?'#ff0000':'#00ff00', new gamejs.Rect([viewport_offset[0] + cl[0], viewport_offset[1] + cl[1]], [32,32]), 1);
			cl = this.numToPos(cells[1]);
			draw.rect(display, this.sheet.charAt(cells[1])!=0?'#ff0000':'#00ff00', new gamejs.Rect([viewport_offset[0] + cl[0], viewport_offset[1] + cl[1]], [32,32]), 1);
			cl = this.numToPos(cells[2]);
			draw.rect(display, this.sheet.charAt(cells[2])!=0?'#ff0000':'#00ff00', new gamejs.Rect([viewport_offset[0] + cl[0], viewport_offset[1] + cl[1]], [32,32]), 1);
			cl = this.numToPos(cells[3]);
			draw.rect(display, this.sheet.charAt(cells[3])!=0?'#ff0000':'#00ff00', new gamejs.Rect([viewport_offset[0] + cl[0], viewport_offset[1] + cl[1]], [32,32]), 1);
		}
		
		if (this.sheet.charAt(cells[0]) == 0 && this.sheet.charAt(cells[1]) == 0 && this.sheet.charAt(cells[2]) == 0 && this.sheet.charAt(cells[3]) == 0 && x > 0 && y > 0 && x1 < this.size[0] && y1 < this.size[1])
			return false;
		
		var count = 0,
			lost_cell = null;
		for(i in cells){
			if (this.sheet.charAt(cells[i]) != 0){
				count++;
				lost_cell = cells[i];
			}
		}
		
		var left_top = false,
			right_top = false,
			left_bottom = false,
			right_bottom = false;
		
		if (this.sheet.charAt(cells[0]) != 0) left_top = true;
		if (this.sheet.charAt(cells[1]) != 0) right_top = true;
		if (this.sheet.charAt(cells[2]) != 0) left_bottom = true;
		if (this.sheet.charAt(cells[3]) != 0) right_bottom = true;
		
		if (x < 0){
			left_top = true;
			left_bottom = true;
		}
		if (x1 > this.size[0] * 32){
			right_top = true;
			right_bottom = true;
		}
		if (y < 0){
			right_top = true;
			left_top = true;
		}
		if (y1 > this.size[1] * 32){
			left_bottom = true;
			right_bottom = true;
		}
		
		
		var vertical = false,
			horizontal = false;
		
		if (left_top && left_bottom || right_top && right_bottom)
			vertical = true;
		if (left_top && right_top || left_bottom && right_bottom)
			horizontal = true;
		
		if (right_top && left_bottom || left_top && right_bottom){
			horizontal = true;
			vertical = true;
		}
		
		
		if (!vertical && !horizontal && count == 1){
			var row = Math.floor(lost_cell / this.size[0]),
				col = lost_cell - (row * this.size[0]);
			var _x = col * 32,
				_y = row * 32,
				_x1 = _x + 32,
				_y1 = _y + 32;
			
			var distance = 0;
			
			if (left_top){
				var dist_x = Math.abs(_x1 - x),
					dist_y = Math.abs(_y1 - y);
				
				if (dist_x < distance && dist_y < distance){
					vertical = false;
					horizontal = false;
				}
				else if (dist_x > dist_y && vector[0] <= 0 && vector[1] < 0)
					horizontal = true;
				else if (dist_x > dist_y && vector[0] > 0 && vector[1] < 0)
					horizontal = true;
				else if (dist_x < dist_y && vector[0] < 0 && vector[1] >= 0)
					vertical = true;
				else if (dist_x < dist_y && vector[0] < 0 && vector[1] < 0)
					vertical = true;
				
			}
			else if (right_top){
				var dist_x = Math.floor(Math.abs(x1 - _x)),
					dist_y = Math.floor(Math.abs(_y1 - y));
				
				if (dist_x < distance && dist_y < distance){
					vertical = false;
					horizontal = false;
				}
				else if (dist_x > dist_y && vector[0] <= 0 && vector[1] < 0)
					horizontal = true;
				else if (dist_x < dist_y && vector[0] > 0 && vector[1] > 0)
					vertical = true;
				else if (dist_x < dist_y && vector[0] > 0 && vector[1] <= 0)
					vertical = true;
				else if (dist_x > dist_y && vector[0] > 0 && vector[1] < 0)
					horizontal = true;
			}
			else if (left_bottom){
				var dist_x = Math.abs(_x1 - x),
					dist_y = Math.abs(_y - y1);
				
				if (dist_x < distance && dist_y < distance){
					vertical = false;
					horizontal = false;
				}
				else if (dist_x > dist_y && vector[0] <= 0 && vector[1] > 0)
					horizontal = true;
				else if (dist_x > dist_y && vector[0] > 0 && vector[1] > 0)
					horizontal = true;
				else if (dist_x < dist_y && vector[0] < 0 && vector[1] <= 0)
					vertical = true;
				else if (dist_x < dist_y && vector[0] < 0 && vector[1] > 0)
					vertical = true;
			}
			else if (right_bottom){
				var dist_x = Math.abs(x1 - _x),
					dist_y = Math.abs(_y - y1);
				
				if (dist_x < distance && dist_y < distance){
					vertical = false;
					horizontal = false;
				}
				else if (dist_x > dist_y && vector[0] <= 0 && vector[1] > 0)
					horizontal = true;
				else if (dist_x > dist_y && vector[0] > 0 && vector[1] > 0)
					horizontal = true;
				else if (dist_x < dist_y && vector[0] > 0 && vector[1] <= 0)
					vertical = true;
				else if (dist_x < dist_y && vector[0] > 0 && vector[1] > 0)
					vertical = true;
			}
		}
		
		if (vertical && horizontal)
			return 'both';
		else if (vertical && !horizontal)
			return 'vertical';
		else if (!vertical && horizontal)
			return 'horizontal';
		else return false;
		
	};
	
	this.mapSimpleCollider = function(coords, size, vector){
		var x = coords[0] + (size[0] / 2) + vector[0],
			y = coords[1] + (size[1] / 2) + vector[1];
		var cell = (parseInt(x / 32) + parseInt(y / 32) * this.size[0]);
		return this.sheet.charAt(cell) == 1;
	};
	
	this.mapCheckPixel = function(pixel){
		var coords = [Math.floor(pixel[0] / 32), Math.floor(pixel[1] / 32)];
		if (coords[0] >= this.size[0] || coords[0] < 0 || coords[1] < 0 || coords[1] >= this.size[1])
			return true;
		return this.sheet.charAt(this.coordsToNum(coords)) == 1;
	};
	
	this.getCellStatus = function(direction, col, row, ret){
		if (typeof ret == "undefined")
			var ret = false;
		var search;
		switch(direction){
			case 'top' : {
				if (row <= 0)
					return 1;
				search = (row - 1) * this.size[0] + col;
				break;
			}
			case 'bottom' : {
				if (row >= this.size[1]-1)
					return 1;
				search = (row + 1) * this.size[0] + col;
				break;
			}
			case 'left' : {
				if (col <= 0)
					return 1;
				search = row * this.size[0] + (col - 1);
				break;
			}
			case 'right' : {
				if (row >= this.size[0]-1)
					return 1;
				search = row * this.size[0] + (col + 1);
				break;
			}
			default :search = 0;
		}
		return ret ? parseInt(search) : this.sheet.charAt(parseInt(search));
	};
	
	this.coordsToNum = function(coords){
		return coords[0] + coords[1] * this.size[0];
	};
	
	this.numToCoords = function(num){
		var row = Math.floor(num / this.size[0]);
		return [num - (row * this.size[0]), row];
	};
	
	this.numToPos = function(num){
		var c = this.numToCoords(num);
		return [c[0] * 32, c[1] * 32];
	}
	
	this.isNum = function(num){
		return typeof num === "number";
	};
	
	this.isCoords = function(coords){
		return typeof coords === "object";
	};
	
	this.getDistance = function(cell1, cell2, rnd){
		var x, y, x1, y1;
		if (this.isNum(cell1)){
			var c = this.numToCoords(cell1);
			x = c[0];y = c[1];
		}
		else if (this.isCoords(cell1)){
			x = cell1[0];y = cell1[1];
		}
		else return null;
		
		if (this.isNum(cell2)){
			var c = this.numToCoords(cell2);
			x1 = c[0];y1 = c[1];
		}
		else if (this.isCoords(cell2)){
			x1 = cell2[0];y1 = cell2[1];
		}
		else return null;
		
		var len_x = x1 - x,
			len_y = y1 - y;
		
		return (typeof rnd != "undefined" && rnd) ? Math.round(Math.sqrt(len_x*len_x + len_y*len_y)) : Math.sqrt(len_x*len_x + len_y*len_y);
	};
	
	/**
	 * Szukanie najblizej polozonego itemu na mapie
	 */
	this.getClosestItem = function(current_cell){
		var distance = 999999,
			item_index = null;
		for(i in this.items){
			var d = this.getDistance(current_cell, this.items[i].coords, true);
			if (d < distance){
				item_index = i;
				distance = d;
			}
		}
		return item_index !== null ? {
			item: this.items[item_index],
			distance: distance,
			index: item_index
		} : false;
	};
	
	/**
	 * Poszukiwanie najkrotszej drogi z punktu cell_from do cell_to (na podstawie algorytmu Dijkstry)
	 */
	this.searchWay = function(cell_from, cell_to, stage, marked_map){
		var _cell_from, _cell_to;
		
		/* Przygotowanie wspolrzednych */
		_cell_from = this.getCleanCoords(cell_from);
		_cell_to = this.getCleanCoords(cell_to);
		
		if (_cell_from === null || _cell_to === null)
			return false;
		
		//console.log(_cell_from, _cell_to);
		
		/* Poszukiwania drogi realizowane jest w kolejnych etapach, 
		   jest to proba zoptymalizowania algorytmu dzielac go na czesci */
		
		switch(stage){
			case 1 : { // tworzenie tablicy, w ktorej bedzie trzymane info o polach
				marked_map = new Array();
				for(var i=0; i<this.size[0]; i++){
					for(var j=0; j<this.size[1]; j++)
						marked_map[j*this.size[0]+i] = null;
				}
				this.marked_cell_counter = 0;
				marked_map[this.coordsToNum(_cell_from)] = 0;
				return marked_map;
			}
			case 2 : { // oznaczanie pol - czesc I
				var safe_counter = max_marked_cycles;
				while (marked_map[this.coordsToNum(_cell_to)] == null){
					if (this.marked_cell_counter > safe_counter)
						return false;
					var array = this.arrayFieldsWhere(this.marked_cell_counter, marked_map);
					this.marked_cell_counter++;
					if (array.length){
						for(var l in array){
							marked_map = this.tagNeighborFields(array[l], this.marked_cell_counter, marked_map);
						}
					}
				}
				return marked_map;
			}
			case 3 : {
				if (marked_map[this.coordsToNum(_cell_to)] != null){
					var first = marked_map[this.coordsToNum(_cell_to)];
					var _cell = this.coordsToNum(_cell_to);
					var target = new Array();
					var f = false;
					while (first > 0){
						first--;
						f = false;
						for(i=0; i<=7; i++){
							var nf = parseInt(_cell) + parseInt(cells_around_point[i]);
							if (parseInt(marked_map[nf]) == first - 1 && marked_map[nf] >= 0){
								var ni = (i==0)?7:i-1,
									ni2 = (i==7)?0:i+1;
								var c1 = parseInt(_cell) + parseInt(cells_around_point[ni]),
									c2 = parseInt(_cell) + parseInt(cells_around_point[ni2]);
								if (parseInt(marked_map[c1]) != -1 && parseInt(marked_map[c2]) != -1){
									target.push(nf);
									f = true;
									_cell = nf;
									first--;
									break;
								}
							}
						}
						if (f)
							continue;
						
						for(i=0; i<=7; i++){
							var nf = parseInt(_cell) + parseInt(cells_around_point[i]);
							if (parseInt(marked_map[nf]) != first)
								continue;
							if (parseInt(marked_map[nf]) == first){
								target.push(nf);
								_cell = nf;
								if (i == 0 || i == 2 || i == 4 || i == 6)
									break;
							}
						}
					}
					target.reverse();
					target.push(this.coordsToNum(_cell_to));
					target.shift();
					return target;
				}
			}
		}
	};
	
	this.tagNeighborFields = function(field, count, marked_map){
		for(var i=0; i<=7; i+=2){
			var nf = field + cells_around_point[i];
			if (marked_map[nf] !== null)
				continue;
			if (this.sheet.charAt(nf) != 0)
				marked_map[nf] = -1;
			else marked_map[nf] = count;
		}
		return marked_map;
	};
	
	this.getCleanCoords = function(cell){
		if (this.isNum(cell))
			return this.numToCoords(cell);
		else if (this.isCoords(cell))
			return cell;
		else return null;
	};
	
	this.arrayFieldsWhere = function(index, marked_map){
		var _tmp_array = new Array();
		for(var i=0; i<=this.size[0]-1; i++)
			for(var l=0; l<=this.size[1]-1; l++){
				var cell = this.coordsToNum([i, l]);
				if (marked_map[cell] == index)
					_tmp_array.push(cell);
			}
		return _tmp_array;
	};
	
	
	
	this.easierTool = function(display, m, line){
		var easierFont = new gamejs.font.Font('14px');
		for(var i=0; i<this.size[0]; i++)
			display.blit(easierFont.render(m[line*this.size[0]+i], '#000000'), [viewport_offset[0] + i*this.size[0] + 10, viewport_offset[1] + line*this.size[0]]);
	};
};

gamejs.utils.objects.extend(Map, gamejs.sprite.Sprite);
exports.Map = Map;