;/***
 * GUI library
 */

var load_files = [
                  {name: 'menu', file: 'menu.html'},
                  {name: 'shop', file: 'shop.html'},
                  {name: 'stats', file: 'stats.html'},
                  {name: 'account', file: 'account_info.html'},
                  {name: 'select_level', file: 'select_level.html'}
                  
    ],
	window_count = 0,
	uid = atob(_n3a),
	button_events = new Array();


shopFunctions = function(){
    for (i in shotTypes){
        if (typeof shotTypes[i].name != "undefined"){
            $('#itemList').children('.ammo').append('<li><a href="javascript:;" id="ammo_'+i+'"><span style="font-size:11px;font-weight:bold;color:#04B486">'+shotTypes[i].shop_amount+'x</span> '+shotTypes[i].name+' <strong>[$'+shotTypes[i].shop_cost+']</strong></a></li>');
        }
    }

    for (i in shotTypes){
        if (typeof shotTypes[i].name != "undefined"){
            $('#itemList').children('.weapons').append('<li><a href="javascript:;" id="weap_'+i+'">'+shotTypes[i].name+' <strong>[$'+shotTypes[i].weapon_cost+']</strong></a></li>');
        }
    }
    
    for (i in generalShopItems){
        if (typeof generalShopItems[i].name != "undefined"){
            $('#itemList').children('.extra').append('<li><a href="javascript:;" id="shopItem_'+i+'"><span style="font-size:11px;font-weight:bold;color:#04B486">+'+generalShopItems[i].amount+'</span> '+generalShopItems[i].name+' <strong>[$'+generalShopItems[i].cost+']</strong></a></li>');
        }
    }
};

var GUI = {
	tank		: null,
	local_pause : false,
	anim_speed	: 200,
	gamejs_obj  : null,
	display		: null,
	tmpl_dir	: _pd+'public/',
	templates	: [],
	current_fire: 0,
	window_open : false,
	finished	: false,
	
	Construct: function(gamejs, display, files){
		this.gamejs_obj = gamejs;
		this.display = display;
		for(i in files){
			this.LoadTemplates(files[i].name, files[i].file);
		}
	},
	
	LoadTemplates: function(name, file){
		$.get(this.tmpl_dir+file, function(e){
			GUI.templates.push({name: name, template: e});
		});
	},
	
	GetTemplate: function(template, translate){
		var tmp = '';
		for(i in this.templates){
			if (this.templates[i].name == template)
				tmp = this.templates[i].template;
		}
		
		if (tmp != '' && typeof translate != "undefined"){
			for(key in translate){
				tmp = tmp.replace('{%'+key+'%}', translate[key]);
			}
		}
		return tmp == '' ? null : tmp;
	},
	
	DrawFPS: function(fps){
		var instructionFont = new this.gamejs_obj.font.Font('16px');
		this.display.blit(instructionFont.render('FPS: '+fps.toFixed(1), '#ffffff'), [viewport_width-60, viewport_height+20]);
	},
	
	Draw: function(tank){
		this.tank = tank;
		var instructionFont = new this.gamejs_obj.font.Font('16px');
		var base_y = viewport_height + 20;
		var base_x = 20;
		
		menu_height = height_px - viewport_height;
		this.gamejs_obj.draw.rect(this.display, '#000000', new this.gamejs_obj.Rect([0, viewport_height], [width_px, viewport_height]));
		
		
		this.display.blit(instructionFont.render('Money: '+tank.player.GetMoney(), '#ffffff'), [base_x, base_y]);
		this.display.blit(instructionFont.render('Health: '+tank.player.health, '#ffffff'), [base_x, base_y+25]);
		this.display.blit(instructionFont.render('Armor: '+tank.player.armor, '#ffffff'), [base_x, base_y+50]);
		this.display.blit(instructionFont.render('Ammo: '+tank.player.GetAmmo(tank.current_shot_type), '#ffffff'), [base_x, base_y+75]);
		this.display.blit(instructionFont.render('Hidden: '+(tank.isHidden() ? 'TRUE' : '-'), '#ffffff'), [base_x + 400, base_y+25]);
		var _crd = tank.getCoords();
		this.display.blit(instructionFont.render('Position: x['+_crd[0]+'] y['+_crd[1]+']', '#ffffff'), [base_x + 400, base_y+50]);
		
		
		this.display.blit(instructionFont.render('Afterburner: '+parseInt(tank.player.afterburner), '#ffffff'), [base_x+400, base_y]);
		
		var weapon_name = tank.current_weapon.name;
		this.display.blit(instructionFont.render('Weapon: '+weapon_name, '#ffffff'), [base_x+200, base_y]);
		
		if (this.current_fire == 0 && this.tank.next_fire != 0){
			this.current_fire = this.tank.next_fire;
		}
		
		if (this.tank.next_fire == 0)
			this.current_fire = 0;
		
		var length = 100,
			final_length;
		var len = Math.round((this.tank.next_fire * 100) / this.current_fire);
		if (len > 0)
			final_length = length - len;
		else final_length = length;
		
		this.gamejs_obj.draw.rect(this.display, '#3D3D35', new this.gamejs_obj.Rect([base_x + 200, base_y + 40], [length+2, 7]), 1);
		this.gamejs_obj.draw.rect(this.display, '#CCCCB2', new this.gamejs_obj.Rect([base_x + 201, base_y + 41], [final_length, 5]));
	},

    Shop: function(){
        var config = {
            ok_cn_buttons: false,
            width: 700,
            height: 450,
            align: 'center'
        };
        game_pause = true;
        this.window_open = true;
        var window = CreateWindow('Shop', config);
        window.PutContent(this.GetTemplate('shop'));
        window.AddButton("Back to game", {onClick: function(){
            window.Kill();
            GUI.window_open = false;
            if (game_over){
                GUI.PlayAgainWindow();
            }
            else if (game_success){
                GUI.LevelFinished();
            }
            else game_pause = false;
        }});
        window.Show();
        shopFunctions();
        var _o = window.getObject();
        	_itemTypes = _o.find('.list-left').children('.item-list').find('a'),
        	_listRight = _o.find('.list-right');
        
        _itemTypes.on('click', function(){
        	_itemTypes.removeClass('active');
        	var _e = $(this);
        	_e.addClass('active');
        	_listRight.find('ul').addClass('disabled');
        	_listRight.find('.'+_e.attr('data-type')).removeClass('disabled');
        	
        }).trigger('click');
        
        var _this = this;
        $('#playerMoney').bind('changeAmount', function(){
        	$(this).text('$'+_this.tank.player.money);
        }).trigger('changeAmount');
        
        _listRight.find('a').on('click', function(){
        	var _p = $(this).attr('id').split('_'),
        		type = _p[0],
        		id = parseInt(_p[1]);
        	switch(type){
        		case 'ammo' : {
        			if (_this.tank.player.money >= shotTypes[id].shop_cost){
        				_this.tank.player.money -= shotTypes[id].shop_cost;
        				_this.tank.player.AddAmmo(id, shotTypes[id].shop_amount);
        			}
        			$('#playerMoney').trigger('changeAmount');
        			break;
        		}
        		case 'weap' : {
        			if (_this.tank.player.money >= shotTypes[id].weapon_cost && !_this.tank.player.GetWeapon(id)){
        				_this.tank.player.money -= shotTypes[id].weapon_cost;
        				_this.tank.player.AddWeapon(id);
        				_this.tank.player.SetAmmo(id, 0);
        			}
        			$('#playerMoney').trigger('changeAmount');
        			break;
        		}
        		case 'shopItem' : {
        			if (_this.tank.player.money >= generalShopItems[id].cost){
        				_this.tank.player.money -= generalShopItems[id].cost;
        				switch(generalShopItems[id].type){
        					case 'armor' 		: _this.tank.player.armor += generalShopItems[id].amount;break;
        					case 'afterburner' 	: _this.tank.player.afterburner += generalShopItems[id].amount;break;
        				}
        			}
        			$('#playerMoney').trigger('changeAmount');
        			break;
        		}
        		default: {
        			
        			break;
        		}
        	}
        });
        
    },

	GameOver: function(){
		this.tank.player.ResetMoney();
		game_over = true;
		var window = CreateWindow('Game Over', {ok_cn_buttons:false});
		window.PutContent('You have been killed!');
		this.window_open = true;
		window.AddButton("OK", {onClick: function(){
			window.Kill();
			GUI.window_open = false;
			GUI.PlayAgainWindow();
		}});
		window.Show();
	},
	
	AccountInfo: function(){
		game_pause = true;
		var window = CreateWindow('My account', {ok_cn_buttons:false, height:500});
		this.window_open = true;
		window.Show();
		
		$.post(atob(_ai), {_uid:uid}, function(result){
			window.PutContent(GUI.GetTemplate('account', {
				name: result.name
			}));
			window.AddButton("OK", {onClick: function(){
				GUI.window_open = false;
				if (game_over){
					window.Kill();
					GUI.PlayAgainWindow();
				}
				else if (game_success){
					window.Kill();
					if (GUI.finished){
						GUI.finished = false;
						GUI.LevelFinished();
					}
					else GUI.PlayAgainWindow();
				}
				else {
					game_pause = false;
					window.Kill();
				}
			}});
		});
		
	},
	
	Statistics: function(){
		game_pause = true;
		
		var window = CreateWindow('Current statistics', {ok_cn_buttons:false, height:400});
		
		var ratio = (playerDefaults.userStats.shots_fired > 0 && playerDefaults.userStats.shots_aimed > 0) ? ((playerDefaults.userStats.shots_aimed / playerDefaults.userStats.shots_fired) * 100).toFixed(2) : 0;
		this.window_open = true;
		window.PutContent(this.GetTemplate('stats', {
			name: playerDefaults.name,
			model: playerDefaults.model,
			kill: playerDefaults.userStats.kills,
			kia: playerDefaults.userStats.kia,
			shots: playerDefaults.userStats.shots_fired,
			aimed: playerDefaults.userStats.shots_aimed,
			money: this.tank.player.money,
			earned_money: playerDefaults.userStats.earned_money,
			level: '1',
			ratio: ratio+'%'
		}));
		
		window.AddButton("OK", {onClick: function(){
			GUI.window_open = false;
			if (game_over){
				window.Kill();
				GUI.PlayAgainWindow();
			}
			else if (game_success){
				window.Kill();
				if (this.finished){
					this.finished = false;
					GUI.LevelFinished();
				}
				else GUI.PlayAgainWindow();
			}
			else {
				game_pause = false;
				window.Kill();
			}
		}});
		window.Show();
	},
	
	PlayAgainWindow: function(){
		this.tank.player.ResetMoney();
		this.window_open = true;
		var window = CreateWindow('Play again', {ok_cn_buttons:false});
		window.PutContent('You can start again, or go to the item shop.<br /><br />What to do now?');
		window.AddButton("Play again", {onClick: function(){
			GUI.window_open = false;
			window.Kill();
			call_restart = true;
		}});
		window.AddButton("Statistics", {onClick: function(){
			GUI.window_open = false;
			window.Kill();
			GUI.Statistics();
		}});
		window.AddButton("Go shopping", {onClick: function(){
			GUI.window_open = false;
            window.Kill();
			GUI.Shop();
		}});
		window.AddButton("Select level", {onClick: function(){
			GUI.window_open = false;
			window.Kill();
			GUI.SelectLevel();
		}});
		window.Show();
	},
	
	SelectLevel: function(){
		this.window_open = true;
		var window = CreateWindow('Select level', {ok_cn_buttons:false}),
			levels = '<li><a href="javascript:;" data-id="1">Nowa mapa</a></li><li><a href="javascript:;" data-id="disabled">-unknown-</a></li><li><a href="javascript:;" data-id="disabled">-unknown-</a></li>';
		
		window.PutContent(this.GetTemplate('select_level', {
			level_list: levels
		}));
		
		window.AddButton("Select", {onClick: function(){
			if ($('.select-level a.active').attr('data-id') != 'disabled'){
				GUI.window_open = false;
				game_pause = false;
	            global_current_map = parseInt($('.select-level a.active').attr('data-id'));
	            window.Kill();
	            call_continue = true;
			}
		}});
		window.Show();
		
		$('.select-level a').on('click', function(){
			$('.select-level a').removeClass('active');
			$(this).addClass('active');
		});
		
		$('.select-level a').on('dblclick', function(){
			if ($('.select-level a.active').attr('data-id') != 'disabled'){
				GUI.window_open = false;
				game_pause = false;
	            global_current_map = parseInt($('.select-level a.active').attr('data-id'));
	            window.Kill();
	            call_continue = true;
			}
		});
	},
	
	
	/**
	 * 
	 * Ladowanie i zapisywanie gry
	 */
	GameSave: {
		wnd				: null,
		type			: '',
		
		CallWindow: function(type){
			GUI.window_open = true;
			this.type = type;
			config = ('select' == type) ? {
					ok_cn_buttons: false,
					width: 600,
					height: 400,
					top: 50,
					left: 50,
					align: 'free'
				} : {
					ok_cn_buttons: false,
					width: 600,
					height: 320,
					align: 'center'
				};
			
			this.wnd = CreateWindow('save'==type?'Save game':'Load game', config);
			this.SetContent();
		},
		
		SetContent: function(){
			var content = 'load' == this.type ? '<ul class="gjs-field" id="gjsGSItems">'+
					'</ul>' : '<ul class="gjs-field" id="gjsGSItems">'+
					'</ul>'+
					'<hr />'+
					'<input type="text" id="gjsGSEnterFileName" maxlength="32" />';
			this.wnd.PutContent(content);
			this.AddButtons();
			this.wnd.Show();
		},
		
		AddButtons: function(){
			this.wnd.AddButton("Load", {onClick: function(){
				GUI.GameSave.Load();
				// ladowanie pliku
				GUI.window_open = false;
				GUI.GameSave.wnd.Kill();
			}});
			this.wnd.AddButton("Cancel", {onClick: function(){
				GUI.window_open = false;
				if (game_over);
					GUI.PlayAgainWindow();
				GUI.GameSave.wnd.Kill();
			}});
		}
	},

    LevelFinished: function(){
    	this.tank.player.AddMoney(map_finish_prize);
    	this.tank.player.ReceiveMoney();
    	this.tank.player.ResetMoney();
        var window = CreateWindow('Level finished!', {ok_cn_buttons:false});
        this.window_open = true;
        window.PutContent('You received additional '+map_finish_prize+' for this level!');
        if (global_current_map + 1 <= test_map_schema.length){
            window.AddButton("Next level", {onClick: function(){
            	GUI.window_open = false;
                game_pause = false;
                global_current_map++;
                window.Kill();
                call_continue = true;
            }});
        }
        window.AddButton("Play again", {onClick: function(){
        	GUI.window_open = false;
            window.Kill();
            game_pause = false;
            call_continue = true;
        }});
        window.AddButton("Go shopping", {onClick: function(){
        	GUI.window_open = false;
            window.Kill();
            GUI.Shop();
        }});
        window.AddButton("Select another level", {onClick: function(){
        	GUI.window_open = false;
            window.Kill();
            GUI.SelectLevel();
        }});
        window.AddButton("Statistics", {onClick: function(){
        	GUI.window_open = false;
        	this.finished = true;
			window.Kill();
			GUI.Statistics();
		}});
        window.Show();
    },
	
	TogglePause: function(){
		if (this.window_open)
			return;
		if (game_pause)
			this.UnPause();
		else this.Pause();
	},
	
	Pause: function(){
		game_pause = true;
		
		$('.container').append('<div id="gjsPopupPause" class="gjs-popup gjs-pause">Game paused<span class="gjs-bg"></span></div>');
		$('#gjsPopupPause').animate({'opacity':1}, this.anim_speed);
		$('.container').append(this.GetTemplate('menu'));
		$('#gjsMenu').animate({'right':'-3px'}, this.anim_speed, function(){
			
		});
	},
	
	UnPause: function(){
		game_pause = false;
		$('#gjsPopupPause').animate({'opacity':0}, this.anim_speed, function(){
			$(this).remove();
		});
		$('#gjsMenu').animate({'right':'-320px'}, this.anim_speed, function(){
			$(this).remove();
		});
		
		
	},
	
	SavePause: function(){
		this.local_pause = true;
	},
	
	ReleasePause: function(){
		if (!this.local_pause)
			return;
		this.local_pause = false;
		this.TogglePause();
	},
	
	EventHandler: function(){
		$(document).on('click', '.main-menu-button', function(){
			var id = $(this).attr('id');
			id = id.replace('gjsMain', '').toLowerCase();
			switch(id){
				case 'restart' : {
					call_restart = true;
					GUI.UnPause();
					break;
				}
				case 'selectlevel' : {
					GUI.UnPause();
					game_pause = true;
					GUI.SelectLevel();
					break;
				}
				case 'shop' : {
                    GUI.UnPause();
                    GUI.Shop();
					break;
				}
				case 'stats' : {
					GUI.UnPause();
					GUI.Statistics();
					break;
				}
				case 'account' : {
					GUI.UnPause();
					GUI.AccountInfo();
					break;
				}
			}
		});
	}
};


var gjsWindow = function(title, id, settings){
	var defaults = {
		width			: 400,
		height			: 300,
		top				: 0,
		left			: 0,
		align			: "center",
		fade_speed		: 200,
		show_inputs		: true,
		ok_cn_buttons	: true,
		callback		: function(){}
	};
	this.bt_defaults = {
		bt_class		: 'gjs-button',
		onClick			: null,
		onHover			: null
	};
	this.settings = $.extend({}, defaults, settings);
	
	this.title = title;
	this.window = '';
	this.id = id;
	this.initialized = false;
	this.buttons = [];
	
	this.Build = function(){
		this.window = this.MakeWindow();
		return this;
	};
	
	this.getObject = function(){
		return $('#gjsWindow_'+this.id);
	};
	
	this.MakeWindow = function(){
		var w = this.settings.width,
			h = this.settings.height,
			add = '';
		if ("center" == this.settings.align){
			var mt = Math.floor(h / 2);
			var ml = Math.floor(w / 2);
			add = ';margin-top:-'+mt+'px;margin-left:-'+ml+'px;top:40%;left:50%';
		} else {
			add = ';margin:0;top:'+this.settings.top+'px;left:'+this.settings.left+'px';
		}
		
		var template = 
				'<div class="gjs-window" id="gjsWindow_'+this.id+'" style="display:none;width:'+w+'px;height:'+h+'px'+add+'">'+
					'<h2>##title##</h2>'+
					'##content##'+
					'##inputs##'+
					'<div class="gjs-bg"></div>'+
				'</div>',
			inputs = 
				'<hr /><div id="gjsExtBtns_'+this.id+'"></div>';
		if (this.settings.ok_cn_buttons)
			inputs += '<a href="javascript:;" id="gjsBtOk_'+this.id+'" class="gjs-button gjs-window-ok">Ok</a><a href="javascript:;" id="gjsBtCn_'+this.id+'" class="gjs-button gjs-window-close">Close</a>';
		var output = template.replace('##inputs##', this.settings.show_inputs?inputs:'');
		output = output.replace('##title##', this.title);
		return output;
	};
	
	this.PutContent = function(content){
		this.window = this.window.replace('##content##', content);
		if (this.initialized){
			var e = $('#gjsWindow_'+this.id).html();
			e = e.replace('##content##', content);
			$('#gjsWindow_'+this.id).html(e);
		}
		return this;
	};
	
	this.AddButton = function(name, settings){
		var st = $.extend({}, this.bt_defaults, settings);
		this.buttons.push({name: name, settings: st});
		if (this.initialized){
			this.InitButtons();
			this.Listener();
		}
	};
	
	this.InitButtons = function(){
		var bt = '';
		button_events[this.id] = new Array();
		for(i in this.buttons){
			var b = this.buttons[i];
			bt += '<a href="javascript:;" class="'+b.settings.bt_class+' gjs-add-bt" id="gjsExtBt_'+this.id+'_'+i+'">'+b.name+'</a>';
			button_events[this.id][i] = this.buttons[i];
		}
		$('#gjsExtBtns_'+this.id).html(bt);
	};
	
	this.Init = function(){
		if (!$('#gjsWindow_'+this.id).length){
			$('.container').append(this.window);
			this.InitButtons();
			this.initialized = true;
		}
	};
	
	this.Show = function(){
		if (!this.initialized)
			this.Init();
		$('#gjsWindow_'+this.id).fadeIn(this.settings.fade_speed);
		this.Listener();
	};
	
	this.Hide = function(){
		$('#gjsWindow_'+this.id).fadeOut(this.settings.fade_speed);
	};
	
	this.Kill = function(){
		$('#gjsWindow_'+this.id).remove();
	};
	
	this.Listener = function(){
		var _this = this;
		
		$('#gjsBtOk_'+this.id).off('click');
		$('#gjsBtOk_'+this.id).bind('click', function(){
			_this.Hide();
			_this.settings.callback(_this, 'ok');
			_this.Kill();
		});
		
		$('#gjsBtCn_'+this.id).off('click');
		$('#gjsBtCn_'+this.id).bind('click', function(){
			_this.Hide();
			_this.settings.callback(_this, 'cancel');
			_this.Kill();
		});
		
		$('.gjs-add-bt').unbind('click hover');
		$('.gjs-add-bt').bind('click', function(){
			var id = $(this).attr('id');
			id = id.replace('gjsExtBt_', '');
			var nums = id.split('_');
			var data = button_events[parseInt(nums[0])][parseInt(nums[1])];
			if (data.settings.onClick != null)
				data.settings.onClick();
		});
		$('.gjs-add-bt').bind('hover', function(){
			var id = $(this).attr('id');
			id = id.replace('gjsExtBt_', '');
			var nums = id.split('_');
			var data = button_events[parseInt(nums[0])][parseInt(nums[1])];
			if (data.settings.onHover != null)
				data.settings.onHover();
		});
		
	};
};

function CreateWindow(title, settings){
	var window = Object.create(new gjsWindow(title, ++window_count, settings || {}));
	window.Build();
	return window;
};


