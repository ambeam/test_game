var gamejs = require('gamejs');
var soundPlayer = require('gamejs/mixer');

/********** ENEMY CLASS
 * 
 */

var RAD_CONST = 0.017453292519943295,
	_edef = {
		search_close_pos_interval: 33,
		tolerance: 1,
		pause_secure_step_count: 50
	};

var display = gamejs.display.getSurface();

var Enemy = function(data, weapon, id, patrol){
	Enemy.superConstructor.apply(this, arguments);
	
	//this.weapon = Object.create(weapon);
	this.weapon = weapon;
	
	this.id = id;
	this.x = data.x * 32 + 4;
	this.y = data.y * 32;
	this.width_px = data.width_px;
	this.height_px = data.height_px;
	this.angle = 0;
	this.prev = this.angle;
	this.vector = [0, 0];
	
	this.patrol_points = new Array();
    this.patrol_player_sight_distance = data.sight_distance || defaultEnemy.sight_distance;

	this.type = data.type;
	
	//this.settings = Object.create(enemyTypes[data.type]);
	this.settings = enemyTypes[data.type];
	
	this.strength = this.settings.strength;
	this.life_time = 0;
	
	this.originalImage = gamejs.image.load(this.settings.image);
	this.originalShad = gamejs.image.load(this.settings.shadow);
	this.originalImage = gamejs.transform.scale(this.originalImage, this.settings.size);
	this.originalShad = gamejs.transform.scale(this.originalShad, this.settings.size);
	
	this.image = gamejs.transform.rotate(this.originalImage, this.angle);
	this.shad = gamejs.transform.rotate(this.originalShad, this.angle);
	
	this.size = this.originalImage.getSize();
	
	this.x2 = this.x + this.size[0];
	this.y2 = this.y + this.size[1];
	this.rect = new gamejs.Rect([this.x, this.y], this.size);
	
	this.steps = [];
	this.current_step = 0;
	this.secure_step_count = 0;
	
	this.mode = data.mode || defaultEnemy.default_mode;
	this.mode_settings = null;
	this.force_mode = '';
	this.change_mode = false;
	
	this.target_cell = null;
	this.turn = false;
	this.turn_steps = 0;
	this.turn_dir = 1;
	this.search_way_stage = 1;
	this.marked_map = [];
	this.step_count = 0;
	this.current_search = null;
	this.current_search_coords = [];
	this.switch_step = false;
	this.force_switch_step = false;
	this.current_search_distance = 0;
	this.save_angle = null;
    this.patrol_index = 0;
    this.player_seen = 0;
    
    this.prop_dist = Math.floor(this.settings.sight_distance);
	
	this.pause_move = false;			                        // zatrzymuje ruch jednostka przez okreslony czas (np zajete pole przez inna jednostke)
	this.changeActualPos = false;								// informuje o zmianie pozycji ze wzgledu na ciagle strzelanie w przeszkode zamiast w gracza
	
	this.current_search_delta = defaultEnemy.search_delta;
	this.shooting = false;
	this._shot = false;
	
	this.calibrate = [1, 1];
	
	
	this._missed = 0;
	
	this.PathNode = function(coords){
		this.coords = getCleanCoords(coords);
		
		this.getCoords = function(){
			return this.coords;
		};
		
		this.getCell = function(){
			return coordsToNum(this.coords);
		};
		
	};
	
	this.getMode = function(mode){
		for(i in enemyModes){
			if (enemyModes[i].name == mode){
				this.mode_settings = enemyModes[i];
				this.mode = mode;
				return true;
			}
		}
		return false;
	};
	
	this.getMode(this.mode);
	
	this.getCoords = function(){
		return [Math.floor((this.x + this.size[0] / 2) / 32), Math.floor((this.y + this.size[1] / 2) / 32)];
	};
	
	this.changeMode = function(mode){
		if (typeof(mode) != "undefined"){
			this.getMode(mode);
			this.force_mode = '';
		}
		else{
			var n = Math.floor((Math.random()*3)+1);
			if (this.mode == 'Passive'){
				if (n == 1 || n == 2)
					this.getMode('Tactical');
				else if (n == 3)
					this.getMode('Aggresive');
			}
			else if (this.mode == 'Noob'){
				if (n == 1 || n == 2)
					this.getMode('Passive');
				else if (n == 3)
					this.getMode('Tactical');
			}
			else if (this.mode == 'Tactical' || this.mode == 'Aggresive'){
				if (n == 1 || n == 2)
					this.getMode('Passive');
				else if (n == 3)
					this.getMode('Noob');
			}
			else {
				
			}
		}
	};
	
	/**
	 * Szukanie wolnych pol wokol gracza w okreslonym dystansie
	 */
	this.searchForAvailableFields = function(coords, map, distance){
		var min_x = coords[0] - distance, min_y = coords[1] - distance,
			max_x = coords[0] + distance, max_y = coords[1] + distance;
		var available = [];
		for (var i = min_x; i <= max_x; i++){
			if (i < 0 || i > map.size[0]-1) continue;
			
			if (min_y >= 0){
				if (map.sheet.charAt(coordsToNum([i, min_y])) == 0)
					available.push([i, min_y]);
			}
			if (max_y <= map.size[1]-1){
				if (map.sheet.charAt(coordsToNum([i, max_y])) == 0)
					available.push([i, max_y]);
			}
		}
		for (var i = (min_y+1); i <= (max_y-1); i++){
			if (i < 0 || i > map.size[1]-1) continue;
			if (min_x >= 0){
				if (map.sheet.charAt(coordsToNum([min_x, i])) == 0)
					available.push([min_x, i]);
			}
			if (max_x <= map.size[0]-1){
				if (map.sheet.charAt(coordsToNum([max_x, i])) == 0)
					available.push([max_x, i]);
			}
		}
		return available;
	};
	
	/**
	 * Wybor najdogodniejszej pozycji do oddania strzalu
	 */
	this.findCloseCoordsToPlayer = function(map, player, distance){
		if (!player.isDead()){
			var array = this.searchForAvailableFields(player.getCoords(), map, distance);
			if (array.length == 0){
				n = 0;
				while(array.length == 0){
					n++;
					array = this.searchForAvailableFields(player.getCoords(), map, distance + n);
					if (n > distance + 4)
						break;
				}
				if (array.length == 0){
					if (DEBUG){
						console.log(array, array.length);
						throw new Error('Nie mozna znalezc pozycji w poblizu playera..');
					}
					else {
						array = this.searchForAvailableFields(player.getCoords(), map, 1);
					}
				}
			}
			
			var len = 2000, index = null;
			for(i in array){
				if (len > getAbsoluteDistance(coordsToPixels(this.getCoords()), coordsToPixels(array[i]))){
					len = getAbsoluteDistance(coordsToPixels(this.getCoords()), coordsToPixels(array[i]));
				}
			}
			var results = [];
			
			for(i in array){
				if (len + _edef.search_close_pos_interval > getAbsoluteDistance(coordsToPixels(this.getCoords()), coordsToPixels(array[i]))){
					results.push(i);
				}
			}
			index = rand(0, results.length - 1);
			
			return array[results[index]];
		}
		else return false;
	};
	
	this.addPatrolPoint = function(point){
		this.patrol_points.push(Object.create(new this.PathNode(point)));
	};

    if (typeof patrol != "undefined"){
        for(i in patrol){
            this.addPatrolPoint(patrol[i]);
        }
    }

	
	/**
	 * Szukanie drogi do kolejnego punktu.
	 * Zapisuje wyznaczona trase do 'this.steps'
	 */
	this.searchWay = function(current, map, coords){
		this.search_way_stage = 1;
		this.current_search_coords = coords;
		this.marked_map = map.searchWay(current, coords, this.search_way_stage, this.marked_map);			
		//console.log('STAGE 1: ', this.marked_map);
		this.search_way_stage = 2;
		this.marked_map = map.searchWay(current, coords, this.search_way_stage, this.marked_map);
		//console.log('STAGE 2: ', this.marked_map);
		this.search_way_stage = 3;
		this.steps = map.searchWay(current, coords, this.search_way_stage, this.marked_map);
		//console.log('STAGE 3: ', this.steps);
		this.current_step = 0;
		this.step_count = this.steps.length - 1;
	};
	
	this.switchStep = function(map, player, proposed_distance){
		var prop_dist = proposed_distance || 4;
		var closest_item = map.getClosestItem([Math.floor(this.x / 32), Math.floor(this.y/32)]);
		
		if (this.player_seen == 2){
        	//console.log(this.mode_settings.patrol);
        }
		var player_distance = map.getDistance(player.getCoords(), [Math.floor(this.x / 32), Math.floor(this.y/32)]);
		if (prop_dist > parseInt(player_distance)){
			prop_dist = parseInt(player_distance);
		}
		
        if (this.mode_settings.patrol == 1 && this.patrol_points.length){
            this.patrol_index++;
            if (this.patrol_index > this.patrol_points.length - 1)
                this.patrol_index = 0;
            this.current_search_coords = this.patrol_points[this.patrol_index].getCoords();
            this.current_search = 'pathnode';
            if (this.mode_settings.shooting_mode == 2 && player_distance <= this.patrol_player_sight_distance && !player.isHidden()){
				var _toSearch = this.findCloseCoordsToPlayer(map, player, prop_dist);
				if (_toSearch != null && typeof _toSearch != "undefined"){
					this.current_search_coords = _toSearch;
					this.current_search = 'player';
					this.current_search_distance = map.getDistance(this.current_search_coords, player.getCoords());
				}
            }
			else if (player.isHidden()){
				if (this.player_seen == 1){
					var _toSearch = this.findCloseCoordsToPlayer(map, player, prop_dist);
					if (_toSearch != null && typeof _toSearch != "undefined"){
						this.current_search_coords = _toSearch;
						this.current_search = 'player';
						this.current_search_distance = map.getDistance(this.current_search_coords, player.getCoords());
						this.player_seen = 2;
					}
					else this.player_seen = 0;
				}
				else {
					this.current_search_coords = this.patrol_points[this.patrol_index].getCoords();
		            this.current_search = 'pathnode';
				}
			}
        }
		else if (((this.mode_settings.aggresive == 1 || this.mode_settings.berserk == 1) && this.mode_settings.items != 1) || !closest_item){
			var _toSearch = this.findCloseCoordsToPlayer(map, player, prop_dist);
			if (_toSearch != null && typeof _toSearch != "undefined" && !player.isHidden()){
				this.current_search_coords = _toSearch;
				this.current_search = 'player';
				this.current_search_distance = map.getDistance(this.current_search_coords, player.getCoords());
			}
		}
		else if (this.mode_settings.items == 1 && this.mode_settings.aggresive != 1 && closest_item != false){
			this.current_search_coords = closest_item.item.coords;
			this.current_search = 'item';
		}
		else if (this.mode_settings.items == 1 && this.mode_settings.aggresive == 1){
			if (map.getDistance(player.getCoords(), [Math.floor(this.x / 32), Math.floor(this.y/32)]) < map.getDistance(closest_item.item.coords, [Math.floor(this.x / 32), Math.floor(this.y/32)]) && !player.isHidden()){
				var _toSearch = this.findCloseCoordsToPlayer(map, player, prop_dist);
				if (_toSearch != null && typeof _toSearch != "undefined"){
					this.current_search_coords = _toSearch;
					this.current_search = 'player';
					this.current_search_distance = map.getDistance(this.current_search_coords, player.getCoords());
				}
				else{
					this.current_search_coords = closest_item.item.coords;
					this.current_search = 'item';
				}
			}
			else{
				this.current_search_coords = closest_item.item.coords;
				this.current_search = 'item';
			}
		}
		else{
			this.current_search_coords = [];
			this.current_search = 'stop';
		}
	};
	
	/**
	 * Wyznacza kolejny punkt drogi a takze odpowiada za zmiane trasy do innego punktu.
	 */
	this.nextStep = function(map, items, player, other_enemies){
		var _prev_cell = this.steps[this.current_step];
		this.switch_step = false;

        var player_distance = map.getDistance(player.getCoords(), [Math.floor(this.x / 32), Math.floor(this.y/32)]);
        if (this.force_switch_step || (this.mode_settings.shooting_mode == 2 && player_distance <= this.patrol_player_sight_distance && !player.isHidden())){
        	if (this.mode_settings.shooting_mode == 2 && player_distance <= this.patrol_player_sight_distance && !player.isHidden())
        		this.player_seen = 1;
			this.switch_step = true;
			this.force_switch_step = false;
		}
		
		this.current_step++;
		
		if (this.current_step >= this.steps.length){
			if (this.current_search == 'player'){
				var dist = map.getDistance(player.getCoords(), [Math.floor(this.x / 32), Math.floor(this.y/32)]);
				if ((dist > this.current_search_distance + this.current_search_delta) || this.changeActualPos){
					this.switch_step = true;
					this.shooting = false;
					this.changeActualPos = false;
				}
				else if (!player.isHidden()){
					if (dist <= this.current_search_distance + this.current_search_delta && !this.turn){
						this.shooting = true;
					}
					else this.shooting = false;
					/*else if (!this.turn){
						this.shooting = true;
					}*/
				}
				else if (player.isHidden()){
					this.current_search == 'pathnode';
					this.switch_step = true;
				}
			}
			else {
				this.switch_step = true;
				this.current_search = 'pathnode';
			}
		}
		
		if (this.switch_step){
			this.switchStep(map, player, this.prop_dist);
			this.switch_step = false;
			this.searchWay([Math.floor(this.x / 32), Math.floor(this.y / 32)], map, this.current_search_coords);
			/*console.log(this.steps);
			if (this.steps.length == 0){
				if (this.current_step > 1)
					this.current_step--;
				this.switchStep(map, player, ++this.prop_dist);
				console.log(this.prop_dist);
				this.searchWay([Math.floor(this.x / 32), Math.floor(this.y / 32)], map, this.current_search_coords);
				this.nextStep(map, items, player, other_enemies);
			}
			else this.prop_dist = 4;*/
			if (Math.floor(this.x / 32) == this.current_search_coords[0] && Math.floor(this.y / 32) == this.current_search_coords[1]){
				this.steps = [coordsToNum(this.current_search_coords)];
				this.step_count = 1;
			}
		}
		
		_prev_cell = coordsToNum([Math.floor(this.x / 32), Math.floor(this.y / 32)]);
		
		if (typeof(this.steps) == "undefined" || this.steps.length < 1 || typeof(this.steps[this.current_step]) == "undefined" || this.current_step > this.steps.length - 1){
			if (this.steps.length){
				this.current_step = this.steps.length - 1;
			}
			else {
				this.force_switch_step = true;
			}
			
			if (this.current_step >= this.steps.length)
				this.current_step = this.steps.length - 1;
		}
		return this.steps[this.current_step];
	};
	
	
	this.enemyAt = function(cell, other_enemies){
		var fn = false;
		var _this = this;
		other_enemies.forEach(function(e){
			if (DEBUG){
				var c = e.getCoords();
				gamejs.draw.rect(display, '#ff0000', new gamejs.Rect([viewport_offset[0] + c[0]*32, viewport_offset[1] + c[1]*32], [32, 32]), 2);
				c = numToCoords(cell);
				gamejs.draw.rect(display, '#eeaa00', new gamejs.Rect([viewport_offset[0] + c[0]*32, viewport_offset[1] + c[1]*32], [32, 32]), 2);
			}
			if (coordsToNum(e.getCoords()) == cell){
				if (e.id != _this.id)
					fn = true;
			}
		});
		return fn;
	};
	
	/**
	 * Sprawdza czy jednostka jest w okreslonym polu (w centrum pola).
	 */
	this.isOnField = function(target_cell, map){
		var _search_cell = numToCoords(target_cell);
		_search_cell = this.returnExactTargetCoords([_search_cell[0] * 32, _search_cell[1] * 32]);
		var x = parseInt(this.x),
			y = parseInt(this.y);
		if (x >= _search_cell[0] - _edef.tolerance && x <= _search_cell[0] + _edef.tolerance &&
			y >= _search_cell[1] - _edef.tolerance && y <= _search_cell[1] + _edef.tolerance)
			return _search_cell;
		return false;
	};
	
	this.returnExactTargetCoords = function(coords){
		var x = (32 - this.size[0]) / 2;
			y = (32 - this.size[1]) / 2;
		return [coords[0] + x + this.calibrate[0], coords[1] + y + this.calibrate[1]];
	};
	
	/**
	 * Wyznacza kat obrotu jednostki przed przejsciem na kolejne pole.
	 */
	this.calculateAngle = function(target_cell, map){
		var _search_cell = map.numToCoords(target_cell);
		_search_cell = this.returnExactTargetCoords([_search_cell[0] * 32, _search_cell[1] * 32]);
		var angle = (Math.atan2(_search_cell[1] - parseInt(this.y), _search_cell[0] - parseInt(this.x)) * (180 / Math.PI)) / 10;
		return ((angle.toFixed(1) * 10) + 360) % 360 + 90;
	};
	
	/**
	 * Glowna petla enemy.
	 */
	this.update = function(msDuration, map, items, player, other_enemies, itemsArrayTemp){
		this.changeActualPos = false;
		this.secure_step_count++;
		
		if (this._missed > defaultEnemy.max_missed){
			this.shooting = false;
			this._missed = 0;
			this.changeActualPos = true;
			this.secure_step_count = 120;
			this.pause_move = false;
		}
		
		if (this.life_time < 5000)
			this.life_time++;
		
		if (this.change_mode){
			this.force_switch_step = true;
			this.changeMode(this.force_mode);
			this.change_mode = false;
		}
		
		
		if (this.target_cell === null){
			this.force_switch_step = true;
			this.secure_step_count = 120;
		}
		
		/*if (this.shooting && player.isHidden()){
			this.shooting = false;
			this.force_switch_step = true;
			this.secure_step_count = 120;
		}*/
		
		if (!this.shooting && (this.isOnField(this.target_cell, map) !== false || this.secure_step_count > 110)){
			this.secure_step_count = 0;
			if (!this.enemyAt(this.target_cell, other_enemies) && this.pause_move){
				this.pause_move = false;
			}
			else {
				if (this.enemyAt(this.target_cell, other_enemies) && this.pause_move){
					this.force_switch_step = true;
				}
				
				var c = this.isOnField(this.target_cell, map);
				if (c !== false){
					this.x = c[0];
					this.y = c[1];
				}
				
				this.target_cell = this.nextStep(map, items, player, other_enemies);
				
				/** fix angle */
				if (this.angle < 0){
					this.angle += 360;
				}
				if (this.angle >= 360)
					this.angle %= 360;
				
				
				//this.prev = this.angle;
				
				var _tmp_angle = 0;
				if (this.shooting && this.current_search != 'pathnode'){
					//console.log('from player');
					_tmp_angle = this.calculateAngle(map.coordsToNum(player.getCoords()), map);
				}
				else {
					//console.log('from cell');
					_tmp_angle = this.calculateAngle(this.target_cell, map);
				}
				
				if (_tmp_angle < 0){
					_tmp_angle += 360;
				}
				if (_tmp_angle >= 360)
					_tmp_angle %= 360;
				
				//console.log('Call ->', this.angle, this.prev, _tmp_angle, 'Turn:', this.turn, 'Shooting:', this.shooting, 'Shot:', this._shot, 'Current search:', this.current_search);
				
				if (this.angle != _tmp_angle/* && !this.turn*/){
					this.prev = this.angle;
					this.angle = _tmp_angle;
					this.turn = true;
					//this.shooting = false;
					if (this.angle - this.prev > 180){
						this.angle -= 360;
					}
					else if (this.angle + this.prev > 360 && this.prev > 180 && this.angle < 180){
						this.angle += 360;
					}
					//this._shot = false;
				}
				else if (this.angle == _tmp_angle && this.shooting && !this.turn){
					this._shot = true;
					this.shooting = true;
					this.turn = false;
				}
				/*else {
					this._shot = false;
					this.shooting = false;
				}*/
				//console.log('Check ->', this.angle, this.prev, _tmp_angle, 'Turn:', this.turn, 'Shooting:', this.shooting, 'Shot:', this._shot, 'Current search:', this.current_search);
			}
			
		}
		
		if (this.enemyAt(this.target_cell, other_enemies) && !this.pause_move){
			this.pause_move = true;
			this.secure_step_count = _edef.pause_secure_step_count;
		}
		
		if (this.shooting){
			this.secure_step_count = 0;
			
			var dist = map.getDistance(player.getCoords(), this.getCoords());
			if (dist > this.current_search_distance + this.current_search_delta && !player.isHidden()){
				this.force_switch_step = true;
				this.shooting = false;
				this.secure_step_count = 300;
			}
			
			if (!this.turn || (!this.turn && !this.force_switch_step)){
				var p = this.angle,
					ang = this.calculateAngle(map.coordsToNum(player.getCoords()), map);
				if (ang < 0){
					ang += 360;
				}
				if (ang >= 360)
					ang %= 360;
				
				if (p != ang){
					this.prev = p;
					this.angle = ang;
					this.turn = true;
					if (this.angle - this.prev > 180){
						this.angle -= 360;
					}
					else if (this.angle + this.prev > 360 && this.prev > 180 && this.angle < 180){
						this.angle += 360;
					}
				}
			}
			
		}
		
		if (this.turn){
			this.secure_step_count = 0;
			var inc = this.settings.turn_speed * msDuration;
			if (this.prev < this.angle){
				this.prev += inc;
				if (this.prev >= this.angle){
					if (this.angle < 0){
						this.angle += 360;
					}
					if (this.angle >= 360)
						this.angle %= 360;
					this.prev = this.angle;
					this.turn = false;
				}
			}
			else if (this.prev > this.angle){
				this.prev -= inc;
				if (this.prev <= this.angle){
					if (this.angle < 0){
						this.angle += 360;
					}
					if (this.angle >= 360)
						this.angle %= 360;
					this.prev = this.angle;
					this.turn = false;
				}
			}
			else {
				this.turn = false;
			}
			
			if (this.turn == false && this.shooting)
				this._shot = true;
			else if (this.turn == false && !this.shooting)
				this._shot = false;
			
			this.image = gamejs.transform.rotate(this.originalImage, this.prev);
			this.shad = gamejs.transform.rotate(this.originalShad, this.prev);
		}
		else if (!this.shooting && !this.force_switch_step && !this.pause_move){
			var base_vect = [1 * Math.sin(this.angle * RAD_CONST), -1 * Math.cos(this.angle * RAD_CONST)];
			this.vector = [this.settings.speed*base_vect[0], this.settings.speed*base_vect[1]];
			
			var col_dir = map.mapAdvancedCollider(
					[this.x + this.size[0] / 2, this.y + this.size[1] / 2],
					[(this.size[0] / 2) - 1, (this.size[1] / 2) - 1],
					[this.vector[0] * (msDuration/global_refresh_rate), this.vector[1] * (msDuration/global_refresh_rate)]
			);
 			
			if (!col_dir){
				this.x += this.vector[0] * (msDuration/global_refresh_rate);
				this.y += this.vector[1] * (msDuration/global_refresh_rate);
			}
			else{
				if (col_dir == 'horizontal')
					this.x += this.vector[0] * (msDuration/global_refresh_rate);
				else if (col_dir == 'vertical')
					this.y += this.vector[1] * (msDuration/global_refresh_rate);
			}
			this._missed = 0;
		}
		
		/*if (items.length){
			for (i in items){
				if (items[i].picked([this.x+this.size[0]/2, this.y+this.size[1]/2], this.size)){
					this.pickupItem(i, items, itemsArrayTemp);
				}
			}
		}*/
		
		if (DEBUG){
			for(i in this.steps){
				var cl = map.numToPos(this.steps[i]);
                gamejs.draw.rect(display, i>=this.current_step?'#004cff':'#001c51', new gamejs.Rect([viewport_offset[0] + cl[0], viewport_offset[1] + cl[1]], [32,32]), 2);
			}
		}
		//return this.shooting ? this.shot(msDuration) : null;
		return this._shot ? this.shot(msDuration) : null;
	};
	
	this.shot = function(msDuration){
		this.weapon.next_fire -= msDuration;
		if (this.weapon.next_fire <= 0){
			this.weapon.next_fire = this.weapon.delay;
			var origin = [1 * Math.sin(deg2rad(this.prev)), -1 * Math.cos(deg2rad(this.prev))];
			
			if (sound_enabled){
				var snd = soundPlayer.Sound(this.weapon.shot_sound);
				snd.setVolume(sound_volume);
	            snd.play(false);
			}
            
			return {
				shot: this.weapon,
				angle: this.prev,
				x: (this.x + origin[0] * 12) + this.size[0] / 2 - 3,
				y: (this.y + origin[1] * 12) + this.size[1] / 2 - 5
			};
		}
		return null;
	};
	
	/*this.pickupItem = function(e, items, itemsArrayTemp){
		var it = items[e];
		if (it.data.respawn){
			itemsArrayTemp.push({coords: it.coords, name: it.data.name, time: it.data.respawn_time});
		}
		items.splice(e, 1);
	};*/
	
	this.isInRange = function(range){
		var x = this.x + this.size[0] / 2,
			y = this.y + this.size[1] / 2;
		if (x >= range.x && x <= range.x1 && y >= range.y && y <= range.y1){
			return true;
		}
		return false;
	};
	
	this.isHit = function(coords, size, vector){
		var x = coords[0] + (size[0] / 2) + vector[0],
			y = coords[1] + (size[1] / 2) + vector[1];
		if (x >= this.x && x <= this.x + this.size[0] && y >= this.y && y <= this.y + this.size[0]){
			return true;
		}
		return false;
	};
	
	this.hit = function(amount, vector, map, move){
		if (this.mode != "Aggresive"){
			this.change_mode = true;
			this.force_mode = 'Aggresive';
		}
		this.strength -= amount;
		
		var _m = move || 0;
		if (_m > 0){
			var _a = (amount * _m)/100;
			
			var v = [vector[0] * _a, vector[1] * _a];
			
			if (vector != [0, 0]){
				var col_dir = map.mapAdvancedCollider(
						[this.x + this.size[0] / 2, this.y + this.size[1] / 2],
						[(this.size[0] / 2) - 1, (this.size[1] / 2) - 1],
						v
					);
				
				if (!col_dir){
					this.x += v[0];
					this.y += v[1];
				}
				else if (col_dir == 'horizontal'){
					this.x += v[0];
				}
				else if (col_dir == 'vertical'){
					this.y += v[1];
				}
			}
		}
		
		if (this.strength <= 0){
			return false;
		};
		return true;
	};
	
	this.drawBody = function(){
		//var display = gamejs.display.getSurface();
		this.x2 = this.x + this.size[0];
		this.y2 = this.y + this.size[1];
		display.blit(this.shad, [viewport_offset[0] + this.x - 2, viewport_offset[1] + this.y + 1]);
		display.blit(this.image, [viewport_offset[0] + this.x, viewport_offset[1] + this.y]);
		if (display_enemy_health)
			this.displayHealthBar();
        if (display_foresight_circle)
            this.displayForesightCircle();
	};
	
	this.displayHealthBar = function(){
		var color = "#00ff00",
			factor = this.strength / this.settings.strength;
		var width = 28 * factor;
		if (Math.round(factor * 100) < 55) color = '#FFFF00';
		if (Math.round(factor * 100) < 40) color = '#FFBF00';
		if (Math.round(factor * 100) < 25) color = '#FF0000';
		
		gamejs.draw.rect(display, color, new gamejs.Rect([viewport_offset[0] + this.x - 2, viewport_offset[1] + this.y + this.size[1]], [width, 4]));
		gamejs.draw.rect(display, '#000000', new gamejs.Rect([viewport_offset[0] + this.x - 2, viewport_offset[1] + this.y + this.size[1]], [28, 4]), 1);
	};

    this.displayForesightCircle = function(){
        gamejs.draw.circle(display, 'rgba(255, 255, 255, 0.07)', [viewport_offset[0] + this.x + this.size[0]/2, viewport_offset[1] + this.y + this.size[1]/2], this.patrol_player_sight_distance * 32);
    };
};


gamejs.utils.objects.extend(Enemy, gamejs.sprite.Sprite);
exports.Enemy = Enemy;