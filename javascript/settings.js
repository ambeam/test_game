var organizer = {
	assets: [],
	
	_contains: function(asset){
		var i = this.assets.length;
		while (i--){
			if (this.assets[i] === asset){
				return true;
			}
		}
		return false;
	},
	
	add: function(asset){
		if (Object.prototype.toString.call(asset) === '[object Array]'){
			for(i in asset){
				if (!this._contains(asset[i]))
					this.assets.push(asset[i]);	
			}
		}
		else if (!this._contains(asset))
			this.assets.push(asset);
		
		return asset;
	},
	
	dump: function(){
		return this.assets;
	}
};

var _pd = atob(_epd);

/*** settings **/

var DEBUG 			            = false;

var display_enemy_health        = true;
var display_foresight_circle    = true;
var display_minimap             = true;
var display_helper_arrow        = true;
var display_helper_arrow_fwd    = false;
var display_fps					= true;
var display_points				= false;

var width_px		= 1024;
var height_px		= 640;
var viewport_width  = 1024;
var viewport_height = 500;
var viewport_offset = [0, 0];
var viewport_margin = [380, 240];
var minimap_size    = [100, 100];
var minimap_pos     = [width_px - 180, height_px - 110];
var global_refresh_rate = 800;
var fps_refresh_rate	= 15;

var game_over = false;
var game_pause = false;
var call_restart = false;
var call_continue = false;
var game_success = false;

var sound_volume = 0.2;
var sound_enabled = true;

var max_marked_cycles = 1024; // ilosc oznaczen pol w poszukiwaniu drogi, im wieksza tym dluzsza droge moze enemy znalezc, ale zwieksza sie czas wykonania skryptu

/*var images_to_load = [
	"images/tank1a.png",
    "images/tank1a_shad.png",
    "images/tank1_tower.png",
    "images/tank1_tower_shad.png",
    "images/fire1.png",
	"images/fire2.png",
	
    "images/enemy1.png",
    "images/enemy1_shad.png",
    "images/enemy2.png",
    "images/enemy2_shad.png",
    "images/enemy3.png",
    "images/enemy3_shad.png",
    "images/enemy4.png",
    "images/enemy4_shad.png",
    "images/enemy5.png",
    "images/enemy5_shad.png",
    "images/enemy6.png",
    "images/enemy6_shad.png",
    
    "images/anims/exp01.png",
    "images/anims/exp03.png",
    "images/anims/exp02.png",
    "images/anims/exp04.png",
    "images/anims/exp07.png",
    "images/anims/exp08.png",
    "images/item01.png",
    "images/item02.png",
    "images/item03.png",
    "images/plasma1.png",
    "images/rocket1.png",
    "images/anims/exp04_smudge.png",
    "images/anims/exp05_sm.png",
    "images/anims/exp05_sm_smudge.png",
    "images/shield1.png",
    "images/key01.png",
    "images/ammo01.png",
    "images/turret01.png",
    "images/building01.png",
    "images/fire3.png",
    "images/helper_arrow.png",
    "images/flare01.png",
    "images/sniper_bullet.png",
    "images/flak_shot.png",

    "sounds/pistol_hit.wav",
    "sounds/pistol_shot.wav",
    "sounds/gokgrav1.wav",
    "sounds/goklaser1hit.wav",
    "sounds/orcboom1.wav",
	"sounds/at_launch.wav",
	"sounds/flakshot.wav",
    "sounds/flakhit.wav",
    "sounds/railgun.wav",
    "sounds/machinegun.wav",
    "sounds/shield_hit.wav",
    "sounds/item_pickup.wav",
    "sounds/tankhit01.wav",
    "sounds/tankhit02.wav",
    "sounds/explode1.wav",
    "sounds/shotgun_heavy_shot.wav",
    
    "sounds/sniper_hit.wav",
    "sounds/sniper_shot.wav",
    
    "images/particles/particle01.png",
    "images/particles/particle02.png",
    "images/particles/particle04.png",
    
    
    "images/structures/oil1.png",
    "images/structures/oil2_sm.png",
    "images/structures/oil3_sm.png",
    "images/structures/oil4_sm.png",
    "images/structures/oil5_sm.png"
];*/

var SHOOT_SPRITE 	= 1,
	SHOOT_BEAM 		= 2,
	SHOOT_PARTICLE 	= 3;

var shopItems = [{
        name: "Armor"
        
	}, {
		
	}

];

var test_map_schema = {
	length: 2
};

var bindings = {
    accelerate		: 87, //38,
    brake			: 83, //40,
    steer_left		: 65, //37,
    steer_right		: 68, //39,
    fire1			: 32,
    pause			: 27,
    slowdown		: 16,
    speedup			: 32,
    prev_weapon		: 90,
    next_weapon		: 88,
    mouse_fire1		: 0
};

defaultEnemy = {
    sight_distance  : 4.5,              // defaultowy zasieg "wzroku" przeciwnika
    default_mode    : 'Passive',        // defaultowy tryb walki przeciwnika
    search_delta    : 2,                // maksymalny dystans pomiedzy current_search_coords a pozycja E, po przekroczeniu ktorego nastepuje ponowne szukanie punktu
    max_missed		: 4					// liczba nietrafionych strzalow (np w mur), po ktorych enemy zmienia pozycje
};


organizer.add("images/particles/particle05.png");
organizer.add("maps/globalMap/map6.jpg");

var shotTypes = [{}, {
		name: "Main gun", //1
		image: organizer.add("images/fire1.png"),
		icon: "",
		explosion_type: 0,
		type: SHOOT_SPRITE,
		
		hit_sound: organizer.add('sounds/pistol_hit.wav'),
		shot_sound: organizer.add('sounds/pistol_shot.wav'),
		
		speed: 310,
		power: 120,
		range: 0,
		delay: 300,
		ammo_cost: 1,
		spread: 0,
		spread_angle: 0,
		acceleration: false,
		acceleration_factor: 0,
		push: 0,		// procent mocy
		
		distance: 210,
		distance_explode: true,
		distance_explode_image: null,
		distance_explode_sound: null,
        distance_explode_range: false,
		
		shop_cost: 5,
		shop_amount: 100,
		weapon_cost: 0,
		
		trackable: false,
		inherit_angle: true,
		has_flare: true,
		
		next_fire: 0
	}, {
		name: "Shotgun", //2
		image: organizer.add("images/fire1.png"),
		icon: "",
		explosion_type: 0,
		type: SHOOT_SPRITE,
		
		hit_sound: organizer.add('sounds/flakhit.wav'),
		shot_sound: organizer.add('sounds/shotgun_heavy_shot.wav'),
		
		speed: 280,
		power: 70,
		range: 0,
		delay: 800,
		ammo_cost: 1,
		spread: 6,
		spread_angle: 10.5,
		acceleration: false,
		acceleration_factor: 0,
		push: 0,
		
		distance: 270,
		distance_explode: true,
		distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: false,
		
		shop_cost: 100,
		shop_amount: 400,
		weapon_cost: 1000,
		
		trackable: false,
		inherit_angle: true,
		has_flare: true,
		
		next_fire: 0
	}, {
		name: "Plasma cannon", //3
		image: organizer.add("images/plasma1.png"),
		icon: "",
		explosion_type: 3,
		type: SHOOT_SPRITE,
		
		hit_sound: organizer.add('sounds/gokgrav1.wav'),
		shot_sound: organizer.add('sounds/goklaser1hit.wav'),
		
		speed: 320,
		power: 290,
		range: 110,
		delay: 690,
		ammo_cost: 1,
		spread: 0,
		spread_angle: 0,
		acceleration: false,
		acceleration_factor: 0,
		push: .005,
		
		distance: 214,
		distance_explode: true,
		distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: true,
		
		shop_cost: 2000,
		shop_amount: 100,
		weapon_cost: 5200,
		
		trackable: false,
		inherit_angle: false,
		has_flare: false,
		
		next_fire: 0
	}, {
		name: "Shotgun v2", //4
		image: organizer.add("images/fire1.png"),
		icon: "",
		explosion_type: 0,
		type: SHOOT_SPRITE,
		
		hit_sound: organizer.add('sounds/flakhit.wav'),
		shot_sound: organizer.add('sounds/shotgun_heavy_shot.wav'),
		
		speed: 300,
		power: 90,
		range: 0,
		delay: 1000,
		ammo_cost: 1,
		spread: 11,
		spread_angle: 11.0,
		acceleration: false,
		acceleration_factor: 0,
		push: 0,
		
		distance: 310,
		distance_explode: true,
		distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: false,
		
		shop_cost: 1500,
		shop_amount: 300,
		weapon_cost: 4500,
		
		trackable: false,
		inherit_angle: true,
		has_flare: true,
		
		next_fire: 0
	}, {
		name: "Winchester", //5
		image: organizer.add("images/fire2.png"),
		icon: "",
		explosion_type: 0,
		type: SHOOT_SPRITE,

		hit_sound: organizer.add('sounds/pistol_hit.wav'),
		shot_sound: organizer.add('sounds/pistol_shot.wav'),
		
		speed: 500,
		power: 140,
		range: 0,
		delay: 600,
		ammo_cost: 1,
		spread: 2,
		spread_angle: 1.0,
		acceleration: false,
		acceleration_factor: 0,
		push: 0,
		
		distance: 500,
		distance_explode: true,
		distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: false,
		
		shop_cost: 1000,
		shop_amount: 100,
		weapon_cost: 7000,
		
		trackable: false,
		inherit_angle: false,
		has_flare: true,
		
		next_fire: 0
	}, {
		name: "Rocket Launcher", //6
		image: organizer.add("images/rocket1.png"),
		icon: "",
		explosion_type: 2,
		type: SHOOT_SPRITE,
		
		hit_sound: organizer.add('sounds/orcboom1.wav'),
		shot_sound: organizer.add('sounds/at_launch.wav'),
		
		speed: 70,
		power: 450,
		range: 160,
		delay: 2000,
		ammo_cost: 1,
		spread: 0,
		spread_angle: 0,
		acceleration: true,
		acceleration_factor: 6,
		push: 0.049,
		
		distance: 600,
		distance_explode: true,
		distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: true,
		
        smoketrail: 1,
        
		shop_cost: 5000,
		shop_amount: 20,
		weapon_cost: 12000,
		
		trackable: false,
		inherit_angle: true,
		has_flare: true,
		
		next_fire: 0
	}, {
        name: "Machinegun", //7
        image: organizer.add("images/fire3.png"),
        icon: "",
        explosion_type: 4,
        type: SHOOT_SPRITE,

        hit_sound: organizer.add('sounds/flakhit.wav'),
        shot_sound: organizer.add('sounds/machinegun.wav'),

        speed: 380,
        power: 60,
        range: 0,
        delay: 75,
        ammo_cost: 2,
        spread: 1,
        spread_angle: 0.8,
        acceleration: false,
        acceleration_factor: 0,
        push: 0.089,

        distance: 500,
        distance_explode: true,
        distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: false,

        shop_cost: 5000,
        shop_amount: 1000,
        weapon_cost: 13800,

        trackable: false,
        inherit_angle: true,
        has_flare: true,

        next_fire: 0
    }, {
		name: "Laser", //8
		image: "",
		icon: "",
		explosion_type: 0,
		type: SHOOT_BEAM,
		color: '230, 69, 69',
		width: 2.5,
		stay_time: 67,
		through: true,
		
		hit_sound: organizer.add('sounds/pistol_hit.wav'),
		shot_sound: organizer.add('sounds/railgun.wav'),
		
		speed: 0,
		power: 1200,
		range: 10,
		delay: 3000,
		ammo_cost: 1,
		spread: 0,
		spread_angle: 0,
		acceleration: false,
		acceleration_factor: 0,
		push: 0,
		
		distance: 0,
		distance_explode: false,
		distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: false,
		
		shop_cost: 7000,
		shop_amount: 20,
		weapon_cost: 14610,
		
		trackable: false,
		inherit_angle: true,
		has_flare: false,
		
		next_fire: 0
	},
    {
        name: "Heavy Rocket Launcher", //9
        image: organizer.add("images/rocket1.png"),
        icon: "",
        explosion_type: 2,
        type: SHOOT_SPRITE,

        hit_sound: organizer.add('sounds/orcboom1.wav'),
        shot_sound: organizer.add('sounds/at_launch.wav'),

        speed: 100,
        power: 700,
        range: 180,
        delay: 2200,
        ammo_cost: 1,
        spread: 3,
        spread_angle: 2.5,
        acceleration: true,
        acceleration_factor: 7,
        push: 0.046,

        distance: 700,
        distance_explode: true,
        distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: true,

        shop_cost: 6000,
        shop_amount: 120,
        weapon_cost: 22400,
        
        smoketrail: 1,

        trackable: false,
        inherit_angle: true,
        has_flare: true,

        next_fire: 0
    },

    {
		name: "Heavy Laser", //10
		image: "",
		icon: "",
		explosion_type: 0,
		type: SHOOT_BEAM,
		color: '36, 129, 255',
		width: 3,
		stay_time: 105,
		through: true,
		
		hit_sound: organizer.add('sounds/pistol_hit.wav'),
		shot_sound: organizer.add('sounds/railgun.wav'),
		
		speed: 0,
		power: 2600,
		range: 10,
		delay: 3000,
		ammo_cost: 1,
		spread: 0,
		spread_angle: 0,
		acceleration: false,
		acceleration_factor: 0,
		push: 0,
		
		distance: 0,
		distance_explode: false,
		distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: false,
		
		shop_cost: 7000,
		shop_amount: 20,
		weapon_cost: 25450,
		
		trackable: false,
		inherit_angle: true,
		has_flare: false,
		
		next_fire: 0
	}, {
		name: "Sniper riffle", // 11
		image: organizer.add("images/sniper_bullet.png"),
		icon: "",
		explosion_type: 0,
		type: SHOOT_SPRITE,

		hit_sound: organizer.add('sounds/sniper_hit.wav'),
		shot_sound: organizer.add('sounds/sniper_shot.wav'),
		
		speed: 780,
		power: 1000,
		range: 0,
		delay: 650,
		ammo_cost: 1,
		spread: 0,
		spread_angle: 0,
		acceleration: false,
		acceleration_factor: 0,
		push: 0,
		
		distance: 1000,
		distance_explode: true,
		distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: false,
		
		shop_cost: 8000,
		shop_amount: 12,
		weapon_cost: 6500,
		
		trackable: false,
		inherit_angle: true,
		has_flare: true,
		
		next_fire: 0
	}, {
		name: "Flak gun", // 12
		image: organizer.add("images/flak_shot.png"),
		icon: "",
		explosion_type: 0,
		type: SHOOT_SPRITE,
		
		hit_sound: organizer.add('sounds/flakhit.wav'),
		shot_sound: organizer.add('sounds/flakshot.wav'),
		
		speed: 460,
		power: 240,
		range: 14,
		delay: 510,
		ammo_cost: 1,
		spread: 6,
		spread_angle: 7.0,
		acceleration: false,
		acceleration_factor: 0,
		push: 0.009,
		
		distance: 400,
		distance_explode: true,
		distance_explode_image: null,
        distance_explode_sound: null,
        distance_explode_range: false,
		
		shop_cost: 9500,
		shop_amount: 60,
		weapon_cost: 7400,
		
		trackable: false,
		inherit_angle: true,
		has_flare: true,
		
		next_fire: 0
	}
];


var turretTypes = [{
    image: organizer.add("images/turret01.png"),
    name: "Defender",
    turn_speed: .2,
    shooter: true,
    strength: 2000,
    explosion_type: 5,
    sight_distance: 230,
    size: [26, 26],
    barrel_origin: [13, 13]
}, {
    image: organizer.add("images/turret01.png"),
    name: "Defender v2",
    turn_speed: .2,
    shooter: true,
    strength: 5000,
    explosion_type: 5,
    sight_distance: 260,
    size: [26, 26],
    barrel_origin: [13, 13]
}
];

var buildingTypes = [{
    image: organizer.add("images/structures/wall01.png"),		// 0
    name: "Vertical wall",
    strength: 10000,
    size: [1, 1],
    is_wall: true
}, {
    image: organizer.add("images/structures/wall02.png"),		// 1
    name: "Horizontal wall",
    strength: 10000,
    size: [1, 1],
    is_wall: true
}, {
    image: organizer.add("images/building01.png"),	// 2
    name: "Research Center",
    strength: 55000,
    size: [3, 2],
    is_wall: false
}, {
	image: organizer.add("images/structures/oil1.png"),	// 3
	name: "Oil container",
	strength: 400,
	explosion_type: 5,
	size: [1, 1],
	is_wall: true
}, {
	image: organizer.add("images/structures/oil2_sm.png"),	// 4
	name: "Oil container small",
	strength: 80,
	explosion_type: 2,
	size: [1, 1],
	is_wall: true
}, {
	image: organizer.add("images/structures/oil3_sm.png"),	// 5
	name: "Oil container small",
	strength: 80,
	explosion_type: 2,
	size: [1, 1],
	is_wall: true
}, {
	image: organizer.add("images/structures/oil4_sm.png"),	// 6
	name: "Oil container small",
	strength: 80,
	explosion_type: 2,
	size: [1, 1],
	is_wall: true
}, {
	image: organizer.add("images/structures/oil5_sm.png"),	// 7
	name: "Oil container small",
	strength: 80,
	explosion_type: 2,
	size: [1, 1],
	is_wall: true
}];

var enemyTypes = [{
		image: organizer.add("images/enemy1.png"), // 0
		shadow: organizer.add("images/enemy1_shad.png"),
		name: "Lamer",
		type: 1,
		turn_speed: .2,
        explosion_type: 5,
		shooter: true,
		speed: 30,
		strength: 2000,
		sight_distance: 4.5,
		size: [26, 26]
	},
    {
        image: organizer.add("images/enemy2.png"), // 1
        shadow: organizer.add("images/enemy2_shad.png"),
        name: "Looser",
        type: 1,
        turn_speed: .1,
        explosion_type: 5,
        shooter: true,
        speed: 40,
        strength: 4000,
        sight_distance: 5,
        size: [26, 26]
    },
    {
        image: organizer.add("images/enemy3.png"), // 2
        shadow: organizer.add("images/enemy3_shad.png"),
        name: "Attacker",
        type: 1,
        turn_speed: .15,
        explosion_type: 5,
        shooter: true,
        speed: 48,
        strength: 11000,
        sight_distance: 4.5,
        size: [26, 26]
    },
    {
        image: organizer.add("images/enemy4.png"), // 3
        shadow: organizer.add("images/enemy4_shad.png"),
        name: "Bulldog",
        type: 1,
        turn_speed: .08,
        explosion_type: 5,
        shooter: true,
        speed: 35,
        strength: 34000,
        sight_distance: 12,
        size: [30, 30]
    },
    {
        image: organizer.add("images/enemy5.png"), // 4
        shadow: organizer.add("images/enemy5_shad.png"),
        name: "Veasel",
        type: 1,
        turn_speed: .39,
        explosion_type: 5,
        shooter: true,
        speed: 60,
        strength: 9000,
        sight_distance: 7.5,
        size: [26, 26]
    },
    {
        image: organizer.add("images/enemy6.png"), // 5
        shadow: organizer.add("images/enemy6_shad.png"),
        name: "Railtank",
        type: 1,
        turn_speed: .06,
        explosion_type: 5,
        shooter: true,
        speed: 28,
        strength: 53320,
        sight_distance: 14,
        size: [30, 30]
    }
];

var enemyModes = [{
	name: 'Passive',
	shooting_mode: 1,	// 1 - tylko jezeli "zainteresowany", 2 - strzela jak tylko widzi postac, 3 - wstrzymuje sie od strzalu czekajac na przeciwnika
	items: 1,			// zbiera itemy (on/off)
	berserk: 0,			// jest wkurwiajacy (on/off)
	coward: 0,			// ucieka (on/off)
	aggresive: 0,		// zwyczajnie atakuje (on/off)
	ignore: 0,			// ma wszystko gdzies (on/off)
	patrol: 0			// czy jednostka patrolowa
}, {
	name: 'Tactical',
	shooting_mode: 1,
	items: 1,
	berserk: 0,
	coward: 0,
	aggresive: 1,
	ignore: 0,
	patrol: 0
}, {
	name: 'Aggresive',
	shooting_mode: 2,
	items: 0,
	berserk: 1,
	coward: 0,
	aggresive: 1,
	ignore: 0,
	patrol: 0
}, {
	name: 'Noob',
	shooting_mode: 3,
	items: 1,
	berserk: 0,
	coward: 1,
	aggresive: 0,
	ignore: 0,
	patrol: 0
}, {
	name: 'Patrol',
	shooting_mode: 2,
	items: 0,
	berserk: 0,
	coward: 0,
	aggresive: 0,
	ignore: 0,
	patrol: 1
}];



var explosionTypes = [{		// 0
	width: 16,
	height: 16,
	image: organizer.add('images/anims/exp01.png'),
    explode_sound: '',
	smudge: '',
	anim: [0, 24, false],
	pause: [24],
	fps: 35,
	particle: {
		maxParticles: 10,
		emissionRate: 10,
		image: organizer.add('images/particles/particle01.png'),
		lifeTime: 20,
		fadeTime: 3,
		velocity: 0.3
	}
}, {						// 1
	width: 64,
	height: 64,
	image: organizer.add('images/anims/exp02.png'),
    explode_sound: '',
	smudge: '',
	anim: [0, 37, false],
	pause: [37],
	fps: 40
}, {						// 2
	width: 64,
	height: 64,
	image: organizer.add('images/anims/exp04.png'),
	smudge: organizer.add('images/anims/exp04_smudge.png'),
    explode_sound: '',
	anim: [0, 13, false],
	pause: [13],
	fps: 26,
	particle: {
		maxParticles: 4,
		emissionRate: 4,
		image: organizer.add('images/particles/particle04.png'),
		lifeTime: 36,
		fadeTime: 10,
		velocity: 0.29
	}
}, {						// 3
	width: 64,
	height: 64,
	image: organizer.add('images/anims/exp07.png'),
    explode_sound: '',
	smudge: '',
	anim: [0, 14, false],
	pause: [14],
	fps: 25,
	particle: {
		maxParticles: 1,
		emissionRate: 1,
		image: organizer.add('images/particles/particle02.png'),
		lifeTime: 10,
		fadeTime: 2,
		velocity: 0
	}
}, {						// 4
	width: 16,
	height: 16,
	image: organizer.add('images/anims/exp08.png'),
    explode_sound: '',
	smudge: '',
	anim: [0, 4, false],
	pause: [4],
	fps: 25,
	particle: {
		maxParticles: 2,
		emissionRate: 2,
		image: organizer.add('images/particles/particle01.png'),
		lifeTime: 5,
		fadeTime: 2,
		velocity: 0.2
	}
}, {						// 5
    width: 80,
    height: 80,
    image: organizer.add('images/anims/exp05_sm.png'),
    smudge: organizer.add('images/anims/exp05_sm_smudge.png'),
    explode_sound: organizer.add('sounds/explode1.wav'),
    anim: [0, 19, false],
    pause: [19],
    fps: 20,
	particle: {
		maxParticles: 7,
		emissionRate: 7,
		image: organizer.add('images/particles/particle02.png'),
		lifeTime: 36,
		fadeTime: 10,
		velocity: 0.96
	}
}, {
	width: 128,
	height: 128,
	image: organizer.add('images/anims/explosion_exported02.png'),
	smudge: organizer.add('images/anims/exp04_smudge.png'),
    explode_sound: organizer.add('sounds/OrcFRL.wav'),
    anim: [0, 46, false],
    pause: [46],
    fps: 26,
	particle: {
		maxParticles: 7,
		emissionRate: 7,
		image: organizer.add('images/particles/particle02.png'),
		lifeTime: 36,
		fadeTime: 10,
		velocity: 0.96
	}
}
];


var generalShopItems = [{
	name: 'Armor',
	type: 'armor',
	amount: 200,
	cost: 1000
},{
	name: 'Afterburner',
	type: 'afterburner',
	amount: 100,
	cost: 1200
}];

var itemTypes = [{
	image: organizer.add("images/item01.png"),
    pickup_sound: organizer.add('sounds/item_pickup.wav'),
	size: [24, 24],
	name: "Cash",
	money: 500,
	armor: 0,
	speed: 0,
	ammo: 0,
	health: 0,
    afterburner: 0,
	respawn: false,
	respawn_time: 0,
	pickup: 1,
	color: '#dfe156',
	single: 1
}, {
	image: organizer.add("images/item02.png"),
    pickup_sound: organizer.add('sounds/item_pickup.wav'),
	size: [20, 20],
	name: "Armor",
	money: 0,
	armor: 100,
	speed: 0,
	ammo: 50,
	health: 0,
	ammo: 0,
    afterburner: 20,
	respawn: true,
	respawn_time: 50,
	pickup: 1,
	color: '#86321c',
	single: 0
}, {
	image: organizer.add("images/item03.png"),
    pickup_sound: organizer.add('sounds/item_pickup.wav'),
	size: [24, 24],
	name: "Health",
	money: 0,
	armor: 0,
	speed: 0,
	ammo: 0,
	health: 500,
    afterburner: 0,
	respawn: true,
	respawn_time: 30,
	pickup: 1,
	color: '#74e156',
	single: 1,
	health_only: 1
}, {
	image: organizer.add("images/item03_sm.png"),
    pickup_sound: organizer.add('sounds/item_pickup.wav'),
	size: [28, 28],
	name: "Medkit",
	money: 0,
	armor: 0,
	speed: 0,
	ammo: 0,
	health: 100,
    afterburner: 0,
	respawn: true,
	respawn_time: 27,
	pickup: 1,
	color: '#74e156',
	single: 1,
	health_only: 1
}, {
	image: organizer.add("images/item05.png"),
    pickup_sound: organizer.add('sounds/item_pickup.wav'),
	size: [24, 24],
	name: "Extra pack",
	money: 100,
	armor: 50,
	speed: 0,
	health: 50,
	ammo: 50000,
    afterburner: 20,
	respawn: true,
	respawn_time: 100,
	pickup: 1,
	color: '#87d9e8',
	single: 0
}, {
	image: organizer.add("images/ammo01.png"),
    pickup_sound: organizer.add('sounds/item_pickup.wav'),
	size: [26, 26],
	name: "Ammo pack",
	money: 0,
	armor: 0,
	speed: 0,
	ammo: 120000,
	health: 0,
    afterburner: 0,
	respawn: true,
	respawn_time: 100,
	pickup: 1,
	color: '#87d9e8',
	single: 1
},{
	image: organizer.add("images/key01.png"),
    pickup_sound: organizer.add('sounds/item_pickup.wav'),
	size: [24, 24],
	name: "Key 1",
	money: 0,
	armor: 0,
	speed: 0,
	ammo: 0,
	health: 0,
	key: 'primary',
    afterburner: 0,
	respawn: false,
	respawn_time: 0,
	pickup: 1,
	color: '#dfe156',
	single: 1
}
];

var map_tiles_folder = 'maps/tiles/';
var map_tree_folder = 'images/tree/';
var global_current_map = 0;
var map_finish_prize = 0;



var playerDefaults = {
	name					: 'Player',		// nazwa gracza
	model					: 'Default',	// model czolgu
	speed					: 60,			// standardowa predkosc
	max_speed				: 100,			// przyspieszona (gdy dostepna jest akceleracja)
	angle_divider			: 3,			// dzielnik zmniejszajacy predkosc obracania sie czolgu (przy wcisnietym klawiszu spowolnienia)
	max_steer_angle 		: 20,			// mnoznik predkosci skretu - nie zmieniac!
	rotate_speed			: 5,			// mnoznik skretu - tez nie zmieniac!
	acceleration_factor		: 1.6,			// przyspieszenie
	max_health				: 1000,         // maksymalna ilosc hp
	armor_factor			: 0.3,          // iloczyn zmniejszenia sily trafienia (w przypadku posiadania armora)
	shield_stay_time		: 80,           // czas (w taktach) przez jaki wyswietlana jest oslona po trafieniu gracza
	immortality				: false,		// niesmiertelnosc
	afterburner_dec			: 0.2,			// predkosc zmniejszania afterburnera podczas uzycia
	flare_time				: 3,			// czas wyswietlania flary po strzale
	hurt_opacity			: 0.4,			// opacity czerwonego koloru po oberwaniu
	hurt_fade_speed			: 0.019,		// szybkosc zanikania opacity koloru po oberwaniu
	
	crash_bounce			: true,			// czy gracz ma sie odbijac od przeciwnika
	crash_bounce_factor		: 0.6,			// sila (procent nominalnej predkosci) odbicia od przeciwnika po wjechaniu w niego
	crash_min_speed			: 30,			// minimalna predkosc uderzenia potrzebna do uszkodzenia przeciwnika
	crash_min_bounce_speed	: 5,			// minimalna predkosc uderzenia potrzebna do odbicia
	crash_force_factor		: 10,			// zycie odbierane przeciwnikowi przez uderzenie (mnoznik predkosci)
	crash_frag_money		: 0.5,			// mnoznik uderzenia, wyznacza ile kasy otrzymuje gracz za zabicie przeciwnika uderzeniem
	crash_self_damage		: 1,			// mnoznik uderzenia, wyznacza ile zycia ma byc odebranego graczowi przez uderzenie, jezeli 0 - nic nie jest odbierane
	
	init_money				: 200000,			// ilosc kasy na starcie
	init_health				: 1000,			// ilosc zycia na starcie
	init_armor				: 200,			// ilosc armora na starcie
	init_ammo				: 1500,			// ilosc amunicji na starcie <- do rozdzielenia
	init_has_viewfinder		: true,			// czy gracz ma laserowy celownik na starcie
	init_gun				: 1,			// id poczatkowej broni
	init_afterburner		: 100,			// ilosc "paliwa" na starcie, paliwo do "dopalaczy"
	
	level_difficulty			: 1,		// poziom trudnosci, od 3 zaczyna sie odbieranie kwitu za oberwanie
	level_difficulty_modifier	: 10,		// dzielnik kwitu ktory ucieka po oberwaniu (moc strzalu / dzielnik)
	
	image					: organizer.add('images/tank1a.png'),
	shadow					: organizer.add('images/tank1a_shad.png'),
	shield					: organizer.add('images/shield1.png'),
	tower					: organizer.add('images/tank1_tower.png'),
	tower_shadow			: organizer.add('images/tank1_tower_shad.png'),
    helper_arrow            : organizer.add('images/helper_arrow.png'),
    flare					: organizer.add('images/flare01.png'),
    shield_hit_sound		: organizer.add('sounds/shield_hit.wav'),
    low_hit_sound			: organizer.add('sounds/tankhit01.wav'),
    hard_hit_sound			: organizer.add('sounds/tankhit02.wav'),
	
    userStats				: {				// statystyki usera
    	shots_fired		: 0,
    	shots_aimed		: 0,
    	kills			: 0,
    	kia				: 0,
    	items			: 0,
    	damage			: 0,
    	received		: 0,
    	earned_money	: 0,
    	enemy_count		: 0
    }
};


var rand = function(min, max){
	return Math.floor((Math.random()*(max-min+1))+min);
};

function deg2rad(angle){
	return angle * .017453292519943295;
}

function inRange(x1, y1, x2, y2, _x1, _y1, _x2, _y2){
	if (parseInt(x1) >= parseInt(_x1) && parseInt(x2) <= parseInt(_x2) && parseInt(y1) >= parseInt(_y1) && parseInt(y2) <= parseInt(_y2))
		return true;
	return false;
}

unique = function(array){
    var initial = array;
    var results = [];
    var seen = [];
    for(i in initial){
    	var index = i;
    	var value = initial[i];
    	if (!index || seen[seen.length - 1] !== value){
    		seen.push(value);
    		results.push(array[index]);
    	}
    };
    return results;
};

var _mpSize = [],
	_mpSheet = {};

getCurrentMapSize = function(){
	return _mpSize;
};

checkSheetAt = function(coords){
	return _mpSheet.charAt(coordsToNum(coords));
};

coordsToNum = function(coords){
    var c = getCurrentMapSize();
	return coords[0] + coords[1] * c[0];
};

numToCoords = function(num){
    var c = getCurrentMapSize();
	var row = Math.floor(num / c[0]);
	return [num - (row * c[0]), row];
};

numToPos = function(num){
	var c = numToCoords(num);
	return [c[0] * 32, c[1] * 32];
}

isNum = function(num){
	return typeof num === "number";
};

isCoords = function(coords){
	return typeof coords === "object";
};

coordsToPixels = function(coords){
	return [coords[0] * 32 + 16, coords[1] * 32 + 16];
};

pixelsToCoords = function(x, y){
	return [Math.floor(x / 32), Math.floor(y / 32)];
}

getCleanCoords = function(cell){
	if (isNum(cell))
		return numToCoords(cell);
	else if (isCoords(cell))
		return cell;
	else return null;
};

getAbsoluteDistance = function(coords1, coords2){
	var xd = coords2[0] - coords1[0],
		yd = coords2[1] - coords1[1];
	return Math.sqrt(xd*xd + yd*yd);
};

getItem = function(name){
	for(var i in itemTypes){
		if (itemTypes[i].name == name)
			return itemTypes[i];
	}
	return false;
};

$(document).ready(function(){
	if ($('#gjs-canvas').length)
		$('#gjs-canvas').focus().disableSelection();
});

