var gamejs = require('gamejs');


/********** TRIGGER
 *
 */

var Trigger = function(id, settings){
	Trigger.superConstructor.apply(this, arguments);
    
	this.settings = settings;
	this.id = id;
	var state = 0;
	
    this.pick = function(){
    	state = 1;
    };
    
    this.ping = function(){
    	return state == 1;
    };
    
    this.setDependencies = function(){
    	if (this.ping()){
    		if (this.settings.dependency){
    			for(i in this.settings.dependency)
    				this.settings.dependency[i].pick();
    		}
    	}
    };
};


gamejs.utils.objects.extend(Trigger, gamejs.sprite.Sprite);
exports.Trigger = Trigger;