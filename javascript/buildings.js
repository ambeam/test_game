var gamejs = require('gamejs');


/********** BUILDING CLASS
 *
 */

var display = gamejs.display.getSurface();

var Building = function(data, id){
    Building.superConstructor.apply(this, arguments);
    this.id = id;
    this.x = data.x * 32 + 4;
    this.y = data.y * 32;

    this.type = data.type;
    this.settings = Object.create(buildingTypes[data.type]);
    this.strength = this.settings.strength;

    this.originalImage = gamejs.image.load(this.settings.image);
    this.originalImage = gamejs.transform.scale(this.originalImage, [this.settings.size[0] * 32, this.settings.size[1] * 32]);
    this.image = gamejs.transform.rotate(this.originalImage, this.angle);
    this.size = this.originalImage.getSize();

    this.x2 = this.x + this.size[0];
    this.y2 = this.y + this.size[1];
    this.rect = new gamejs.Rect([this.x, this.y], this.size);


    this.getCoords = function(){
        return [Math.floor((this.x + this.size[0] / 2) / 32), Math.floor((this.y + this.size[1] / 2) / 32)];
    };

    /**
     * Zwraca polozenie w pikselach
     *
     * @param coords
     * @returns {Array}
     */
    this.returnExactTargetCoords = function(coords){
        var x = (32 - this.size[0]) / 2;
        y = (32 - this.size[1]) / 2;
        return [coords[0] + x + this.calibrate[0], coords[1] + y + this.calibrate[1]];
    };


    /**
     * Glowna petla
     */
    this.update = function(msDuration, map, player){

    };

    this.isHit = function(coords, size, vector){
        var x = coords[0] + (size[0] / 2) + vector[0],
            y = coords[1] + (size[1] / 2) + vector[1];
        if (x >= this.x && x <= this.x + this.size[0] && y >= this.y && y <= this.y + this.size[0]){
            return true;
        }
        return false;
    };

    this.hit = function(amount, vector, map){
        this.strength -= amount;
        if (this.strength <= 0){
            return false;
        };
        return true;
    };

    this.drawBody = function(){
        display.blit(this.image, [viewport_offset[0] + this.x, viewport_offset[1] + this.y]);
        if (display_enemy_health)
            this.displayHealthBar();
    };

    this.displayHealthBar = function(){
    	
        var size = this.size[0] - 10;
        var color = "#00ff00",
            factor = this.strength / this.settings.strength;
        var width = size * factor;
        if (Math.round(factor * 100) < 55) color = '#FFFF00';
        if (Math.round(factor * 100) < 40) color = '#FFBF00';
        if (Math.round(factor * 100) < 25) color = '#FF0000';

        gamejs.draw.rect(display, color, new gamejs.Rect([viewport_offset[0] + this.x + 7, viewport_offset[1] + this.y + this.size[1]], [width, 4]));
        gamejs.draw.rect(display, '#000000', new gamejs.Rect([viewport_offset[0] + this.x + 7, viewport_offset[1] + this.y + this.size[1]], [size, 4]), 1);
    };
};

gamejs.utils.objects.extend(Building, gamejs.sprite.Sprite);
exports.Building = Building;