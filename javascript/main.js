;/**************************************************************/
/* 
 * Main project file
 * 
 * Created by Tobiasz Palka
 * archontp@yahoo.com
 */


var gamejs = require('gamejs');
var box2d = require('gamejs/Box2dWeb-21.a.3');
var vectors = require('gamejs/utils/vectors');
var math = require('gamejs/utils/math');
var enemy = require('./enemy');
var mapReader = require('./map_reader');
var map = require('./map');
var flyby = require('./flyby');
var turret = require('./turret');
var trigger = require('./trigger');
var structure = require('./buildings');
var player = require('gamejs/mixer');
var _ev = require('gamejs/event');
var particles = require('./particles');

var STEER_NONE		= 0;
var STEER_RIGHT		= 1;
var STEER_LEFT		= 2;

var NO_FIRE1		= 0;
var FIRE1			= 1;
var FIRE1			= 1;

var ACC_NONE		= 0;
var ACC_ACCELERATE	= 1;
var ACC_BRAKE		= 2;


var scale			= 15;
var width_m			= width_px / scale;
var height_m		= height_px / scale;
var keys_down		= {};
var _game_mouse_pos = [0, 0];
var mouse_pointer	= {};

var player_shots	= new gamejs.sprite.Group();
var enemy_shots		= new gamejs.sprite.Group();

var all_shots		= new gamejs.sprite.Group();
var shots_array		= new Array();



var game_success_ev = false;
var b2world;
var tank;
var enemies			= new gamejs.sprite.Group();
var turrets         = new gamejs.sprite.Group();
var structures      = new gamejs.sprite.Group();
var particleSystem	= new gamejs.sprite.Group();

var msDurationLimit 		= 70; // ogranicza "clipping" czy jakos tak..
var maxAnimationsOnScreen 	= 10; // maksymalna ilosc na raz wyswietlanych animacji
var maxSmudgesOnScreen		= 50; // maksymalna ilosc na raz wyswietlanych smug

var explosionAnims		= [];
var currentAnimations	= [];
var itemsArray			= [];
var itemsArrayTemp		= [];
var smudgesArray		= [];
var flybyList			= new gamejs.sprite.Group();
var missionSchema       = {};
var missionScenario     = [];
var deployPoints        = [];
var Sprite				= null;


var display;
var cmap;
var mainSurface;
var mapObject;
var _gameStatus;

/********** SPRITES AND ANIMATION
 * 
 */

var SpriteContainer = function(){
	
	var anim_cache = [];
	
	this.getExplosion = function(number){
		return getCache(parseInt(number), explosionTypes[number]);
	};
	
	var findInCache = function(number){
		return (typeof anim_cache[number] != "undefined");
	};
	
	var _get = function(id){
		if (anim_cache.length){
			for(i in anim_cache)
				if (anim_cache[i].id == id)
					return anim_cache[i].data;
		}
		return false;
	};
	
	var getCache = function(number, settings){
		var _g = _get(number);
		if (_g !== false){
			return new Animation(_g.exp, _g.data, _g.fps);
		}
		else {
			var expDimensions = {width: settings.width, height: settings.height};
			var exp = new SpriteSheet(settings.image, expDimensions);
			anim_cache.push({id: number, data: {exp: exp, data: {'anim': settings.anim, 'pause': settings.pause, width: expDimensions.width, height: expDimensions.height, particle: settings.particle || {}}, fps: settings.fps}});
			return new Animation(exp, {'anim': settings.anim, 'pause': settings.pause, width: expDimensions.width, height: expDimensions.height, particle: settings.particle || {}}, settings.fps);
		}
	};
	
	var SpriteSheet = function(imagePath, sheetSpec){
		this.get = function(id){
			return surfaceCache[id];
		};
		
		var width = sheetSpec.width;
		var height = sheetSpec.height;
		var image = gamejs.image.load(imagePath);
		var surfaceCache = [];
		var imgSize = new gamejs.Rect([0,0],[width,height]);
	    
		// extract the single images from big spritesheet image
		for (var i=0; i<image.rect.height; i+=height){
			for (var j=0;j<image.rect.width;j+=width){
				var surface = new gamejs.Surface([width, height]);
				var rect = new gamejs.Rect(j, i, width, height);
				surface.blit(image, imgSize, rect);
				surfaceCache.push(surface);
			}
		}
		return this;
	};
};



// Animation

var Animation = function(spriteSheet, animationSpec, fps){
	this.fps = fps || 6;
    this.frameDuration = global_refresh_rate / this.fps;
    this.spec = animationSpec;
    this.currentFrame = null;
    this.currentFrameDuration = 0;
    this.currentAnimation = null;
    this.spriteSheet = spriteSheet;
	this.started = false;
	this.loopFinished = false;
	this.width = animationSpec.width;
	this.height = animationSpec.height;
	this.particle = animationSpec.particle || {};
    this.image = null;
    return this;
};

Animation.prototype.start = function(animation){
	this.currentAnimation = animation;
    this.currentFrame = this.spec[animation][0];
    this.currentFrameDuration = 0;
	this.started = true;
	this.loopFinished = false;
    //this.update(0);
	
    return;
};

Animation.prototype.update = function(msDuration){
	if (!this.currentAnimation){
		throw new Error('No animation started. call start("fooCycle") before updating');
	}

	this.currentFrameDuration += msDuration;
	if (this.currentFrameDuration >= this.frameDuration){
		this.currentFrame++;
		this.currentFrameDuration = 0;
		// loop back to first frame if animation finished or single frame
		var aniSpec = this.spec[this.currentAnimation];
		if (aniSpec.length == 1 || this.currentFrame > aniSpec[1]){
			this.loopFinished = true;
			this.started = false;
			// unless third argument is false, which means: do not loop
			if (aniSpec.length === 3 && aniSpec[2] === false){
				this.currentFrame--;
			}
			else{
				this.currentFrame = aniSpec[0];
			}
		}
	}
	this.image = this.spriteSheet.get(this.currentFrame);
	return;
};



/********** PLAYER SHOT CLASS
 * 
 */


var Shots = {
	array: [],
	
	Add: function(def){
		def.type = def.shot.type;
		def._counter = 0;
		
		if (SHOOT_SPRITE == def.type){
			var image = gamejs.image.load(def.shot.image),
				dims = image.getSize();
			if (def.shot.inherit_angle){
				def.image = gamejs.transform.rotate(image, def.angle);
			}
			else {
				def.image = gamejs.transform.rotate(image, 0);
			}
			def.rect = new gamejs.Rect([def.x, def.y], dims);
			def.size = dims;
			
			def.accelerator = 0;
			def._distance = 0;
		}
		else if (SHOOT_BEAM == def.type){
			
			if (def.target == 'enemy'){
			
				var end_point = [def.x, def.y];
				var vect = [1 * Math.sin(deg2rad(def.angle)), -1 * Math.cos(deg2rad(def.angle))];
				var factor = 3;
				var nvect = [factor * vect[0], factor * vect[1]];
				
				var brk = false;
				
				var hit_array = [];
		        var turret_hit_array = [];
		        var building_hit_array = [];
				
				for(var i = 1; i <= 2000; i++){
					end_point = [end_point[0] += nvect[0], end_point[1] += nvect[1]];
					if (cmap.mapCheckPixel(end_point))
						brk = true;
					
					var enemy_cnt = 0;
					enemies.forEach(function(e){
						enemy_cnt++;
						if (typeof hit_array[enemy_cnt] == "undefined"){
							if (e.isHit([end_point[0], end_point[1]], [1, 1], [0, 0])){
								hit_array[enemy_cnt] = 1;
								if (!def.shot.through)
									brk = true;
								playerDefaults.userStats.shots_aimed++;
								if (!e.hit(def.shot.power, [0, 0], cmap, def.shot.push)){
									var vx = e.x + (e.size[0]/2), vy = e.y + (e.size[1]/2);
									var cash = e.settings.strength / 7;
									playerDefaults.userStats.kills++;
		                            setIdScenario(e.id);
									playerDefaults.userStats.damage += e.settings.strength;
									if (e.life_time > 800 && e.life_time < 2200)
										cash *= 0.8;
									else if (e.life_time >= 2200)
										cash *= 0.5;
									tank.addCash(cash);
									if (display_points)
										flybyList.add(new flyby.Flyby('+'+parseInt(cash), e.getCoords(), '#99FFCC'));
									hit_array.splice(hit_array.indexOf(hit_array[enemy_cnt]), 1);
									currentAnimations.push({obj:Sprite.getExplosion(e.settings.explosion_type), pos: [vx, vy], all: explosionTypes[e.settings.explosion_type]});
		                            e.kill();
		                            playSingleSound(explosionTypes[e.settings.explosion_type].explode_sound);
								}
							}
						}
					});
		            var turret_cnt = 0;
		            turrets.forEach(function(e){
		                turret_cnt++;
		                if (typeof turret_hit_array[turret_cnt] == "undefined"){
		                    if (e.isHit([end_point[0], end_point[1]], [1, 1], [0, 0])){
		                        turret_hit_array[turret_cnt] = 1;
		                        if (!def.shot.through)
		                            brk = true;
		                        playerDefaults.userStats.shots_aimed++;
		                        if (!e.hit(def.shot.power, [0, 0], cmap, def.shot.push)){
		                            var vx = e.x + (e.size[0]/2), vy = e.y + (e.size[1]/2);
		                            var cash = e.settings.strength / 7;
		                            playerDefaults.userStats.kills++;
		                            setIdScenario(e.id);
		                            playerDefaults.userStats.damage += e.settings.strength;
		                            if (e.life_time > 800 && e.life_time < 2200)
		                                cash *= 0.8;
		                            else if (e.life_time >= 2200)
		                                cash *= 0.5;
		                            tank.addCash(cash);
		                            e.kill();
		                            if (display_points)
		                            	flybyList.add(new flyby.Flyby('+'+parseInt(cash), e.getCoords(), '#99FFCC'));
		                            cmap.updateElements(structures, turrets);
		                            hit_array.splice(hit_array.indexOf(hit_array[enemy_cnt]), 1);
		                            currentAnimations.push({obj:Sprite.getExplosion(e.settings.explosion_type), pos: [vx, vy], all: explosionTypes[e.settings.explosion_type]});
		                            playSingleSound(explosionTypes[e.settings.explosion_type].explode_sound);
		                        }
		                    }
		                }
		            });
	
		            var buildings_cnt = 0;
		            structures.forEach(function(e){
		                buildings_cnt++;
		                if (typeof building_hit_array[buildings_cnt] == "undefined"){
		                    if (e.isHit([end_point[0], end_point[1]], [1, 1], [0, 0])){
		                        building_hit_array[buildings_cnt] = 1;
		                        if (!def.shot.through)
		                            brk = true;
		                        playerDefaults.userStats.shots_aimed++;
		                        if (!e.hit(def.shot.power, [0, 0], cmap, def.shot.push)){
		                            var vx = e.x + (e.size[0]/2), vy = e.y + (e.size[1]/2);
		                            var cash = e.settings.strength / 7;
		                            playerDefaults.userStats.kills++;
		                            setIdScenario(e.id);
		                            playerDefaults.userStats.damage += e.settings.strength;
		                            if (e.life_time > 800 && e.life_time < 2200)
		                                cash *= 0.8;
		                            else if (e.life_time >= 2200)
		                                cash *= 0.5;
		                            tank.addCash(cash);
		                            e.kill();
		                            if (display_points)
		                            	flybyList.add(new flyby.Flyby('+'+parseInt(cash), e.getCoords(), '#99FFCC'));
		                            cmap.updateElements(structures, turrets);
		                            hit_array.splice(hit_array.indexOf(hit_array[enemy_cnt]), 1);
		                            currentAnimations.push({obj:Sprite.getExplosion(e.settings.explosion_type), pos: [vx, vy], all: explosionTypes[e.settings.explosion_type]});
		                            playSingleSound(explosionTypes[e.settings.explosion_type].explode_sound);
		                        }
		                    }
		                }
		            });
		            
					if (brk)
						break;
				}
				def.end_point = end_point;
				def._counter = 0;
			}
			else if (def.target == "player"){
				var end_point = [def.x, def.y];
				var vect = [1 * Math.sin(deg2rad(def.angle)), -1 * Math.cos(deg2rad(def.angle))];
				var factor = 3;
				var nvect = [factor * vect[0], factor * vect[1]];
				
				var brk = false,
					__hit = false;
				
				for(var i = 1; i <= 2000; i++){
					end_point = [end_point[0] += nvect[0], end_point[1] += nvect[1]];
					if (cmap.mapCheckPixel(end_point))
						brk = true;
					
					if (!tank.player.IsDead() && tank.isHit([end_point[0], end_point[1]], [1, 1], [0, 0])){
						brk = true;
						__hit = true;
						tank.hit(def.shot.power);
						
						if (playerDefaults.level_difficulty > 3)
							tank.addCash(-parseInt(def.shot.power / playerDefaults.level_difficulty_modifier));
						
						currentAnimations.push({obj:Sprite.getExplosion(def.shot.explosion_type), pos: [def.x, def.y], all: explosionTypes[def.shot.explosion_type]});
		                playSingleSound(def.shot.hit_sound);
					}
					
					if (brk)
						break;
				}
				
				if (!__hit)
					def.unit._missed++;
				
				def.end_point = end_point;
				def._counter = 0;
			}
		};
		
		this.array.push(def);
	},
	
	
	_ShotSprite: function(def, msDuration){
		
		if (def.x > cmap.size[0] * 32 || def.x < 0 || def.y < 0 || def.y > cmap.size[1] * 32){
			return -1;
		}
		
		var _fvect = [1 * Math.sin(deg2rad(def.angle)), -1 * Math.cos(deg2rad(def.angle))];
		var _bvect = [def.shot.speed * _fvect[0], def.shot.speed * _fvect[1]];
		var _bvect_b = [(def.shot.speed / 2) * _fvect[0], (def.shot.speed / 2) * _fvect[1]];
		
		if (def.shot.acceleration){
			def.accelerator += def.shot.acceleration_factor;
			_bvect = [(def.shot.speed + def.accelerator) * _fvect[0], (def.shot.speed + def.accelerator) * _fvect[1]];
		}
		
		if (cmap.mapSimpleCollider([def.x - def.size[0]/2, def.y - def.size[1]/2], def.size, [_bvect[0] * (msDuration/global_refresh_rate), _bvect[1] * (msDuration/global_refresh_rate)])){
            playSingleSound(def.shot.hit_sound);
			currentAnimations.push({obj:Sprite.getExplosion(def.shot.explosion_type), pos: [def.x, def.y], all: explosionTypes[def.shot.explosion_type]});
			if (def.shot.range > 0){
				Shots._ExplodeRange(def);
			}
			if (def.target == 'player'){
				def.unit._missed++;
			}
			
			return -1;
		}
		
		if (def.target == 'enemy'){
			var ret = false;
			enemies.forEach(function(e){
				if (e.isHit([def.x - def.size[0]/2, def.y - def.size[1]/2], def.size, [_bvect[0] * (msDuration/global_refresh_rate), _bvect[1] * (msDuration/global_refresh_rate)]) || 
					e.isHit([def.x - def.size[0]/2, def.y - def.size[1]/2], def.size, [_bvect_b[0] * (msDuration/global_refresh_rate), _bvect_b[1] * (msDuration/global_refresh_rate)])){
					
					playerDefaults.userStats.shots_aimed++;
                    playSingleSound(def.shot.hit_sound);
					currentAnimations.push({obj:Sprite.getExplosion(def.shot.explosion_type), pos: [def.x, def.y], all: explosionTypes[def.shot.explosion_type]});
					if (def.shot.range > 0){
						Shots._ExplodeRange(def);
					}
					
					if (!e.hit(def.shot.power, [_bvect[0] * (msDuration/global_refresh_rate), _bvect[1] * (msDuration/global_refresh_rate)], cmap, def.shot.push)){
						var vx = e.x + (e.size[0]/2), vy = e.y + (e.size[1]/2);
						var cash = e.settings.strength / 7;
						playerDefaults.userStats.kills++;
                        setIdScenario(e.id);
						playerDefaults.userStats.damage += e.settings.strength;
						if (e.life_time > 800 && e.life_time < 2200)
							cash *= 0.8;
						else if (e.life_time >= 2200)
							cash *= 0.5;
						tank.addCash(cash);
						e.kill();
						if (display_points)
							flybyList.add(new flyby.Flyby('+'+parseInt(cash), e.getCoords(), '#99FFCC'));
						currentAnimations.push({obj:Sprite.getExplosion(e.settings.explosion_type), pos: [vx, vy], all: explosionTypes[e.settings.explosion_type]});
                        playSingleSound(explosionTypes[e.settings.explosion_type].explode_sound);
					}
					ret = true;
					return;
				}
			});

            turrets.forEach(function(e){
                if (e.isHit([def.x - def.size[0]/2, def.y - def.size[1]/2], def.size, [_bvect[0] * (msDuration/global_refresh_rate), _bvect[1] * (msDuration/global_refresh_rate)]) ||
                	e.isHit([def.x - def.size[0]/2, def.y - def.size[1]/2], def.size, [_bvect_b[0] * (msDuration/global_refresh_rate), _bvect_b[1] * (msDuration/global_refresh_rate)])){
                	
                    playerDefaults.userStats.shots_aimed++;
                    playSingleSound(def.shot.hit_sound);
                    currentAnimations.push({obj:Sprite.getExplosion(def.shot.explosion_type), pos: [def.x, def.y], all: explosionTypes[def.shot.explosion_type]});
                    if (def.shot.range > 0){
                    	Shots._ExplodeRange(def);
                    }
                    
                    if (!e.hit(def.shot.power, [_bvect[0] * (msDuration/global_refresh_rate), _bvect[1] * (msDuration/global_refresh_rate)], cmap, def.shot.push)){
                        var vx = e.x + (e.size[0]/2), vy = e.y + (e.size[1]/2);
                        var cash = e.settings.strength / 7;
                        playerDefaults.userStats.kills++;
                        setIdScenario(e.id);
                        playerDefaults.userStats.damage += e.settings.strength;
                        if (e.life_time > 800 && e.life_time < 2200)
                            cash *= 0.8;
                        else if (e.life_time >= 2200)
                            cash *= 0.5;
                        tank.addCash(cash);
                        e.kill();
                        if (display_points)
                        	flybyList.add(new flyby.Flyby('+'+parseInt(cash), e.getCoords(), '#99FFCC'));
                        cmap.updateElements(structures, turrets);
                        currentAnimations.push({obj:Sprite.getExplosion(e.settings.explosion_type), pos: [vx, vy], all: explosionTypes[e.settings.explosion_type]});
                        playSingleSound(explosionTypes[e.settings.explosion_type].explode_sound);
                    }
                    ret = true;
                    return;
                }
            });

            structures.forEach(function(e){
                if (e.isHit([def.x - def.size[0]/2, def.y - def.size[1]/2], def.size, [_bvect[0] * (msDuration/global_refresh_rate), _bvect[1] * (msDuration/global_refresh_rate)]) || 
                	e.isHit([def.x - def.size[0]/2, def.y - def.size[1]/2], def.size, [_bvect_b[0] * (msDuration/global_refresh_rate), _bvect_b[1] * (msDuration/global_refresh_rate)])){
                	
                    playerDefaults.userStats.shots_aimed++;
                    playSingleSound(def.shot.hit_sound);
                    currentAnimations.push({obj:Sprite.getExplosion(def.shot.explosion_type), pos: [def.x, def.y], all: explosionTypes[def.shot.explosion_type]});
                    if (def.shot.range > 0){
                        Shots._ExplodeRange(def);
                    }
                    
                    if (!e.hit(def.shot.power, [_bvect[0] * (msDuration/global_refresh_rate), _bvect[1] * (msDuration/global_refresh_rate)], cmap, def.shot.push)){
                        var vx = e.x + (e.size[0]/2), vy = e.y + (e.size[1]/2);
                        var cash = e.settings.strength / 7;
                        playerDefaults.userStats.kills++;
                        setIdScenario(e.id);
                        playerDefaults.userStats.damage += e.settings.strength;
                        if (e.life_time > 800 && e.life_time < 2200)
                            cash *= 0.8;
                        else if (e.life_time >= 2200)
                            cash *= 0.5;
                        tank.addCash(cash);
                        e.kill();
                        if (display_points)
                        	flybyList.add(new flyby.Flyby('+'+parseInt(cash), e.getCoords(), '#99FFCC'));
                        cmap.updateElements(structures, turrets);
                        currentAnimations.push({obj:Sprite.getExplosion(e.settings.explosion_type), pos: [vx, vy], all: explosionTypes[e.settings.explosion_type]});
                        playSingleSound(explosionTypes[e.settings.explosion_type].explode_sound);
                    }
                    ret = true;
					return;
                }
            });
            if (ret)
            	return -1;
		}
		else if (def.target == 'player' && !tank.player.IsDead()){
			if (tank.isHit([def.x - def.size[0]/2, def.y - def.size[1]/2], def.size, [_bvect[0] * (msDuration/global_refresh_rate), _bvect[1] * (msDuration/global_refresh_rate)])){
				playerDefaults.userStats.received += def.shot.power;
				tank.hit(def.shot.power);
				if (playerDefaults.level_difficulty > 3)
					tank.addCash(-parseInt(def.shot.power / playerDefaults.level_difficulty_modifier));
				currentAnimations.push({obj:Sprite.getExplosion(def.shot.explosion_type), pos: [def.x, def.y], all: explosionTypes[def.shot.explosion_type]});
                playSingleSound(def.shot.hit_sound);
				return -1;
			}
		}
		
		var _xp = def.x,
			_yp = def.y;
		
		def.x += _bvect[0] * (msDuration/global_refresh_rate);
		def.y += _bvect[1] * (msDuration/global_refresh_rate);
		
		var _xd = _xp - def.x,
			_yd = _yp - def.y;
		
		def._distance += Math.sqrt(_xd*_xd + _yd*_yd);
		
		if (def.shot.distance > 0 && def._distance >= def.shot.distance){
			if (def.shot.distance_explode){
				var _exp = def.shot.distance_explode_image || def.shot.explosion_type;
				currentAnimations.push({obj:Sprite.getExplosion(_exp), pos: [def.x, def.y], all: explosionTypes[_exp]});
				
                if (def.shot.hit_sound || def.shot.distance_explode_sound){
                    playSingleSound(def.shot.distance_explode_sound || def.shot.hit_sound);
				}
                
                if (def.shot.distance_explode_range){
                	Shots._ExplodeRange(def);
                }
			}
			return -1;
		}
		
	},
	
	_ExplodeRange: function(def){
		enemies.forEach(function(e){
			var dist = Math.round(getAbsoluteDistance([e.x + (e.size[0]/2), e.y + (e.size[1]/2)], [def.x, def.y]) * 2);
			if (dist <= def.shot.range){
				var power = Math.round(def.shot.power * (100 - ((dist * 100) / def.shot.range)) / 100);
				if (power < 0) power = 1;
				if (!e.hit(power, [0, 0], cmap)){
					var vx = e.x + (e.size[0]/2), vy = e.y + (e.size[1]/2);
					var cash = e.settings.strength / 10;
					if (def.target != 'player'){
						playerDefaults.userStats.kills++;
						playerDefaults.userStats.damage += power;
						if (e.life_time > 800 && e.life_time < 2200)
							cash *= 0.8;
						else if (e.life_time >= 2200)
							cash *= 0.5;
						tank.addCash(cash);
					}
					setIdScenario(e.id);
					e.kill();
					if (display_points)
						flybyList.add(new flyby.Flyby('+'+parseInt(cash), e.getCoords(), '#99FFCC'));
					currentAnimations.push({obj:Sprite.getExplosion(e.settings.explosion_type), pos: [vx, vy], all: explosionTypes[e.settings.explosion_type]});
                    playSingleSound(explosionTypes[e.settings.explosion_type].explode_sound);
				}
			}
		});
		
        turrets.forEach(function(e){
            var dist = Math.round(getAbsoluteDistance([e.x + (e.size[0]/2), e.y + (e.size[1]/2)], [def.x, def.y]) * 2);
            if (dist <= def.shot.range){
                var power = Math.round(def.shot.power * (100 - ((dist * 100) / def.shot.range)) / 100);
                if (power < 0) power = 1;
                if (!e.hit(power, [0, 0], cmap)){
                    var vx = e.x + (e.size[0]/2), vy = e.y + (e.size[1]/2);
                    var cash = e.settings.strength / 10;
                    if (def.target != 'player'){
                        playerDefaults.userStats.kills++;
                        playerDefaults.userStats.damage += power;
                        if (e.life_time > 800 && e.life_time < 2200)
                            cash *= 0.8;
                        else if (e.life_time >= 2200)
                            cash *= 0.5;
                        tank.addCash(cash);
                    }
                    setIdScenario(e.id);
                    e.kill();
                    if (display_points)
                    	flybyList.add(new flyby.Flyby('+'+parseInt(cash), e.getCoords(), '#99FFCC'));
                    cmap.updateElements(structures, turrets);
                    currentAnimations.push({obj:Sprite.getExplosion(e.settings.explosion_type), pos: [vx, vy], all: explosionTypes[e.settings.explosion_type]});
                    playSingleSound(explosionTypes[e.settings.explosion_type].explode_sound);
                }
            }
        });

        structures.forEach(function(e){
            var dist = Math.round(getAbsoluteDistance([e.x + (e.size[0]/2), e.y + (e.size[1]/2)], [def.x, def.y]) * 2);
            if (dist <= def.shot.range){
                var power = Math.round(def.shot.power * (100 - ((dist * 100) / def.shot.range)) / 100);
                if (power < 0) power = 1;
                if (!e.hit(power, [0, 0], cmap)){
                    var vx = e.x + (e.size[0]/2), vy = e.y + (e.size[1]/2);
                    var cash = e.settings.strength / 10;
                    if (def.target != 'player'){
                        playerDefaults.userStats.kills++;
                        playerDefaults.userStats.damage += power;
                        if (e.life_time > 800 && e.life_time < 2200)
                            cash *= 0.8;
                        else if (e.life_time >= 2200)
                            cash *= 0.5;
                        tank.addCash(cash);
                    }
                    setIdScenario(e.id);
                    e.kill();
                    if (display_points)
                    	flybyList.add(new flyby.Flyby('+'+parseInt(cash), e.getCoords(), '#99FFCC'));
                    cmap.updateElements(structures, turrets);
                    currentAnimations.push({obj:Sprite.getExplosion(e.settings.explosion_type), pos: [vx, vy], all: explosionTypes[e.settings.explosion_type]});
                    playSingleSound(explosionTypes[e.settings.explosion_type].explode_sound);
                }
            }
        });
	},
	
	Update: function(msDuration){
		if (this.array.length){
			for(i in this.array){
				var e = this.array[i];
				if (SHOOT_SPRITE == e.type){
					if (-1 == this._ShotSprite(e, msDuration)){
						var pos = this.array.indexOf(e);
						if (~pos){
							e.image = null;
							this.array.splice(pos, 1);
						}
						continue;
					}
					else {
						this.Draw(e);
						
						if (e.shot.smoketrail == 1){
							var def = {
								maxParticles: e.shot.smokerate || 1,
								emissionRate: e.shot.smokerate || 1,
								particleSize: 1,
								lifeTime: e.shot.smoke_lifetime || 14,
								fadeTime: e.shot.smoke_fadetime || 3,
								image: organizer.add("images/particles/particle05.png"),
								velocity: 0.1,
								passing: false
							};
							particleSystem.add(new particles.Particles(def, [e.x, e.y]));
						}
					}
				}
				else if (SHOOT_BEAM == e.type){
					var alpha = Math.round(100 - ((e._counter * 100) / e.shot.stay_time)) / 100;
					e._counter++;
					if (e._counter >= e.shot.stay_time){
						var pos = this.array.indexOf(e);
						if (~pos)
							this.array.splice(pos, 1);
						continue;
					}
					gamejs.draw.line(mainSurface, 'rgba('+e.shot.color+', '+alpha+')', [viewport_offset[0] + e.x, viewport_offset[1] + e.y], [viewport_offset[0] + e.end_point[0], viewport_offset[1] + e.end_point[1]], e.shot.width);
				}
			}
		}
	},
	
	Empty: function(){
		this.array = [];
	},
	
	Draw: function(def){
		mainSurface.blit(def.image, [viewport_offset[0] + def.x - def.size[0]/2, viewport_offset[1] + def.y - def.size[1]/2]);
	}
};



/********** ITEMS CLASS
 * 
 */

var Items = function(item, coords, id){
	Items.superConstructor.apply(this, arguments);
	this.data = getItem(item);
	this.width_px = this.data.size[0];
	this.height_px = this.data.size[1];
	this.angle = 0;
    this.id = id;
	this.type = item;
	this.distance = 0;
	this.max_distance = 15;
	this.coords = coords;
	this.x = (coords[0] * 32) + ((32 - this.data.size[0]) / 2);
	this.y = (coords[1] * 32) + ((32 - this.data.size[1]) / 2);
	this.originalImage = gamejs.image.load(this.data.image);
	this.originalImage = gamejs.transform.scale(this.originalImage, this.data.size);
	this.current_scale = [1, 1];
	this.final_scale = this.data.size;
	this.scallable = true;
	this.stage = 0;
	
	this.image = gamejs.transform.scale(this.originalImage, this.current_scale);
	
	this.update = function(msDuration){
		if (this.scallable){
			this.stage += 10;
			var w = Math.round([this.final_scale[0] * this.stage] / 100),
				h = Math.round([this.final_scale[1] * this.stage] / 100);
			this.image = gamejs.transform.scale(this.originalImage, [w, h]);
			mainSurface.blit(this.image, [viewport_offset[0] + this.x + (this.final_scale[0]/2) - (w/2), viewport_offset[1] + this.y + (this.final_scale[1]/2) - (h/2)]);
			if (w >= this.final_scale[0]){
				this.scallable = false;
			}
		}
		else mainSurface.blit(this.image, [viewport_offset[0] + this.x, viewport_offset[1] + this.y]);
	};
	
	this.picked = function(coords, dims){
		var x = coords[0] - (dims[0] / 2),
			y = coords[1] - (dims[1] / 2),
			x1 = coords[0] + (dims[0] / 2),
			y1 = coords[1] + (dims[1] / 2);
		var _x = this.x,
			_y = this.y,
			_x1 = this.x + this.data.size[0],
			_y1 = this.y + this.data.size[1];
		
		if ((x >= _x && y >= _y && x <= _x1 && y <= _y1) ||
			(x1 <= _x1 && y >= _y && x1 >= _x && y <= _y1) ||
			(x >= _x && y1 <= _y1 && x <= _x1 && y1 >= _y) ||
			(x1 <= _x1 && y1 <= _y1 && x1 >= _x && y1 >= _y)){
			return true;
		}
		
		if ((x >= _x && x <= _x1 && y <= _y && y1 >= _y1) ||
			(x1 >= _x && x1 <= _x1 && y <= _y && y1 >= _y1) ||
			(y1 <= _y1 && y1 >= _y && x <= _x && x1 >= _x1) ||
			(y >= _y && y <= _y1 && x <= _x && x1 >= _x1)){
			return true;
		}
		return false;
	};
	
	this.preKill = function(){
		this.kill();
	};
};
gamejs.utils.objects.extend(Items, gamejs.sprite.Sprite);


/********** TANK CLASS
 * 
 */

var Tank = function(respawn, defaults){
	Tank.superConstructor.apply(this, arguments);

	this.originalImage = gamejs.image.load(playerDefaults.image);
	this.tankShad = gamejs.image.load(playerDefaults.shadow);
	this.towerImage = gamejs.image.load(playerDefaults.tower);
	this.towerShad = gamejs.image.load(playerDefaults.tower_shadow);

    this.helperArrow = gamejs.image.load(playerDefaults.helper_arrow);
    var dims = this.originalImage.getSize();
	
	this.shieldImage = gamejs.image.load(playerDefaults.shield);
	this.flareImage = gamejs.image.load(playerDefaults.flare);
	this.originalImage = gamejs.transform.scale(this.originalImage, [dims[0] * (0.5), dims[1] * (0.5)]);
	this.tankShad = gamejs.transform.scale(this.tankShad, [dims[0] * (0.5), dims[1] * (0.5)]);
	this.towerImage = gamejs.transform.scale(this.towerImage, [dims[0] * (0.5), dims[1] * (0.5)]);
	this.towerShad = gamejs.transform.scale(this.towerShad, [dims[0] * (0.5), dims[1] * (0.5)]);
	
	dims = this.originalImage.getSize();
    this.show_arrow = false;
	this.dims = dims;
	this.rotation = respawn.angle;
	this.direction = 2;
	this.rotate_speed = defaults.rotate_speed;
	this.rotation_ended = true;
	this.temp_rotation = 0;
	this.image = gamejs.transform.rotate(this.originalImage, this.rotation);
	this.shad = gamejs.transform.rotate(this.tankShad, this.rotation);
	this.rect = new gamejs.Rect(coordsToPixels(respawn.coords), dims);
	this.ship_width = dims[0];
	this.ship_height = dims[1];
	this.max_steer_angle = defaults.max_steer_angle;
	this.wheel_angle = this.rotation;
    this.tower_angle = this.wheel_angle;
	
	this.viewfinder_angle = null;
	this.viewfinder_end = [0, 0];
	this.is_hidden = false;
	
	this.acceleration = .0;
	this.power = 0;
	this.reverse = 0;
	this.factor = defaults.acceleration_factor;
	this.max_speed = defaults.speed;
	this.slow_down_key = false;
	this.defaults = defaults;
	
	this.current_shot_type = defaults.init_gun;
	this.current_acceleration_amount = 0;
	
	this.fire = NO_FIRE1;
	this.fire_lock = false;
	this.next_fire = 0;
	
	this.player = {
		_parent			: null,
		_hit_shield		: 0,
		_flare_time		: 0,
		_tmp_money		: 0,
		
		money			: defaults.init_money,
		health			: defaults.init_health,
		max_health		: defaults.max_health,
		armor			: defaults.init_armor,
		ammo			: defaults.init_ammo,
		has_viewfinder	: defaults.init_has_viewfinder,
		afterburner		: defaults.init_afterburner,
		_keys			: [],
		h_opacity		: 0,
		dead			: false,
		
		ammo_array		: [],
		weapon_array	: [],
		
		SetParent: function(obj){
			this._parent = obj;
		},
		
		SetWeaponDefaultStatus: function(){
			for(i in shotTypes){
				this.weapon_array[i] = false;
			}
		},
		
		IsDead: function(){
			return this.dead;
		},
		
		AddKey: function(key){
			this._keys[key] = true;
		},
		
		GotKey: function(key){
			return this._keys[key] != undefined;
		},
		
		ResetKey: function(){
			this._keys = [];
		},
		
		AddWeapon: function(weapon){
			this.weapon_array[weapon] = true;
			return this;
		},
		
		GetWeapon: function(weapon){
			return this.weapon_array[weapon] == true;
		},
		
		SearchWeapon: function(direction){
			if (direction > 0){
				for(i = this._parent.current_shot_type+1; i <= shotTypes.length; i++)
					if (this.weapon_array[i])
						return i;
			}
			else if (direction < 0){
				for(i = this._parent.current_shot_type-1; i > 0; i--)
					if (this.weapon_array[i])
						return i;
			}
			return false;
		},
		
		Hit: function(weapon){
			
		},
		
		GetHurt: function(){
			this.h_opacity = playerDefaults.hurt_opacity;
		},
		
		DisplayHurt: function(){
			this.h_opacity -= playerDefaults.hurt_fade_speed;
			if (this.h_opacity < 0)
				this.h_opacity = 0;
			gamejs.draw.rect(mainSurface, 'rgba(255, 0, 10, '+this.h_opacity+')', new gamejs.Rect([0, 0], [viewport_width, viewport_height]));
		},
		
		GetAmmo: function(weapon){
			return typeof this.ammo_array[weapon] != "undefined" ? this.ammo_array[weapon] : false;
		},
		
		SetAmmo: function(weapon, amount){
			this.ammo_array[weapon] = parseInt(amount);
		},
		
		AddAmmo: function(weapon, amount){
			this.ammo_array[weapon] += parseInt(amount);
		},
		
		ShotAmmo: function(weapon, amount){
			this.ammo_array[weapon] -= parseInt(amount);
		},
		
		ActivateShield: function(){
			this._hit_shield = playerDefaults.shield_stay_time;
			playSingleSound(playerDefaults.shield_hit_sound);
		},
		
		LevelUp: function(){
			
		},
		
		DisplayShield: function(shield, pos){
			if (this._hit_shield <= 0)
				return;
			var opacity = ((this._hit_shield*100) / playerDefaults.shield_stay_time) / 100;
			shield.setAlpha(1 - opacity);
			display.blit(shield, [viewport_offset[0] + pos[0], viewport_offset[1] + pos[1]]);
			this._hit_shield--;
		},
		
		ActivateFlare: function(){
			this._flare_time = playerDefaults.flare_time;
		},
		
		Flare: function(flare, pos){
			if (this._parent.current_weapon.has_flare && this._flare_time > 0){
				var dm = this._parent.originalImage.getSize();
				var origin = [1 * Math.sin(deg2rad(this._parent.tower_angle)), -1 * Math.cos(deg2rad(this._parent.tower_angle))];
				var x = (this._parent.actual_position[0] + origin[0] * 24) - (dm[0]/3),
					y = (this._parent.actual_position[1] + origin[1] * 24) - (dm[1]/3);
				
				var opacity = ((this._flare_time*100) / playerDefaults.flare_time) / 100;
				_f = gamejs.transform.rotate(flare, this._parent.tower_angle);
				_f.setAlpha(1 - opacity);
				
				display.blit(_f, [viewport_offset[0] + x, viewport_offset[1] + y]);
				this._flare_time--;
			}
		},
		
		AddMoney: function(amount){
			this._tmp_money += amount;
		},
		
		ReceiveMoney: function(){
			this.money += this._tmp_money;
			this._tmp_money = 0;
		},
		
		GetMoney: function(){
			return this.money + this._tmp_money;
		},
		
		GetRawMoney: function(){
			return this.money;
		},
		
		ResetMoney: function(){
			this._tmp_money = 0;
		},
		
		Pickup: function(item){
			var current_item = itemsArray[item];
			
			if (typeof current_item.data.health_only != "undefined" && current_item.data.health_only == 1 && this.health == this.max_health){
				return;
			}
			
			if (current_item.data.key != undefined){
				this.AddKey(current_item.data.key);
				playSingleSound(current_item.data.pickup_sound);
				setIdScenario(current_item.id);
				itemsArray.splice(item, 1);
				return;
			}
			
			
			setIdScenario(current_item.id);
			if (current_item.data.pickup != 1)
				return;
			this.AddMoney(current_item.data.money);
			this.health += current_item.data.health;
			if (this.health > this.max_health){
				this.health = this.max_health;
			}
			this.armor += current_item.data.armor;
            this.afterburner += current_item.data.afterburner;
			
			if (current_item.data.ammo > 0){
				this.SetAmmo(this._parent.current_shot_type, this.GetAmmo(this._parent.current_shot_type) + Math.ceil(current_item.data.ammo / this._parent.current_weapon.weapon_cost > 0 ? this._parent.current_weapon.weapon_cost : 1000));
			}
			if (current_item.data.single){
				if (current_item.data.money > 0)
					flybyList.add(new flyby.Flyby(current_item.data.money, current_item.coords, current_item.data.color));
				else if (current_item.data.health > 0)
					flybyList.add(new flyby.Flyby(current_item.data.health, current_item.coords, current_item.data.color));
				else if (current_item.data.armor > 0)
					flybyList.add(new flyby.Flyby(current_item.data.armor, current_item.coords, current_item.data.color));
				else if (current_item.data.ammo > 0)
					flybyList.add(new flyby.Flyby(current_item.data.ammo, current_item.coords, current_item.data.color));
			}
			else{
				flybyList.add(new flyby.Flyby('EXTRA', current_item.coords, current_item.data.color));
			}
			playerDefaults.userStats.items++;

            playSingleSound(current_item.data.pickup_sound);

			if (current_item.data.respawn){
				itemsArrayTemp.push({coords: current_item.coords, name: current_item.data.name, time: current_item.data.respawn_time});
			}
			itemsArray.splice(item, 1);
		}
		
	};
	
	this.actual_position = coordsToPixels(respawn.coords);
	this.previous_position = this.actual_position;
	this.previous_position = this.actual_position;
	
	this.current_weapon = Object.create(shotTypes[this.current_shot_type]);
	
	return this;
};

gamejs.utils.objects.extend(Tank, gamejs.sprite.Sprite);

Tank.prototype.toggleWeapon = function(next){
	if (this.next_fire > 0)
		return;
	
	var wpid = this.player.SearchWeapon(next);
	if (wpid == false)
		return;
	
	this.current_shot_type = wpid;
	this.switchWeapon();
};

Tank.prototype.getCoords = function(){
	return [Math.floor(this.actual_position[0] / 32), Math.floor(this.actual_position[1] / 32)];
};

Tank.prototype.switchWeapon = function(){
	this.current_weapon = Object.create(shotTypes[this.current_shot_type]);
};


Tank.prototype.getAcceleration = function(dir){
	var d = typeof dir != "undefined" ? dir : 1;
	if (d == 1){
		if (this.power < this.max_speed){
			this.power += this.factor;
		}
		else this.power = this.max_speed;
		return this.power;
	}
	else {
		if (this.power > (-(this.max_speed / 2))){
			this.power -= this.factor;
		}
		else this.power = -(this.max_speed / 2);
		return this.power;
	}
};

Tank.prototype.getDeceleration = function(dir){
	var d = typeof dir != "undefined" ? dir : 1;
	if (d == 1){
		if (this.reverse < this.max_speed / 2){
			this.reverse += this.factor / 2;
		}
		else this.reverse = this.max_speed / 2;
		return this.reverse;
	}
	else {
		if (this.reverse > (-(this.max_speed / 2))){
			this.reverse -= this.factor / 2;
		}
		else this.reverse = -(this.max_speed / 2);
		return this.reverse;
	}
};

Tank.prototype.addCash = function(amount){
	if (amount < 0 && this.player.money + amount < 0)
		this.player._tmp_money = 0;
	else this.player.AddMoney(parseInt(amount));
};

Tank.prototype.getSlowDown = function(fc){
	if (this.power > 0){
		var m = fc || 4;
		this.power -= this.factor / m;
	}
	else this.power = 0;
	return this.power;
};

Tank.prototype.getSpeed = function(msDuration){
	var p = this.previous_position,
		n = [this.actual_position[0], this.actual_position[1]];
	var sp = [(n[0]-p[0]), (n[1]-p[1])];
	
	var len = vectors.len(sp);
    return (len/global_refresh_rate)*3600;
};

Tank.prototype.enableShooting = function(){
	this.fire_lock = false;
};

Tank.prototype.isHit = function(coords, size, vector){
	var x = coords[0] + (size[0] / 2) + vector[0],
		y = coords[1] + (size[1] / 2) + vector[1];
	if (x >= this.actual_position[0]-this.dims[0]/2 && x <= this.actual_position[0] + this.dims[0]/2 && y >= this.actual_position[1]-this.dims[1]/2 && y <= this.actual_position[1] + this.dims[1]/2)
		return true;
	return false;
};

Tank.prototype.hit = function(power){
	if (playerDefaults.immortality)
		return;
	
	if (this.player.armor > 0){
		var p = power * playerDefaults.armor_factor;
		this.player.ActivateShield();
		this.player.armor -= p;
		if (this.player.armor < 0){
			this.player.health += this.player.armor;
			this.player.GetHurt();
			this.player.armor = 0;
        }
	}
	else {
		playSingleSound(power > 100 ? playerDefaults.hard_hit_sound : playerDefaults.low_hit_sound);
		this.player.health -= power;
		this.player.GetHurt();
	}
	
	this.player.armor = Math.floor(this.player.armor);
	this.player.health = Math.floor(this.player.health);
	
	if (this.player.health <= 0){
		this.player.health = 0;
		this.player.dead = true;
		playerDefaults.userStats.kia++;
		currentAnimations.push({obj:Sprite.getExplosion(5), pos: this.actual_position, all: explosionTypes[5]});
		playSingleSound(explosionTypes[5].explode_sound);
		EventTimer.AttachEvent(1, function(){
			GUI.GameOver();
        });
	}
};

Tank.prototype.update = function(msDuration){
	if (this.player.IsDead())
		return;
	var incr = (this.max_steer_angle/200) * msDuration;
    this.show_arrow = false;

	if (this.steer == STEER_RIGHT){
        this.show_arrow = true;
		if (this.slow_down_key)
			this.wheel_angle += incr / this.defaults.angle_divider;
		else this.wheel_angle += incr;
    }
	else if (this.steer == STEER_LEFT){
        this.show_arrow = true;
		if (this.slow_down_key)
			this.wheel_angle -= incr / this.defaults.angle_divider;
		else this.wheel_angle -= incr;
    }
	
	if (this.wheel_angle >= 360)
		this.wheel_angle = 0;
	
	var base_vect, fvect;

	if ((this.accelerate == ACC_ACCELERATE) && (this.getSpeed() < this.max_speed)){
        if (display_helper_arrow_fwd)
            this.show_arrow = true;
        base_vect = [1 * Math.sin(deg2rad(this.wheel_angle)), -1 * Math.cos(deg2rad(this.wheel_angle))];
		var acc = this.getAcceleration();
		fvect = [acc * base_vect[0], acc * base_vect[1]];
    }
    else if (this.accelerate == ACC_BRAKE){
		if (this.power > 0){	// zwalnia
			base_vect = [1 * Math.sin(deg2rad(this.wheel_angle)), -1 * Math.cos(deg2rad(this.wheel_angle))];
			var dec = this.getSlowDown(1);
			fvect = [dec * base_vect[0], dec * base_vect[1]];
		}
		else {
			base_vect = [-1 * Math.sin(deg2rad(this.wheel_angle)), 1 * Math.cos(deg2rad(this.wheel_angle))];
			var dec = this.getDeceleration();
			fvect = [dec * base_vect[0], dec * base_vect[1]];
		}
    }
    else {
    	var dec = this.getSlowDown();
		if (this.power > 0){
			base_vect = [1 * Math.sin(deg2rad(this.wheel_angle)), -1 * Math.cos(deg2rad(this.wheel_angle))];
		} else if (this.reverse > 0) {	// wsteczny, zwalnia
			base_vect = [-1 * Math.sin(deg2rad(this.wheel_angle)), 1 * Math.cos(deg2rad(this.wheel_angle))];
			dec = this.getDeceleration(0);
		} else {
			base_vect = [0, 0];
		}
		fvect = [dec * base_vect[0], dec * base_vect[1]];
	}
	
	var _x = this.actual_position[0] + viewport_offset[0],
		_y = this.actual_position[1] + viewport_offset[1];
	
	this.tower_angle = (Math.atan2(_game_mouse_pos[1] - _y, _game_mouse_pos[0] - _x) * (180 / Math.PI)) + 90;
	
//	if ((this.tower_angle != q || this.actual_position != this.previous_position) && q !== false){
//		
//	}
	
	this.previous_position = this.actual_position;
	if (this.next_fire > 0)
		this.next_fire -= msDuration;
	if (this.next_fire < 0)
		this.next_fire = 0;
	
	if (this.fire == FIRE1){
		if (this.player.GetAmmo(this.current_shot_type) !== false && this.player.GetAmmo(this.current_shot_type) >= this.current_weapon.ammo_cost){
			while(this.next_fire <= 0){
				this.next_fire = this.current_weapon.delay;
				var dm = this.originalImage.getSize();
				var origin = [1 * Math.sin(deg2rad(this.tower_angle)), -1 * Math.cos(deg2rad(this.tower_angle))];
				var x = (this.actual_position[0] + origin[0] * 15) - (dm[0]/5) + 6,
					y = (this.actual_position[1] + origin[1] * 15) - (dm[1]/5) + 7;

				if (this.current_weapon.shot_sound){
					playSingleSound(this.current_weapon.shot_sound);
	                /*var snd = player.Sound(this.current_weapon.shot_sound);
	                snd.play(false);*/
				}

				if (this.current_weapon.spread > 0){
					for (var i = 1; i <= this.current_weapon.spread; i++){
						var min = this.tower_angle - (this.current_weapon.spread_angle),
							max = this.tower_angle + (this.current_weapon.spread_angle);
						var ang = rand(min, max);
						
						Shots.Add({
							x		: x,
							y		: y,
							angle	: ang,
							shot	: this.current_weapon,
							target	: 'enemy'
						});
						
						/*all_shots.add(new Shot({
							x		: x,
							y		: y,
							angle	: ang,
							shot	: this.current_weapon
						}, 'enemy'));*/
						playerDefaults.userStats.shots_fired++;
						this.player.ShotAmmo(this.current_shot_type, this.current_weapon.ammo_cost);
						this.player.ActivateFlare();
					}
				}
				else {
					
					Shots.Add({
						x		: x,
						y		: y,
						angle	: this.tower_angle,
						shot	: this.current_weapon,
						target	: 'enemy'
					});
					
					/*all_shots.add(new Shot({
						x		: x,
						y		: y,
						angle	: this.tower_angle,
						shot	: this.current_weapon
					}, 'enemy'));*/
					playerDefaults.userStats.shots_fired++;
					this.player.ShotAmmo(this.current_shot_type, this.current_weapon.ammo_cost);
					this.player.ActivateFlare();
				}
			}
		}
	}
	
	var col_dir = cmap.mapAdvancedCollider(
			this.actual_position,
			[this.ship_width / 2 - 3, this.ship_height / 2 - 3],
			[fvect[0] * (msDuration/global_refresh_rate), fvect[1] * (msDuration/global_refresh_rate)]
	);
	
	if (enemies._sprites.length){
		var hitted = [],
			_this = this;
		
		enemies.forEach(function(e){
			var crd = [e.x, e.y, e.x2, e.y2];
			if (_this.shipCollide(crd)){
				hitted.push(e);
			}
		});
		if (hitted.length){
			var pwr = this.getAcceleration();
			for(var i in hitted){
				var e = hitted[i];
				if (!e.hit(pwr >= playerDefaults.crash_min_speed ? pwr * playerDefaults.crash_force_factor : 0, [fvect[0] * (msDuration/global_refresh_rate), fvect[1] * (msDuration/global_refresh_rate)], cmap)){
					var vx = e.x + (e.size[0]/2), vy = e.y + (e.size[1]/2);
					var cash = pwr * playerDefaults.crash_frag_money;
					playerDefaults.userStats.kills++;
					playerDefaults.userStats.damage += pwr;
					setIdScenario(e.id);
					if (e.life_time > 800 && e.life_time < 2200)
						cash *= 0.8;
					else if (e.life_time >= 2200)
						cash *= 0.5;
					this.addCash(cash);
					if (display_points)
						flybyList.add(new flyby.Flyby('+'+parseInt(cash), e.getCoords(), '#99FFCC'));
					e.kill();
					currentAnimations.push({obj:Sprite.getExplosion(2), pos: [vx, vy], all: explosionTypes[2]});
				}
				if (!playerDefaults.immortality && playerDefaults.crash_self_damage > 0)
					this.hit(Math.floor(pwr * playerDefaults.crash_self_damage));
			}
			if (playerDefaults.crash_bounce && pwr >= playerDefaults.crash_min_bounce_speed){
				this.power = -(this.power * playerDefaults.crash_bounce_factor);
				base_vect = [-1 * Math.sin(deg2rad(this.tower_angle)), 1 * Math.cos(deg2rad(this.tower_angle))];
				var dec = Math.abs(this.getAcceleration(0));
				fvect = [dec * base_vect[0], dec * base_vect[1]];
			}
		}
	};
	
	this.previous_position = this.actual_position;
	
	if (!col_dir)
		this.actual_position = [this.actual_position[0] + fvect[0] * (msDuration/global_refresh_rate), this.actual_position[1] + fvect[1] * (msDuration/global_refresh_rate)];
	else{
		if (col_dir == 'horizontal')
			this.actual_position = [this.actual_position[0] + (fvect[0] * 0.67) * (msDuration/global_refresh_rate), this.actual_position[1]];
		else if (col_dir == 'vertical')
			this.actual_position = [this.actual_position[0], this.actual_position[1] + (fvect[1] * 0.67) * (msDuration/global_refresh_rate)];
	}
	
	if (cmap.isUnderTree(this.actual_position)){
		this.is_hidden = true;
	}
	else this.is_hidden = false;
	
	if (this.player.h_opacity > 0){
		this.player.DisplayHurt();
	}
	
	this.rect.center = this.actual_position;
	
	this.rotation = this.wheel_angle;
	this.image = gamejs.transform.rotate(this.originalImage, this.rotation);
	this.shad = gamejs.transform.rotate(this.tankShad, this.rotation);
	
	this._towerImage = gamejs.transform.rotate(this.towerImage, this.tower_angle);
	this._towerShad = gamejs.transform.rotate(this.towerShad, this.tower_angle);
};

Tank.prototype.isHidden = function(){
	return this.is_hidden;
};

Tank.prototype.moveTo = function(data){
	this.actual_position = coordsToPixels(data.coords);
	this.rotation = this.wheel_angle = data.angle;
};

Tank.prototype.shipCollide = function(coords){
	var x = this.actual_position[0] - this.dims[0]/2,
		y = this.actual_position[1] - this.dims[0]/2,
		x2 = this.actual_position[0] + this.dims[1]/2,
		y2 = this.actual_position[1] + this.dims[1]/2;
 	
	if (DEBUG){
		gamejs.draw.rect(mainSurface, '#fdc01f', new gamejs.Rect([viewport_offset[0] + x, viewport_offset[1] + y], [26, 26]), 1);
		gamejs.draw.rect(mainSurface, '#adf0af', new gamejs.Rect([viewport_offset[0] + coords[0], viewport_offset[1] + coords[1]], [coords[2]-coords[0], coords[3]-coords[1]]), 1);
	}
	
	if (coords[0] >= x && coords[0] <= x2 && coords[1] >= y && coords[1] <= y2 ||
		coords[0] >= x && coords[0] <= x2 && coords[3] >= y && coords[3] <= y2 ||
		coords[2] >= x && coords[2] <= x2 && coords[1] >= y && coords[1] <= y2 ||
		coords[2] >= x && coords[2] <= x2 && coords[3] >= y && coords[3] <= y2 ||
		
		coords[2] >= x2 && coords[2] >= x && coords[1] <= y && coords[3] >= y2 ||
		coords[0] >= x && coords[0] <= x2 && coords[1] <= y && coords[3] >= y2 ||
		
		coords[0] <= x && coords[2] >= x2 && coords[3] >= y && coords[3] <= y2 ||
		coords[0] <= x && coords[2] >= x2 && coords[1] <= y2 && coords[1] >= y ||
		
		coords[0] >= x && coords[1] >= y && coords[2] <= x2 && coords[3] <= y2
	){
		return true;
	}
	return false;
};

Tank.prototype.draw = function(){
	//var surface = gamejs.display.getSurface();
	if (this.player.health <= 0)
		return;
	
    if (!game_pause){
        if (viewport_offset[0] + this.actual_position[0] > viewport_width - viewport_margin[0] &&
        		(cmap.size[0] * 32 < viewport_width || viewport_width < cmap.size[0] * 32 + viewport_offset[0])){
        	viewport_offset[0] -= this.actual_position[0] - this.previous_position[0];
        }
        if (viewport_offset[1] + this.actual_position[1] > viewport_height - viewport_margin[1] &&
        		(cmap.size[1] * 32 < viewport_height || viewport_height < cmap.size[1] * 32 + viewport_offset[1])){
            viewport_offset[1] -= this.actual_position[1] - this.previous_position[1];
        }
        if (viewport_offset[0] + this.actual_position[0] < viewport_margin[0] &&
        		(cmap.size[0] * 32 < viewport_width || viewport_offset[0] < 0)){
            viewport_offset[0] += Math.abs(this.actual_position[0] - this.previous_position[0]);
        }
        if (viewport_offset[1] + this.actual_position[1] < viewport_margin[1] &&
        		(cmap.size[1] * 32 < viewport_height || viewport_offset[1] < 0)){
            viewport_offset[1] += Math.abs(this.actual_position[1] - this.previous_position[1]);
        }
    }
	
    mainSurface.blit(this.shad, [viewport_offset[0] + (this.actual_position[0] - this.dims[0]/2) - 2, viewport_offset[1] + (this.actual_position[1] - this.dims[1]/2) + 1]);
    mainSurface.blit(this.image, [viewport_offset[0] + this.actual_position[0] - this.dims[0]/2, viewport_offset[1] + this.actual_position[1] - this.dims[1]/2]);
	
    mainSurface.blit(this._towerShad, [viewport_offset[0] + (this.actual_position[0] - this.dims[0]/2) - 2, viewport_offset[1] + (this.actual_position[1] - this.dims[1]/2) + 1]);
    mainSurface.blit(this._towerImage, [viewport_offset[0] + this.actual_position[0] - this.dims[0]/2, viewport_offset[1] + this.actual_position[1] - this.dims[1]/2]);
	
	var s_size = this.shieldImage.getSize();
	this.player.DisplayShield(this.shieldImage, [this.actual_position[0] - s_size[0]/2, this.actual_position[1] - s_size[1]/2]);
	this.player.Flare(this.flareImage, [this.actual_position[0] - s_size[0]/2, this.actual_position[1] - s_size[1]/2]);
    if (display_helper_arrow && this.show_arrow)
	    this.drawHelperArrow();

	if (DEBUG){
		var dm = this.originalImage.getSize();
		var origin = [1 * Math.sin(deg2rad(this.wheel_angle)), -1 * Math.cos(deg2rad(this.wheel_angle))];
		var x = (this.actual_position[0] + origin[0] * 15) - (dm[0]/5) + 6,
			y = (this.actual_position[1] + origin[1] * 15) - (dm[1]/5) + 7;
		gamejs.draw.rect(mainSurface, '#ff0000', new gamejs.Rect([viewport_offset[0] + x - 2, viewport_offset[1] + y - 2], [4, 4]));
	}
};

Tank.prototype.drawViewFinder = function(){
	this.viewfinder_end = [this.actual_position[0], this.actual_position[1]];
	var vect = [1 * Math.sin(deg2rad(this.tower_angle)), -1 * Math.cos(deg2rad(this.tower_angle))];
	var factor = 3;
	var nvect = [factor * vect[0], factor * vect[1]];
	var brk = false;
	for(var i = 1; i <= 2000; i++){
		this.viewfinder_end = [this.viewfinder_end[0] += nvect[0], this.viewfinder_end[1] += nvect[1]];
		if (cmap.mapCheckPixel(this.viewfinder_end)){
			brk = true;
		}
		
		var _this = this;
		enemies.forEach(function(e){
			if (e.isHit([_this.viewfinder_end[0], _this.viewfinder_end[1]], [1, 1], [0, 0])){
				brk = true;
			}
		});
        turrets.forEach(function(e){
            if (e.isHit([_this.viewfinder_end[0], _this.viewfinder_end[1]], [1, 1], [0, 0])){
                brk = true;
            }
        });
        structures.forEach(function(e){
            if (e.isHit([_this.viewfinder_end[0], _this.viewfinder_end[1]], [1, 1], [0, 0])){
                brk = true;
            }
        });
		if (brk)
			break;
	}
	
	var dm = this.originalImage.getSize();
	var origin = [1 * Math.sin(deg2rad(this.tower_angle)), -1 * Math.cos(deg2rad(this.tower_angle))];
	var x = (this.actual_position[0] + origin[0] * 15) - (dm[0]/5) + 6,
		y = (this.actual_position[1] + origin[1] * 15) - (dm[1]/5) + 7;
	gamejs.draw.line(mainSurface, 'rgba(235, 80, 70, 0.5)', [viewport_offset[0] + x, viewport_offset[1] + y], [viewport_offset[0] + this.viewfinder_end[0], viewport_offset[1] + this.viewfinder_end[1]], 1);
};

Tank.prototype.getMaxSpeed = function(){
	var max = playerDefaults.max_speed;
    this.player.afterburner -= playerDefaults.afterburner_dec;
	if (this.player.afterburner < 0){
        this.player.afterburner = 0;
		max = playerDefaults.speed;
	}
	return max;
};

Tank.prototype.drawHelperArrow = function(){
    var dm = this.helperArrow.getSize();
    var origin = [75 * Math.sin(deg2rad(this.wheel_angle)), -75 * Math.cos(deg2rad(this.wheel_angle))];
    var x = (this.actual_position[0] + origin[0]) - 40,
        y = (this.actual_position[1] + origin[1]) - 40;
    var arrow = gamejs.transform.rotate(this.helperArrow, this.wheel_angle);
    mainSurface.blit(arrow, [viewport_offset[0] + x, viewport_offset[1] + y]);
};



/*********************** KONIEC KLASY TANK ******************************/



/**
 * Zwraca tablice kluczy id => obj eventow do wykonania.
 *
 * @param scenario
 * @returns {{}}
 */
function readScenario(){
    if (!missionSchema.event_schema.length)
        return {};
    var s = {};
    for(i in missionSchema.event_schema){
        var _tmp = {type: missionSchema.event_schema[i].type, status: false};
        if (typeof missionSchema.event_schema[i].dependencies != "undefined"){
        	if (missionSchema.event_schema[i].dependencies.length){
        		_tmp.dependencies = new trigger.Trigger();
        	}
        }
        s[missionSchema.event_schema[i].id] = _tmp;
    }
    return s;
}

function finishedScenario(){
    if (missionScenario.length)
        return false;
    for(i in missionScenario){
        if (!missionScenario[i].status)
            return false;
    }
    return true;
}

function setIdScenario(id){
    if (typeof missionScenario[id] != "undefined"){
    	
        missionScenario[id].status = true;
    }
}

/****************/

function loadTank(){
	if (!tank){
    	tank = new Tank(mapObject.getRespawnPoint(true), playerDefaults);
	    tank.max_speed = playerDefaults.speed;
	    tank.player.SetParent(tank);
	    
	    /*tank2 = new Tank({angle:0, coords:[3,4]}, playerDefaults);
	    tank2.max_speed = playerDefaults.speed;
	    tank2.player.SetParent(tank2);*/
	    
    }
    else {
    	tank.moveTo(mapObject.getRespawnPoint(true));
    	/*tank.wheel_angle = 0;
    	tank.rotation = 180;*/
    	tank.power = 0;
    	tank.next_fire = 0;
    	tank.player.health = playerDefaults.init_health;
    	tank.player.dead = false;
    	tank.max_speed = playerDefaults.speed;
    }
}

function loadMap(load_map){
    mapObject.load(load_map);
    missionSchema = mapObject.getScenario();
    missionScenario = readScenario();
    loadItems(mapObject);
    
    cmap = new map.Map(display, itemsArray);
    viewport_offset = [0, 0];
    
    loadTank();
    
    cmap.load(mapObject, mapObject.getImage() != false ? true : false);
    cmap.updateElements(mapObject.getElement('structure'), mapObject.getElement('turret'));
    
    map_finish_prize = mapObject.settings.finish_prize;
    
    var resp = mapObject.getRespawnPoint();
    var offset_x = resp[0] * 32 - viewport_width / 2,
        offset_y = resp[1] * 32 - viewport_height / 2;
    
    if (-offset_x > 0 && cmap.size[0] * 32 >= viewport_width)
    	offset_x = 0;
    if (-offset_y > 0 && cmap.size[1] * 32 >= viewport_height)
    	offset_y = 0;
    
    viewport_offset = [-offset_x, -offset_y];
    game_success = false;
}

function loadItems(obj){
    EventTimer.Clean();
    var items = obj.getElement('item');
    if (items.length){
        for (i in items){
            itemsArray.push(new Items(items[i].type, items[i].coords, items[i].id));
        }
    }

    items = obj.getElement('turret');
    if (items.length){
        for (i in items){
            playerDefaults.userStats.enemy_count++;
            turrets.add(new turret.Turret({x: items[i].coords[0], y: items[i].coords[1], type: items[i].type}, shotTypes[items[i].shoot_type], items[i].id));
        }
    }

    items = obj.getElement('structure');
    if (items.length){
        for (i in items){
            playerDefaults.userStats.enemy_count++;
            structures.add(new structure.Building({x: items[i].coords[0], y: items[i].coords[1], type: items[i].type}, items[i].id));
        }
    }

    items = obj.getElement('enemy');
    if (items.length){
        for (i in items){
            playerDefaults.userStats.enemy_count++;
            if (typeof items[i].patrol != "undefined")
                enemies.add(new enemy.Enemy({x: items[i].coords[0], y: items[i].coords[1], type: items[i].type, width_px: width_px, height_px: height_px, mode: items[i].mode, sight_distance: enemyTypes[items[i].type].sight_distance}, shotTypes[items[i].shoot_type], items[i].id, items[i].patrol));
            else
                enemies.add(new enemy.Enemy({x: items[i].coords[0], y: items[i].coords[1], type: items[i].type, width_px: width_px, height_px: height_px, mode: items[i].mode}, shotTypes[items[i].shoot_type], items[i].id));
        }
    }

    items = obj.getElement('deploy');
    if (items.length){
        for (i in items){
            var _ni = items[i];
            for(var l = 1; l <= _ni.amount; l++){
                EventTimer.AttachEvent(_ni.timeout + (l * 3), function(){
                    enemies.add(new enemy.Enemy({x: _ni.coords[0], y: _ni.coords[1], type: 0, width_px: width_px, height_px: height_px, mode: "Aggresive"}, shotTypes[4], 0));
                });
            }
        }
    }
}

playSingleSound = function(sound, loop){
    if (sound && sound != '' && sound_enabled){
        var snd = player.Sound(sound);
        snd.setVolume(sound_volume);
        snd.play(typeof loop != "undefined" && loop ? loop : false);
    }
};


var EventTimer = {
	counter		: 0,
	events		: [],
	timer		: 0,
	time_reset	: 999999,
	flush		: 920,
	
	AttachEvent: function(time, e, data, repeat){
		this.events.push({start: this.timer, time: time, callback: e, data: data, repeat: repeat || false});
	},
	
	Reset: function(){
		this.timer = 0;
	},

    Clean: function(){
        this.events = [];
        this.Reset();
    },

	ScanEvents: function(){
		for(i in this.events){
			var e = this.events[i];
			if (parseInt(e.start) + parseInt(e.time) <= this.timer){
				if (typeof e.data != "undefined")
					e.callback(e.data);
				else e.callback();
				var index = EventTimer.events.indexOf(e);
				if (!e.repeat){
					EventTimer.events.splice(index, 1);
				}
				else{
					EventTimer.events[index].start = this.timer;
				}
			}
		}
	},
	
	Update: function(msDuration){
		if (this.events.length && !game_pause){
			this.counter += msDuration;
			if (this.counter >= this.flush){
				this.counter = 0;
				this.timer += 1;
				this.ScanEvents();
				if (this.timer > this.time_reset)
					this.Reset();
			}
		}
	}
};

function setInitialSettings(){
	tank.player.SetWeaponDefaultStatus();
	tank.player.SetAmmo(tank.current_shot_type, 1000);
//	this.player.SetAmmo(2, 1000);
//	this.player.SetAmmo(3, 1000);
//	this.player.SetAmmo(4, 1000);
//	this.player.SetAmmo(5, 1000);
//	this.player.SetAmmo(6, 1000);
//	this.player.SetAmmo(7, 1000);
//	this.player.SetAmmo(8, 1000);
//    this.player.SetAmmo(9, 1000);
//    this.player.SetAmmo(10, 1000);
    tank.player.AddWeapon(1);
    //tank.player.SetAmmo(1, 1000);
	//.AddWeapon(2).AddWeapon(3).AddWeapon(4).AddWeapon(5).AddWeapon(6).AddWeapon(7).AddWeapon(8).AddWeapon(9).AddWeapon(10);
    tank.player.weapon = shotTypes[0];
}

function restartGame(){
    enemies.empty();
    turrets.empty();
    structures.empty();
    particleSystem.empty();
    Shots.Empty();
    explosionAnims		= [];
    currentAnimations	= [];
    itemsArray			= [];
    smudgesArray		= [];
    flybyList.empty();
    Shots.array 		= new Array();
    
    //loadMap(test_map_schema[global_current_map]);
    
    loadMap(_map3);
}

var __tmp = false;


gamejs.display.setCaption("Game");

function main(){
	_gameStatus = gamejs.ready(function(){
	
	    $('#gjs-canvas').attr('tabindex', 1).focus();
	    $(window).keydown(function(event) {
	        if (event.ctrlKey && event.keyCode == 84) {
	            event.preventDefault();
	            return false;
	        }
	        if(event.ctrlKey && event.keyCode == 83) {
	            event.preventDefault();
	            return false;
	        }
	    });
	
	
		display = gamejs.display.setMode([width_px, height_px]);
	    mapObject = new mapReader.MapReader();
	    Sprite = new SpriteContainer();
	    restartGame();
	    setInitialSettings();
	    
	    mainSurface = gamejs.display.getSurface();
	    GUI.Construct(gamejs, mainSurface, load_files);
	    GUI.EventHandler();
	
	    gamejs.onEvent(function(event){
	    	if (event.type === gamejs.event.KEY_DOWN){
	    		keys_down[event.key] = true;
	    	}
	        else if (event.type === gamejs.event.KEY_UP)
	        	keys_down[event.key] = false;
	        else if (event.type === gamejs.event.MOUSE_MOTION){
	            _game_mouse_pos = event.pos;
	        }
	        else if (event.type === gamejs.event.MOUSE_DOWN){
	        	mouse_pointer[event.button] = true;
	        }
	        else if (event.type === gamejs.event.MOUSE_UP){
	        	mouse_pointer[event.button] = false;
	        }
	    });
	    
	    var _lastLoop = new Date;
	    var _fps = 0, _fps_clbk = 0;
	    var loading_pt = [200, 100],
	    loading_img_size = [200, 100];
	    
	    gamejs.onTick(function(msDuration){
	    	/*if (_gameStatus > 0 && _gameStatus < 1){
	    		var progress = _gameStatus();
	    	    progress = Math.min(Math.max(progress-0.5, 0)*2, 1);
	    	    gamejs.draw.rect(display, '#ffffff', new gamejs.Rect([loading_pt[0]-100, loading_pt[1]+260], [loading_img_size[0]+200, 40]), 2);
	    	    gamejs.draw.rect(display, '#ffffff', new gamejs.Rect([loading_pt[0]-100, loading_pt[1]+260], [(loading_img_size[0]+200)*progress, 40]), 0);
	    	}
	    	*/
	    	
			if (msDuration < msDurationLimit){
				
				display.clear();
				cmap.draw(display);
				cmap.drawEnvironment(display, 0);
	            EventTimer.Update(msDuration);
				
				if (call_restart){
					restartGame();
					if (!game_over)
						setInitialSettings();
					call_restart = false;
					game_over = false;
	                game_success = false;
				}
				
				if (call_continue){
					restartGame();
					call_restart = false;
					game_over = false;
	                game_success = false;
					call_continue = false;
				}
				
				if (game_over || game_success){
					
				}
				else {
					/***
					 * Glowna petla gry
					 */
	
	                if (finishedScenario()){
	                    if (!game_success_ev){
	                        game_success_ev = true;
	                        EventTimer.AttachEvent(1, function(){
	                            game_success = true;
	                            game_success_ev = false;
	                            GUI.LevelFinished();
	                        });
	                    }
	                }
	
					if (keys_down[bindings.accelerate]){
						tank.accelerate = ACC_ACCELERATE;
					}
					else if (keys_down[bindings.brake]){
						tank.accelerate = ACC_BRAKE;
					}
					else{
						tank.accelerate = ACC_NONE;
					}
	
					if (keys_down[bindings.steer_right]){
						tank.steer = STEER_RIGHT;
					}
					else if (keys_down[bindings.steer_left]){
						tank.steer = STEER_LEFT;
					}
					else{
						tank.steer = STEER_NONE;
					}
	
					if (keys_down[bindings.fire1]){
						tank.fire = FIRE1;
					}
					else{
						tank.fire = NO_FIRE1;
					}
					
					if (mouse_pointer[bindings.mouse_fire1]){
						tank.fire = FIRE1;
					}
					else {
						tank.fire = NO_FIRE1;
					}
					
					if (keys_down[bindings.next_weapon]){
						tank.toggleWeapon(1);
						keys_down[bindings.next_weapon] = false;
					}
					else if (keys_down[bindings.prev_weapon]){
						tank.toggleWeapon(-1);
						keys_down[bindings.prev_weapon] = false;
					}
					
					if (keys_down[bindings.slowdown]){tank.slow_down_key = true;}
					else {tank.slow_down_key = false;}
					
					if (keys_down[bindings.speedup]){
						tank.max_speed = tank.getMaxSpeed();
					}
					else {
						tank.max_speed = playerDefaults.speed;
					}
					
					if (keys_down[bindings.pause]){
						GUI.SavePause();
					}
					else{
						GUI.ReleasePause();
					}
					
					
					/*** draw **/
					
					
					
					
					if (!game_pause){
						particleSystem.update(msDuration);
						//particleSystem.update(msDuration);
					}
					//particleSystem.draw();
					particleSystem.draw();
	
	                /**
	                 * smudges
	                 */
					if (smudgesArray.length){
						if (smudgesArray.length > maxSmudgesOnScreen){
							for(var i = 1; i <= (smudgesArray.length - maxSmudgesOnScreen); i++)
								smudgesArray.shift();
						}
						for(var i in smudgesArray){
							display.blit(smudgesArray[i].smudge, [viewport_offset[0] + smudgesArray[i].coords[0] + (smudgesArray[i].size[0]/2), viewport_offset[1] + smudgesArray[i].coords[1] + (smudgesArray[i].size[1]/2)]);
						}
					}
	
	                /**
	                 * player viewfinder
	                 */
					if (tank.player.has_viewfinder){
						tank.drawViewFinder();
					}
	
	                /**
	                 * bullets
	                 */
					if (!game_pause){
						Shots.Update(msDuration);
					}
					//all_shots.update(msDuration, mainSurface);
					//all_shots.draw(mainSurface);
					
	
	                /**
	                 * enemy
	                 */
					enemies.forEach(function(e){
						if (!game_pause){
							var sh = e.update(msDuration, cmap, itemsArray, tank, enemies, itemsArrayTemp);
							if (sh !== null){
								var _weap = sh.shot;
								
								if (_weap.spread > 0){
									for (var i = 1; i <= _weap.spread; i++){
										var min = sh.angle - (_weap.spread_angle),
											max = sh.angle + (_weap.spread_angle);
										var ang = rand(min, max);
										
										Shots.Add({
											x		: sh.x,
											y		: sh.y,
											angle	: ang,
											shot	: sh.shot,
											unit	: e,
											target	: 'player'
										});
									}
								}
								else {
									Shots.Add({
										x		: sh.x,
										y		: sh.y,
										angle	: sh.angle,
										shot	: sh.shot,
										unit	: e,
										target	: 'player'
									});
								}
							}
						}
						e.drawBody();
					});
	
	                /**
	                 * turrets
	                 */
	                turrets.forEach(function(t){
	                    if (!game_pause){
	                        var sh = t.update(msDuration, cmap, tank);
	                        if (sh !== null){
	                        	var _weap = sh.shot;
								
								if (_weap.shot_sound){
									playSingleSound(_weap.shot_sound);
								}
								if (_weap.spread > 0){
									for (var i = 1; i <= _weap.spread; i++){
										var min = sh.angle - (_weap.spread_angle),
											max = sh.angle + (_weap.spread_angle);
										var ang = rand(min, max);
										
										Shots.Add({
											x		: sh.x,
											y		: sh.y,
											angle	: ang,
											shot	: sh.shot,
											unit	: t,
											target	: 'player'
										});
									}
								}
								else {
									Shots.Add({
										x		: sh.x,
										y		: sh.y,
										angle	: sh.angle,
										shot	: sh.shot,
										unit	: t,
										target	: 'player'
									});
								}
	                        }
	                    }
	                    t.drawBody();
	                });
	                
	                
	                /**
	                 * items
	                 */
					if (itemsArray.length){
						for (i in itemsArray){
							itemsArray[i].update(msDuration);
							if (itemsArray[i].picked(tank.actual_position, tank.dims)){
								tank.player.Pickup(i);
							}
						}
					}
	
	                /**
	                 * animations
	                 */
					if (currentAnimations.length){
						if (currentAnimations.length > maxAnimationsOnScreen){
							for(var i = 1; i <= (currentAnimations.length - maxAnimationsOnScreen); i++)
								currentAnimations.shift();
						}
						
						for(var i in currentAnimations){
							if (currentAnimations[i].obj.loopFinished){
								currentAnimations.splice(i, 1);
								continue;
							}
							else if (!currentAnimations[i].obj.started){
								if (!game_pause){
									var _ca = currentAnimations[i];
									_ca.obj.start('anim');
									
									if (!$.isEmptyObject(_ca.obj.particle)){
										particleSystem.add(new particles.Particles(_ca.obj.particle, _ca.pos));
									}
										//particleSystem.addNewParticles(_ca.obj.particle, _ca.pos);
									
									if (_ca.all.smudge != ''){
										var img = gamejs.image.load(_ca.all.smudge);
										img = gamejs.transform.rotate(img, rand(0, 359));
										var size = img.getSize();
										smudgesArray.push({smudge: img, size: size, coords: [_ca.pos[0] - (_ca.obj.width / 2) - size[0]/2, _ca.pos[1] - (_ca.obj.height / 2) - size[1]/2]});
									}
								}
							}
							
							if (!game_pause)
								currentAnimations[i].obj.update(msDuration);
							display.blit(currentAnimations[i].obj.image, [viewport_offset[0] + currentAnimations[i].pos[0] - (currentAnimations[i].obj.width / 2), viewport_offset[1] + currentAnimations[i].pos[1] - (currentAnimations[i].obj.height / 2)]);
						}
					}
	
	                /**
	                 * player
	                 */
					if (!game_pause){
						tank.update(msDuration);
					}
					tank.draw(mainSurface);
					
					
	                /**
	                 * structures
	                 */
	                structures.forEach(function(t){
	                    if (!game_pause){
	                        var sh = t.update(msDuration, cmap, tank);
	                    }
	                    t.drawBody();
	                });
	
	                /**
	                 * flyby
	                 */
					if (!game_pause){
						flybyList.update(msDuration);
					}
					flybyList.draw();
					
	                /**
	                 * item event timer
	                 */
					if (itemsArrayTemp.length){
						for(var i in itemsArrayTemp){
							EventTimer.AttachEvent(itemsArrayTemp[i].time, function(s){
								itemsArray.push(new Items(s.n, s.c));
							}, {n: itemsArrayTemp[i].name, c: itemsArrayTemp[i].coords});
							itemsArrayTemp.splice(i, 1);
						}
					}
				}
				cmap.drawEnvironment(display, 1);
				GUI.Draw(tank);
	            cmap.drawMiniMap(display, itemsArray, enemies, structures, turrets, tank);
	            
	            if (display_fps){
	            	_fps_clbk++;
	            	var _thisLoop = new Date;
	            	if (_fps_clbk > fps_refresh_rate){
	            		_fps_clbk = 0;
	            		_fps = 1000 / (_thisLoop - _lastLoop);
	            	}
	            	GUI.DrawFPS(_fps);
	                _lastLoop = _thisLoop;
	            }
			}
	    });
	});
}

gamejs.preload(organizer.dump());
main();