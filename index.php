<?php
	$asset_base = '.';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Game &bull; Gamejs</title>
    <meta name="Author" content="furberia.pl"/>
    <link rel="stylesheet" href="<?php echo $asset_base?>/css/jquery-ui-1.10.4.custom.css" />
	<link rel="stylesheet" href="<?php echo $asset_base?>/css/main.css" />
	<script type="text/javascript">var _epd='<?php echo base64_encode($asset_base.'/')?>',_ai='<?php /*echo base64_encode(path('game_account_info'))*/?>',_n3a='<?php /*echo base64_encode($uid)*/?>';</script>
	<script src="<?php echo $asset_base?>/javascript/jquery-1.10.2.js"></script>
	<script src="<?php echo $asset_base?>/javascript/jquery-ui-1.10.4.custom.min.js"></script>
	<script src="<?php echo $asset_base?>/public/_map.js"></script>
	<script src="<?php echo $asset_base?>/public/_map2.js"></script>
	<script src="<?php echo $asset_base?>/public/_map3.js"></script>
	<script src="<?php echo $asset_base?>/public/_map4.js"></script>
	<script src="<?php echo $asset_base?>/javascript/settings.js"></script>
	<script src="<?php echo $asset_base?>/javascript/gui.js"></script>
    <script src="<?php echo $asset_base?>/public/yabble.js"></script>
    <script src="<?php echo $asset_base?>/public/gamejs.min.js"></script>
    <script type="text/javascript">
        require.setModuleRoot('<?php echo $asset_base?>/javascript/');
        require.run('main');
    </script>
</head>
<!-- Html -->
<body>
    <div class="container">
        <div id="gjs-loader">
            Loading...
        </div>
		
        <canvas id="gjs-canvas"></canvas>
    </div>

    <script type="text/javascript">
		$(document).bind('keydown', function(e){
			e.preventDefault();
		});
		//document.getElementById("ctrl").setAttribute('disabled', true);
    </script>
</body>
</html>